/* mbed Microcontroller Library
 * Copyright (c) 2018 ARM Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef FLASH_H
#define FLASH_H

#include "SFDP.h"
#include "GPIO.h"
#include "SPI.h"

/** Enum spif standard error codes
 *
 *  @enum SPIFError
 */
enum SpifError 
{
    SPIF_OK                    = 0,     /*!< no error */
    SPIF_DEVICE_ERROR          = -4001, /*!< device specific error -4001 */
    SPIF_PARSING_FAILED        = -4002, /* SFDP Parsing failed */
    SPIF_READY_FAILED          = -4003, /* Wait for Memory Ready failed */
    SPIF_WREN_FAILED           = -4004, /* Write Enable Failed */
    SPIF_INVALID_ERASE_PARAMS  = -4005, /* Erase command not on sector aligned addresses or exceeds device size */
};

class Flash
{
public:
    /** Creates a FLASH on a SPI bus specified by pins
     *
     *  @param mosi     SPI master out, slave in pin
     *  @param miso     SPI master in, slave out pin
     *  @param sclk     SPI clock pin
     *  @param csel     SPI chip select pin
     *  @param freq     Clock speed of the SPI bus (defaults to 40MHz)
     *
     */
    Flash(PinName mosi,
          PinName miso,
          PinName sclk,
          PinName csel,
          uint32_t freq);

    /** Initialize a block device
     *
     *  @return         SPIF_OK(0) - success
     *                  SPIF_DEVICE_ERROR - device driver transaction failed
     *                  SPIF_READY_FAILED - Waiting for Memory ready failed or timed out
     *                  SPIF_PARSING_FAILED - unexpected format or values in one of the SFDP tables
     */
    virtual SpifError init();

    /** Deinitialize a block device
     *
     *  @return         SPIF_OK(0) - success
     */
    virtual SpifError deinit();

    /** Desctruct SPIFBlockDevice
      */
    ~Flash()
    {
        deinit();
    }

    /** Read blocks from a block device
     *
     *  @param buffer   Buffer to write blocks to
     *  @param addr     Address of block to begin reading from
     *  @param size     Size to read in bytes, must be a multiple of read block size
     *  @return         SPIF_OK(0) - success
     *                  SPIF_DEVICE_ERROR - device driver transaction failed
     */
    virtual SpifError read(void *buffer, uint32_t addr, uint32_t size);

    /** Program blocks to a block device
     *
     *  @note The blocks must have been erased prior to being programmed
     *
     *  @param buffer   Buffer of data to write to blocks
     *  @param addr     Address of block to begin writing to
     *  @param size     Size to write in bytes, must be a multiple of program block size
     *  @return         SPIF_OK(0) - success
     *                  SPIF_DEVICE_ERROR - device driver transaction failed
     *                  SPIF_READY_FAILED - Waiting for Memory ready failed or timed out
     *                  SPIF_WREN_FAILED - Write Enable failed
     */
    virtual SpifError program(const void *buffer, uint32_t addr, uint32_t size);

    /** Erase blocks on a block device
     *
     *  @note The state of an erased block is undefined until it has been programmed
     *
     *  @param addr     Address of block to begin erasing
     *  @param size     Size to erase in bytes, must be a multiple of erase block size
     *  @return         SPIF_OK(0) - success
     *                  SPIF_DEVICE_ERROR - device driver transaction failed
     *                  SPIF_READY_FAILED - Waiting for Memory ready failed or timed out
     *                  SPIF_INVALID_ERASE_PARAMS - Trying to erase unaligned address or size
     */
    virtual SpifError erase(uint32_t addr, uint32_t size);

    /** Get the size of a readable block
     *
     *  @return         Size of a readable block in bytes
     */
    virtual uint32_t get_read_size() const;

    /** Get the size of a programable block
     *
     *  @return         Size of a programable block in bytes
     *  @note Must be a multiple of the read size
     */
    virtual uint32_t get_program_size() const;

    /** Get the size of an erasable block
     *
     *  @return         Size of an erasable block in bytes
     *  @note Must be a multiple of the program size
     */
    virtual uint32_t get_erase_size() const;

    /** Get the size of minimal erasable sector size of given address
     *
     *  @param addr     Any address within block queried for erase sector size (can be any address within flash size offset)
     *  @return         Size of minimal erase sector size, in given address region, in bytes
     *  @note Must be a multiple of the program size
     */
    virtual uint32_t get_erase_size(uint32_t addr) const;

    /** Get the value of storage byte after it was erased
     *
     *  If get_erase_value returns a non-negative byte value, the underlying
     *  storage is set to that value when erased, and storage containing
     *  that value can be programmed without another erase.
     *
     *  @return         The value of storage when erased, or -1 if you can't
     *                  rely on the value of erased storage
     */
    virtual int get_erase_value() const;

    /** Get the total size of the underlying device
     *
     *  @return         Size of the underlying device in bytes
     */
    virtual uint32_t size() const;

    /** Get the BlockDevice class type.
     *
     *  @return         A string representation of the BlockDevice class type.
     */
    virtual const char *get_type() const;

private:
    /****************************************/
    /* SFDP Detection and Parsing Functions */
    /****************************************/
    // Send SFDP Read command to Driver
    int _spi_send_read_sfdp_command(uint32_t addr,
                                    void *rx_buffer,
                                    uint32_t rx_length);

    // Parse and Detect required Basic Parameters from Table
    int _sfdp_parse_basic_param_table(sfdp_hdr_info &hdr_info);

    // Detect fastest read Bus mode supported by device
    int _sfdp_detect_best_bus_read_mode(uint8_t *basic_param_table_ptr, 
                                        uint32_t basic_param_table_size, 
                                        uint8_t &read_inst);
    
    // Parse SFDP Database
    int sfdp_parse_headers(sfdp_hdr_info &sfdp_info);

    // Parse Sector Map Parameter Table
    int sfdp_parse_sector_map_table(sfdp_smptbl_info &smtbl);

    /********************************/
    /*   Calls to SPI Driver APIs   */
    /********************************/
    // Send Program => Write command to Driver
    SpifError _spi_send_program_command(uint8_t prog_inst, const void *buffer, uint32_t addr, uint32_t size);

    // Send Read command to Driver
    SpifError _spi_send_read_command(uint8_t read_inst, uint8_t *buffer, uint32_t addr, uint32_t size);

    // Send Erase Instruction using command_transfer command to Driver
    SpifError _spi_send_erase_command(uint8_t erase_inst, uint32_t addr, uint32_t size);

    // Send Generic command_transfer command to Driver
    SpifError _spi_send_general_command(uint8_t instruction, 
                                        uint32_t addr, 
                                        uint8_t *tx_buffer,
                                        size_t tx_length, 
                                        uint8_t *rx_buffer, 
                                        size_t rx_length);

    // Send set_frequency command to Driver
    SpifError _spi_set_frequency(uint32_t freq);
    /********************************/

    // Soft Reset Flash Memory
    int32_t _reset_flash_mem();

    // Configure Write Enable in Status Register
    int32_t _set_write_enable();

    // Wait on status register until write not-in-progress
    bool _is_mem_ready();

private:
    // Master side hardware
    SPI m_spi;

    // Command Instructions
    uint8_t m_read_instruction;
    uint8_t m_prog_instruction;
    uint8_t m_erase_instruction;

    // Data extracted from the devices SFDP structure
    sfdp_hdr_info m_sfdp_info;

    uint32_t m_page_size_bytes; // Page size - 256 Bytes default
    uint32_t m_device_size_bytes;

    // Bus configuration
    uint32_t m_address_size; // number of bytes for address
    uint32_t m_read_dummy_and_mode_cycles; // Number of Dummy and Mode Bits required by Read Bus Mode
    uint32_t m_write_dummy_and_mode_cycles; // Number of Dummy and Mode Bits required by Write Bus Mode
    uint32_t m_dummy_and_mode_cycles; // Number of Dummy and Mode Bits required by Current Bus Mode
    uint32_t m_init_ref_count;
    bool m_is_initialized;
};

#endif 
