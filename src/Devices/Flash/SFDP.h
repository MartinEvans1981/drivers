/*
 * Copyright (c) 2020, Arm Limited and affiliates.
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef SFDP_H
#define SFDP_H

#include <cstddef>
#include <cstdint>
#include <stdint.h>

/** \defgroup drivers-internal-api-sfdp SFDP
 * \ingroup drivers-internal-api
 * Serial Flash Discoverable Parameters.
 *
 * Based on <a href="https://www.jedec.org/standards-documents/docs/jesd216b">JESD216D.01 Standard</a>.
 * @{
 */

const int SFDP_HEADER_SIZE = 8; ///< Size of an SFDP header in bytes, 2 DWORDS
const int SFDP_BASIC_PARAMS_TBL_SIZE = 80; ///< Basic Parameter Table size in bytes, 20 DWORDS
const int SFDP_SECTOR_MAP_MAX_REGIONS = 10; ///< Maximum number of regions with different erase granularity

// Erase Types Per Region BitMask
const int SFDP_ERASE_BITMASK_TYPE4 = 0x08; ///< Erase type 4 (erase granularity) identifier
const int SFDP_ERASE_BITMASK_TYPE3 = 0x04; ///< Erase type 3 (erase granularity) identifier
const int SFDP_ERASE_BITMASK_TYPE2 = 0x02; ///< Erase type 2 (erase granularity) identifier
const int SFDP_ERASE_BITMASK_TYPE1 = 0x01; ///< Erase type 1 (erase granularity) identifier
const int SFDP_ERASE_BITMASK_NONE = 0x00;  ///< Erase type None
const int SFDP_ERASE_BITMASK_ALL = 0x0F;   ///< Erase type All

const int SFDP_MAX_NUM_OF_ERASE_TYPES = 4;  ///< Maximum number of different erase types (erase granularity)

/** JEDEC Basic Flash Parameter Table info */
struct sfdp_bptbl_info 
{
    uint32_t addr; ///< Address
    uint32_t size; ///< Size
    uint32_t device_size_bytes;
    int legacy_erase_instruction; ///< Legacy 4K erase instruction
};

/** SFDP Parameter header */
struct sfdp_prm_hdr
{
    uint8_t PID_LSB; ///< Parameter ID LSB
    uint8_t P_MINOR; ///< Parameter Minor Revision
    uint8_t P_MAJOR; ///< Parameter Major Revision
    uint8_t P_LEN;   ///< Parameter length in DWORDS
    uint32_t DWORD2; ///< Parameter ID MSB + Parameter Table Pointer
};

/** SFDP Header */
struct sfdp_hdr 
{
    uint8_t SIG_B0; ///< SFDP Signature, Byte 0
    uint8_t SIG_B1; ///< SFDP Signature, Byte 1
    uint8_t SIG_B2; ///< SFDP Signature, Byte 2
    uint8_t SIG_B3; ///< SFDP Signature, Byte 3
    uint8_t R_MINOR; ///< SFDP Minor Revision
    uint8_t R_MAJOR; ///< SFDP Major Revision
    uint8_t NPH; ///< Number of parameter headers (0-based, 0 indicates 1 parameter header)
    uint8_t ACP; ///< SFDP Access Protocol
};

/** JEDEC Sector Map Table info */
struct sfdp_smptbl_info 
{
    uint32_t addr; ///< Address
    uint32_t size; ///< Size
    int region_cnt; ///< Number of erase regions
    int region_size[SFDP_SECTOR_MAP_MAX_REGIONS]; ///< Erase region size in bytes
    uint8_t region_erase_types_bitfld[SFDP_SECTOR_MAP_MAX_REGIONS]; ///< Each Region can support a bit combination of any of the 4 Erase Types
    unsigned int regions_min_common_erase_size; ///< Minimal common erase size for all regions (0 if none exists)
    uint32_t region_high_boundary[SFDP_SECTOR_MAP_MAX_REGIONS]; ///< Region high address offset boundary
    int erase_type_inst_arr[SFDP_MAX_NUM_OF_ERASE_TYPES]; ///< // Up To 4 Erase Types are supported by SFDP (each with its own command Instruction and Size)
    unsigned int erase_type_size_arr[SFDP_MAX_NUM_OF_ERASE_TYPES]; ///< Erase sizes for all different erase types
};

/** SFDP JEDEC Parameter Table info */
struct sfdp_hdr_info 
{
    sfdp_bptbl_info bptbl;
    sfdp_smptbl_info smptbl;
};


/** Detect page size used for writing on flash
 *
 * @param bptbl_ptr  Pointer to memory holding a Basic Parameter Table structure
 * @param bptbl_size Size of memory holding the Basic Parameter Table
 *
 * @return Page size
 */
uint32_t sfdp_detect_page_size(uint8_t *bptbl_ptr, uint32_t bptbl_size);

/** Detect all supported erase types
 *
 * @param         bptbl_ptr Pointer to memory holding a JEDEC Basic Flash Parameter Table
 * @param[in,out] sfdp_info Contains the results of parsing erase type instructions and sizes
 *
 * @return MBED_SUCCESS on success, negative error code on failure
 */
int sfdp_detect_erase_types_inst_and_size(uint8_t *bptbl_ptr, sfdp_hdr_info &sfdp_info);

/** Find the region to which the given offset belongs to
 *
 * @param offset    Offset value
 * @param sfdp_info Region information
 *
 * @return Region number
 */
int sfdp_find_addr_region(uint32_t offset, const sfdp_hdr_info &sfdp_info);

/** Finds the largest Erase Type of the Region to which the offset belongs to
 *
 * Iterates from highest type to lowest.
 *
 * @param bitfield Erase types bit field
 * @param size     Upper limit for region size
 * @param offset   Offset value
 * @param region   Region number
 * @param smtbl    Information about different erase types
 *
 * @return Largest erase type
 */
int sfdp_iterate_next_largest_erase_type(uint8_t &bitfield,
                                         int size,
                                         int offset,
                                         int region,
                                         const sfdp_smptbl_info &smptbl);


int sfdp_parse_sfdp_header(sfdp_hdr *sfdp_hdr_ptr);

int sfdp_parse_single_param_header(sfdp_prm_hdr *phdr_ptr, sfdp_hdr_info &hdr_info);

#endif
