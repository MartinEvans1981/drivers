/* mbed Microcontroller Library
 * Copyright (c) 2018 ARM Limited
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "Flash.h"

#include <string.h>
#include <inttypes.h>


/* Default SPIF Parameters */
/****************************/
#define SPIF_DEFAULT_READ_SIZE  1
#define SPIF_DEFAULT_PROG_SIZE  1

// Status Register Bits
#define SPIF_STATUS_BIT_WIP 0x1 //Write In Progress
#define SPIF_STATUS_BIT_WEL 0x2 // Write Enable Latch

/* Basic Parameters Table Parsing */
/**********************************/
// Address Length
#define SPIF_ADDR_SIZE_3_BYTES 3
#define SPIF_ADDR_SIZE_4_BYTES 4

// Default read/legacy erase instructions
#define SPIF_INST_READ_DEFAULT          0x03
#define SPIF_INST_LEGACY_ERASE_DEFAULT  (-1)

#define IS_MEM_READY_MAX_RETRIES 10000


const uint32_t SPI_NO_ADDRESS_COMMAND = 0xFFFFFFFF;

enum SpifDefaultInstructions 
{
    SPIF_NOP = 0x00, // No operation
    SPIF_PP = 0x02, // Page Program data
    SPIF_READ = 0x03, // Read data
    SPIF_SE   = 0x20, // 4KB Sector Erase
    SPIF_SFDP = 0x5a, // Read SFDP
    SPIF_WRSR = 0x01, // Write Status/Configuration Register
    SPIF_WRDI = 0x04, // Write Disable
    SPIF_RDSR = 0x05, // Read Status Register
    SPIF_WREN = 0x06, // Write Enable
    SPIF_RSTEN = 0x66, // Reset Enable
    SPIF_RST = 0x99, // Reset
    SPIF_RDID = 0x9f, // Read Manufacturer and JDEC Device ID
    SPIF_ULBPR = 0x98, // Clears all write-protection bits in the Block-Protection register,
    SPIF_4BEN = 0xB7, // Enable 4-byte address mode
    SPIF_4BDIS = 0xE9, // Disable 4-byte address mode
};

//***********************
// SPIF Block Device APIs
//***********************
Flash::Flash(PinName mosi,
             PinName miso,
             PinName sclk,
             PinName csel,
             uint32_t freq)
    :m_spi(mosi, miso, sclk, csel, true),
     m_prog_instruction(0),
     m_erase_instruction(0),
     m_page_size_bytes(0),
     m_init_ref_count(0),
     m_is_initialized(false)
{
    m_spi.deselect();
    
    m_address_size = SPIF_ADDR_SIZE_3_BYTES;
    // Initial SFDP read tables are read with 8 dummy cycles
    // Default Bus Setup 1_1_1 with 0 dummy and mode cycles
    m_read_dummy_and_mode_cycles = 8;
    m_write_dummy_and_mode_cycles = 0;
    m_dummy_and_mode_cycles = m_read_dummy_and_mode_cycles;

    m_sfdp_info.bptbl.device_size_bytes = 0;
    m_sfdp_info.bptbl.legacy_erase_instruction = SPIF_INST_LEGACY_ERASE_DEFAULT;
    m_sfdp_info.smptbl.regions_min_common_erase_size = 0;
    m_sfdp_info.smptbl.region_cnt = 1;
    m_sfdp_info.smptbl.region_erase_types_bitfld[0] = SFDP_ERASE_BITMASK_NONE;

    // Set default read/erase instructions
    m_read_instruction = SPIF_INST_READ_DEFAULT;

    if (SPIF_OK != _spi_set_frequency(freq))
    {
        // some error
    }
}

/**********************************************************************************/

SpifError Flash::init()
{
    uint8_t vendor_device_ids[4] = {0};
    size_t data_length = 3;
    SpifError status = SPIF_OK;
    SpifError spi_status = SPIF_OK;

    m_sfdp_info.bptbl.addr = 0x0;
    m_sfdp_info.bptbl.size = 0;
    m_sfdp_info.smptbl.addr = 0x0;
    m_sfdp_info.smptbl.size = 0;

    if (!m_is_initialized) 
    {
        m_init_ref_count = 0;
    }

    m_init_ref_count++;

    if (m_init_ref_count != 1) 
    {
        goto cleanup;
    }

    // Soft Reset
    if (-1 == _reset_flash_mem()) 
    {
        status = SPIF_DEVICE_ERROR;
        goto cleanup;
    } 

    /* Read Manufacturer ID (1byte), and Device ID (2bytes)*/
    spi_status = _spi_send_general_command(SPIF_RDID, 
                                           SPI_NO_ADDRESS_COMMAND,
                                           NULL,
                                           0,
                                           (uint8_t *)vendor_device_ids,
                                           data_length);

    if (spi_status != SPIF_OK) 
    {
        status = SPIF_DEVICE_ERROR;
        goto cleanup;
    }

    switch (vendor_device_ids[0]) 
    {
        case 0xbf:
            // SST devices come preset with block protection
            // enabled for some regions, issue global protection unlock to clear
            _set_write_enable();
            _spi_send_general_command(SPIF_ULBPR,
                                      SPI_NO_ADDRESS_COMMAND,
                                      NULL,
                                      0,
                                      NULL,
                                      0);
            break;
    }

    //Synchronize Device
    if (false == _is_mem_ready()) 
    {
        status = SPIF_READY_FAILED;
        goto cleanup;
    }

    /**************************** Parse SFDP Header ***********************************/
    if (sfdp_parse_headers(m_sfdp_info) < 0)
    {
        status = SPIF_PARSING_FAILED;
        goto cleanup;
    }


    /**************************** Parse Basic Parameters Table ***********************************/
    if (_sfdp_parse_basic_param_table(m_sfdp_info) < 0) 
    {
        status = SPIF_PARSING_FAILED;
        goto cleanup;
    }

    /**************************** Parse Sector Map Table ***********************************/
    m_sfdp_info.smptbl.region_size[0] = m_sfdp_info.bptbl.device_size_bytes;
    // If there's no region map, we have a single region sized the entire device size
    m_sfdp_info.smptbl.region_high_boundary[0] = m_sfdp_info.bptbl.device_size_bytes - 1;

    if ((m_sfdp_info.smptbl.addr != 0) && (0 != m_sfdp_info.smptbl.size)) 
    {
        if (sfdp_parse_sector_map_table(m_sfdp_info.smptbl) < 0)
         {
            status = SPIF_PARSING_FAILED;
            goto cleanup;
        }
    }

    // Configure  BUS Mode to 1_1_1 for all commands other than Read
    // Dummy And Mode Cycles Back default 0
    m_dummy_and_mode_cycles = m_write_dummy_and_mode_cycles;
    m_is_initialized = true;

    if (m_sfdp_info.bptbl.device_size_bytes > (1 << 24)) 
    {
        _spi_send_general_command(SPIF_4BEN, SPI_NO_ADDRESS_COMMAND, NULL, 0, NULL, 0);
        m_address_size = SPIF_ADDR_SIZE_4_BYTES;
    }

cleanup:
    return status;
}

/**********************************************************************************/

SpifError Flash::deinit()
{
    SpifError status = SPIF_OK;

    if (!m_is_initialized) 
    {
        m_init_ref_count = 0;
        goto cleanup;
    }

    m_init_ref_count--;

    if (m_init_ref_count) 
    {
        goto cleanup;
    }

    // Disable Device for Writing
    status = _spi_send_general_command(SPIF_WRDI, 
                                       SPI_NO_ADDRESS_COMMAND, 
                                       NULL, 
                                       0, 
                                       NULL, 
                                       0);
    if (status != SPIF_OK)  
    {
    }

    m_is_initialized = false;

cleanup:

    return status;
}

/**********************************************************************************/

SpifError Flash::read(void *buffer, uint32_t addr, uint32_t size)
{
    SpifError status = SPIF_OK;
    
    if (!m_is_initialized) 
    {
        return SPIF_DEVICE_ERROR;
    }

    // Set Dummy Cycles for Specific Read Command Mode
    m_dummy_and_mode_cycles = m_read_dummy_and_mode_cycles;

    status = _spi_send_read_command(m_read_instruction,
                                    static_cast<uint8_t *>(buffer),
                                    addr,
                                    size);

    // Set Dummy Cycles for all other command modes
    m_dummy_and_mode_cycles = m_write_dummy_and_mode_cycles;

    return status;
}

/**********************************************************************************/

SpifError Flash::program(const void *buffer, uint32_t addr, uint32_t size)
{
    SpifError status = SPIF_OK;
    uint32_t offset = 0;
    uint32_t chunk = 0;

    if (!m_is_initialized)
    {
        return SPIF_DEVICE_ERROR;
    }

    while (size > 0)
    {
        // Write on m_page_size_bytes boundaries (Default 256 bytes a page)
        offset = addr % m_page_size_bytes;
        chunk = (offset + size < m_page_size_bytes) ? size 
                                                   : (m_page_size_bytes - offset);

        //Send WREN
        if (_set_write_enable() != 0) 
        {
            status = SPIF_WREN_FAILED;
            goto cleanup;
        }

        _spi_send_program_command(m_prog_instruction, 
                                  buffer, 
                                  addr, 
                                  chunk);

        buffer = static_cast<const uint8_t *>(buffer) + chunk;
        addr += chunk;
        size -= chunk;

        if (false == _is_mem_ready()) 
        {
            status = SPIF_READY_FAILED;
            goto cleanup;
        }
    }

cleanup:

    return status;
}

/**********************************************************************************/

SpifError Flash::erase(uint32_t addr, uint32_t in_size)
{
    int type = 0;
    uint32_t offset = 0;
    uint32_t chunk = 4096;
    int cur_erase_inst = m_erase_instruction;
    int size = (int)in_size;
    SpifError status = SPIF_OK;

    // Find region of erased address
    int region = sfdp_find_addr_region(addr, m_sfdp_info);
    // Erase Types of selected region
    uint8_t bitfield = m_sfdp_info.smptbl.region_erase_types_bitfld[region];

    if (!m_is_initialized)
    {
        return SPIF_DEVICE_ERROR;
    }

    if (region < 0) 
    {
        return SPIF_INVALID_ERASE_PARAMS;
    }

    if ((addr + in_size) > m_sfdp_info.bptbl.device_size_bytes) 
    {
        return SPIF_INVALID_ERASE_PARAMS;
    }

    if (((addr % get_erase_size(addr)) != 0) || (((addr + in_size) % get_erase_size(addr + in_size - 1)) != 0)) 
    {
        return SPIF_INVALID_ERASE_PARAMS;
    }

    // For each iteration erase the largest section supported by current region
    while (size > 0) 
    {
        // iterate to find next Largest erase type ( a. supported by region, b. smaller than size)
        // find the matching instruction and erase size chunk for that type.
        type = sfdp_iterate_next_largest_erase_type(bitfield, 
                                                    size, 
                                                    (unsigned int)addr, 
                                                    region, 
                                                    m_sfdp_info.smptbl);

        cur_erase_inst = m_sfdp_info.smptbl.erase_type_inst_arr[type];
        offset = addr % m_sfdp_info.smptbl.erase_type_size_arr[type];

        chunk = ((offset + size) < m_sfdp_info.smptbl.erase_type_size_arr[type]) ?
                size : (m_sfdp_info.smptbl.erase_type_size_arr[type] - offset);

        if (_set_write_enable() != 0) 
        {
            status = SPIF_READY_FAILED;
            goto cleanup;
        }

        _spi_send_erase_command(cur_erase_inst, addr, size);

        addr += chunk;
        size -= chunk;

        if ((size > 0) && (addr > m_sfdp_info.smptbl.region_high_boundary[region]))
        {
            // erase crossed to next region
            region++;
            bitfield = m_sfdp_info.smptbl.region_erase_types_bitfld[region];
        }

        if (false == _is_mem_ready())
        {
            status = SPIF_READY_FAILED;
            goto cleanup;
        }

    }

cleanup:
    return status;
}

/**********************************************************************************/

uint32_t Flash::get_read_size() const
{
    // Assuming all devices support 1byte read granularity
    return SPIF_DEFAULT_READ_SIZE;
}

/**********************************************************************************/

uint32_t Flash::get_program_size() const
{
    // Assuming all devices support 1byte program granularity
    return SPIF_DEFAULT_PROG_SIZE;
}

/**********************************************************************************/

uint32_t Flash::get_erase_size() const
{
    // return minimal erase size supported by all regions (0 if none exists)
    return m_sfdp_info.smptbl.regions_min_common_erase_size;
}

/**********************************************************************************/

// Find minimal erase size supported by the region to which the address belongs to
uint32_t Flash::get_erase_size(uint32_t addr) const
{
    // Find region of current address
    int region = sfdp_find_addr_region(addr, m_sfdp_info);

    unsigned int min_region_erase_size = m_sfdp_info.smptbl.regions_min_common_erase_size;
    int8_t type_mask = SFDP_ERASE_BITMASK_TYPE1;
    int i_ind = 0;

    if (region != -1) 
    {
        type_mask = 0x01;

        for (i_ind = 0; i_ind < 4; i_ind++) 
        {
            // loop through erase types bitfield supported by region
            if (m_sfdp_info.smptbl.region_erase_types_bitfld[region] & type_mask) 
            {
                min_region_erase_size = m_sfdp_info.smptbl.erase_type_size_arr[i_ind];
                break;
            }
            type_mask = type_mask << 1;
        }

        if (i_ind == 4) 
        {
            return 0/*"error"*/;
        }
    }

    return (uint32_t)min_region_erase_size;
}

/**********************************************************************************/

uint32_t Flash::size() const
{
    if (!m_is_initialized) 
    {
        return 0;
    }

    return m_sfdp_info.bptbl.device_size_bytes;
}

/**********************************************************************************/

int Flash::get_erase_value() const
{
    return 0xFF;
}

/**********************************************************************************/

const char *Flash::get_type() const
{
    return "SPIF";
}

/**********************************************************************************/

/***************************************************/
/*********** SPI Driver API Functions **************/
/***************************************************/
SpifError Flash::_spi_set_frequency(uint32_t freq)
{
    m_spi.frequency(freq);
    return SPIF_OK;
}

/**********************************************************************************/

SpifError Flash::_spi_send_read_command(uint8_t read_inst, uint8_t *buffer, uint32_t addr, uint32_t size)
{
    uint32_t dummy_bytes = m_dummy_and_mode_cycles / 8;
    int dummy_byte = 0;

    // csel must go low for the entire command (Inst, Address and Data)
    m_spi.select();

    // Write 1 byte Instruction
    m_spi.write(read_inst);

    // Write Address (can be either 3 or 4 bytes long)
    for (int address_shift = ((m_address_size - 1) * 8); address_shift >= 0; address_shift -= 8) 
    {
        m_spi.write((addr >> address_shift) & 0xFF);
    }

    // Write Dummy Cycles Bytes
    for (uint32_t i = 0; i < dummy_bytes; i++) 
    {
        m_spi.write(dummy_byte);
    }

    // Read Data
    m_spi.write(NULL, 0, buffer, size);

    // csel back to high
    m_spi.deselect();
    return SPIF_OK;
}

/**********************************************************************************/

int Flash::_spi_send_read_sfdp_command(uint32_t addr, void *rx_buffer, uint32_t rx_length)
{
    // Set 1-1-1 bus mode for SFDP header parsing
    // Initial SFDP read tables are read with 8 dummy cycles
    m_read_dummy_and_mode_cycles = 8;
    m_dummy_and_mode_cycles = 8;

    int status = _spi_send_read_command(SPIF_SFDP, (uint8_t *)rx_buffer, addr, rx_length);
    if (status < 0) 
    {
        return 0/*"error"*/;
    }

    return status;
}

/**********************************************************************************/

SpifError Flash::_spi_send_program_command(uint8_t prog_inst, 
                                           const void *buffer,
                                           uint32_t addr,
                                           uint32_t size)
{
    // Send Program (write) command to device driver
    uint32_t dummy_bytes = m_dummy_and_mode_cycles / 8;
    int dummy_byte = 0;
    uint8_t *data = (uint8_t *)buffer;
    
    // csel must go low for the entire command (Inst, Address and Data)
    m_spi.select();

    // Write 1 byte Instruction
    m_spi.write(prog_inst);

    // Write Address (can be either 3 or 4 bytes long)
    for (int address_shift = ((m_address_size - 1) * 8); address_shift >= 0; address_shift -= 8) 
    {
        m_spi.write((addr >> address_shift) & 0xFF);
    }

    // Write Dummy Cycles Bytes
    for (uint32_t i = 0; i < dummy_bytes; i++)
    {
        m_spi.write(dummy_byte);
    }

    // Write Data
    for (uint32_t i = 0; i < size; i++) 
    {
        m_spi.write(data[i]);
    }

    // csel back to high
    m_spi.deselect();

    return SPIF_OK;
}

/**********************************************************************************/

SpifError Flash::_spi_send_erase_command(uint8_t erase_inst, uint32_t addr, uint32_t size)
{
    addr = (((int)addr) & 0xFFFFF000);
    _spi_send_general_command(erase_inst, addr, NULL, 0, NULL, 0);
    return SPIF_OK;
}

/**********************************************************************************/

SpifError Flash::_spi_send_general_command(uint8_t instruction,
                                           uint32_t addr, 
                                           uint8_t *tx_buffer,
                                           size_t tx_length,
                                           uint8_t *rx_buffer,
                                           size_t rx_length)
{
    // Send a general command Instruction to driver
    uint32_t dummy_bytes = m_dummy_and_mode_cycles / 8;
    uint8_t dummy_byte = 0x00;

    // csel must go low for the entire command (Inst, Address and Data)
    m_spi.select();

    // Write 1 byte Instruction
    m_spi.write(instruction);

    // Reading SPI Bus registers does not require Flash Address
    if (addr != SPI_NO_ADDRESS_COMMAND) 
    {
        // Write Address (can be either 3 or 4 bytes long)
        for (int address_shift = ((m_address_size - 1) * 8); address_shift >= 0; address_shift -= 8) 
        {
            m_spi.write((addr >> address_shift) & 0xFF);
        }

        // Write Dummy Cycles Bytes
        for (uint32_t i = 0; i < dummy_bytes; i++) 
        {
            m_spi.write(dummy_byte);
        }
    }

    // Read/Write Data
    m_spi.write(tx_buffer, (int)tx_length, rx_buffer, (int)rx_length);

    // csel back to high
    m_spi.deselect();

    return SPIF_OK;
}

/**********************************************************************************/

/*********************************************************/
/********** SFDP Parsing and Detection Functions *********/
/*********************************************************/

/**********************************************************************************/

int Flash::sfdp_parse_headers(sfdp_hdr_info &sfdp_info)
{
    uint32_t addr = 0x0;
    int number_of_param_headers = 0;
    size_t data_length;

    {
        data_length = SFDP_HEADER_SIZE;
        uint8_t sfdp_header[SFDP_HEADER_SIZE];

        int status = _spi_send_read_sfdp_command(addr, sfdp_header, data_length);
        if (status < 0) 
        {
            /*tr_error("Retrieving SFDP Header failed")*/;
            return -1;
        }

        number_of_param_headers = sfdp_parse_sfdp_header((sfdp_hdr *)sfdp_header);
        if (number_of_param_headers < 0) 
        {
            return number_of_param_headers;
        }
    }

    addr += SFDP_HEADER_SIZE;

    {
        data_length = SFDP_HEADER_SIZE;
        uint8_t param_header[SFDP_HEADER_SIZE];
        int status;
        int hdr_status;

        // Loop over Param Headers and parse them (currently supports Basic Param Table and Sector Region Map Table)
        for (int i_ind = 0; i_ind < number_of_param_headers; i_ind++) {
            status = _spi_send_read_sfdp_command(addr, param_header, data_length);
            if (status < 0) {
                /*tr_error("Retrieving a parameter header %d failed", i_ind + 1);*/
                return -1;
            }

            hdr_status = sfdp_parse_single_param_header((sfdp_prm_hdr *)param_header, sfdp_info);
            if (hdr_status < 0) {
                return hdr_status;
            }

            addr += SFDP_HEADER_SIZE;
        }
    }

    return 0;
}

/**********************************************************************************/

int Flash::sfdp_parse_sector_map_table(sfdp_smptbl_info &smptbl)
{
    uint8_t sector_map_table[SFDP_BASIC_PARAMS_TBL_SIZE]; /* Up To 20 DWORDS = 80 Bytes */
    uint32_t tmp_region_size = 0;
    int i_ind = 0;
    int prev_boundary = 0;
    // Default set to all type bits 1-4 are common
    int min_common_erase_type_bits = SFDP_ERASE_BITMASK_ALL;

    int status = _spi_send_read_sfdp_command(smptbl.addr, sector_map_table, smptbl.size);
    if (status < 0) 
    {
        /*tr_error("Sector Map: Table retrieval failed");*/
        return -1;
    }

    // Currently we support only Single Map Descriptor
    if (!((sector_map_table[0] & 0x3) == 0x03) && (sector_map_table[1] == 0x0)) 
    {
        /*tr_error("Sector Map: Supporting Only Single Map Descriptor (not map commands)");*/
        return -1;
    }

    smptbl.region_cnt = sector_map_table[2] + 1;
    if (smptbl.region_cnt > SFDP_SECTOR_MAP_MAX_REGIONS) 
    {
        /*tr_error("Sector Map: Supporting up to %d regions, current setup to %d regions - fail",
                 SFDP_SECTOR_MAP_MAX_REGIONS,
                 smptbl.region_cnt);*/
        return -1;
    }

    // Loop through Regions and set for each one: size, supported erase types, high boundary offset
    // Calculate minimum Common Erase Type for all Regions
    for (i_ind = 0; i_ind < smptbl.region_cnt; i_ind++) {
        tmp_region_size = ((*((uint32_t *)&sector_map_table[(i_ind + 1) * 4])) >> 8) & 0x00FFFFFF; // bits 9-32
        smptbl.region_size[i_ind] = (tmp_region_size + 1) * 256; // Region size is 0 based multiple of 256 bytes;
        smptbl.region_erase_types_bitfld[i_ind] = sector_map_table[(i_ind + 1) * 4] & 0x0F; // bits 1-4
        min_common_erase_type_bits &= smptbl.region_erase_types_bitfld[i_ind];
        smptbl.region_high_boundary[i_ind] = (smptbl.region_size[i_ind] - 1) + prev_boundary;
        prev_boundary = smptbl.region_high_boundary[i_ind] + 1;
    }

    // Calc minimum Common Erase Size from min_common_erase_type_bits
    uint8_t type_mask = SFDP_ERASE_BITMASK_TYPE1;
    for (i_ind = 0; i_ind < 4; i_ind++) {
        if (min_common_erase_type_bits & type_mask) {
            smptbl.regions_min_common_erase_size = smptbl.erase_type_size_arr[i_ind];
            break;
        }
        type_mask = type_mask << 1;
    }

    if (i_ind == 4) {
        // No common erase type was found between regions
        smptbl.regions_min_common_erase_size = 0;
    }

    return 0;
}

/**********************************************************************************/

/**********************************************************************************/

int Flash::_sfdp_parse_basic_param_table(sfdp_hdr_info &sfdp_info)
{
    uint8_t param_table[SFDP_BASIC_PARAMS_TBL_SIZE]; /* Up To 20 DWORDS = 80 Bytes */

    int status = _spi_send_read_sfdp_command(sfdp_info.bptbl.addr, param_table, sfdp_info.bptbl.size);
    if (status != SPIF_OK) {
        return -1;
    }

    // Check address size, currently only supports 3byte addresses
    if ((param_table[2] & 0x4) != 0 || (param_table[7] & 0x80) != 0) {
        return -1;
    }

    // Get device density (stored in bits - 1)
    uint32_t density_bits = ((param_table[7] << 24) |
                             (param_table[6] << 16) |
                             (param_table[5] << 8) |
                             param_table[4]);
    sfdp_info.bptbl.device_size_bytes = (density_bits + 1) / 8;

    // Set Default read/program/erase Instructions
    m_read_instruction = SPIF_READ;
    m_prog_instruction = SPIF_PP;
    m_erase_instruction = SPIF_SE;

    // Set Page Size (SPI write must be done on Page limits)
    m_page_size_bytes = sfdp_detect_page_size(param_table, sfdp_info.bptbl.size);

    // Detect and Set Erase Types
    if (sfdp_detect_erase_types_inst_and_size(param_table, sfdp_info) < 0) {
        return -1;
    }

    m_erase_instruction = sfdp_info.bptbl.legacy_erase_instruction;

    // Detect and Set fastest Bus mode (default 1-1-1)
    _sfdp_detect_best_bus_read_mode(param_table, sfdp_info.bptbl.size, m_read_instruction);

    return 0;
}

/**********************************************************************************/

int Flash::_sfdp_detect_best_bus_read_mode(uint8_t *basic_param_table_ptr,
                                           uint32_t basic_param_table_size,
                                           uint8_t &read_inst)
{
    do {

        // TBD - SPIF Dual Read Modes Require SPI driver support
        m_read_dummy_and_mode_cycles = 0;
    } while (false);

    return 0;
}

/**********************************************************************************/

int32_t Flash::_reset_flash_mem()
{
    // Perform Soft Reset of the Device prior to initialization
    int32_t status = 0;
    uint8_t status_value[2] = {0};
    /*tr_info("_reset_flash_mem:");*/
    //Read the Status Register from device
    if (SPIF_OK != _spi_send_general_command(SPIF_RDSR, SPI_NO_ADDRESS_COMMAND, NULL, 0, status_value, 1)) 
    {
        // store received values in status_value
        status = -1;
    }

    if (0 == status) 
    {
        //Send Reset Enable
        if (SPIF_OK != _spi_send_general_command(SPIF_RSTEN, SPI_NO_ADDRESS_COMMAND, NULL, 0, NULL, 0)) 
        {
            status = -1;
        }

        if (0 == status) 
        {
            //Send Reset
            if (SPIF_OK != _spi_send_general_command(SPIF_RST, SPI_NO_ADDRESS_COMMAND, NULL, 0, NULL, 0)) 
            {
                status = -1;
            }
            
            if (false == _is_mem_ready()) 
            {
                status = -1;
            }
        }
    }

    return status;
}

/**********************************************************************************/

bool Flash::_is_mem_ready()
{
    // Check Status Register Busy Bit to Verify the Device isn't Busy
    uint8_t status_value[2];
    uint32_t retries = 0;
    bool mem_ready = true;

    do {
        retries++;
        //Read the Status Register from device
        SPIF_OK != _spi_send_general_command(SPIF_RDSR, 
                                             SPI_NO_ADDRESS_COMMAND, 
                                             NULL, 
                                             0, 
                                             status_value,
                                             1);

    } while ((status_value[0] & SPIF_STATUS_BIT_WIP) != 0 && retries < IS_MEM_READY_MAX_RETRIES);

    if ((status_value[0] & SPIF_STATUS_BIT_WIP) != 0) 
    {
        mem_ready = false;
    }
    return mem_ready;
}

/**********************************************************************************/

int32_t Flash::_set_write_enable()
{
    // Check Status Register Busy Bit to Verify the Device isn't Busy
    uint8_t status_value[2];
    int32_t status = -1;

    do {
        if (SPIF_OK !=  _spi_send_general_command(SPIF_WREN, SPI_NO_ADDRESS_COMMAND, 
                                                  NULL, 
                                                  0, 
                                                  NULL, 
                                                  0)) 
        {
            break;
        }

        if (false == _is_mem_ready()) 
        {
            break;
        }

        memset(status_value, 0, 2);
        if (SPIF_OK != _spi_send_general_command(SPIF_RDSR, 
                                                 SPI_NO_ADDRESS_COMMAND, 
                                                 NULL, 
                                                 0, 
                                                 status_value,
                                                 1)) 
        {
            break;
        }

        if ((status_value[0] & SPIF_STATUS_BIT_WEL) == 0) 
        {
            break;
        }
        status = 0;
    } while (false);
    return status;
}

/**********************************************************************************/
