#ifndef TIMERS_H
#define TIMERS_H

#include <hal\timer_hal.h>
class Timer
{
public:

    /**
      * @brief The constructor creates a Timer object and initilaze it
      * @param timer_name : timer to intilazie
      * @retval None
    */
    Timer(TimerName timer_name);

    /**
      * @brief The Destructor is detaching the the timer from the intrrupts
      * @retval None
    */
    ~Timer();

   /**
     * @brief Start the current timer
     * @retval TimerError - error number.
   */
    TimerError start();

    /**
      * @brief Get the current value of the timer
      * @param time in us
      * @retval TimerError - error number.
    */
    TimerError get_timer_us(us_time* us);

    /**
      * @brief Suspend the current timer
      * @retval TimerError - error number.
    */
    TimerError suspend();

    /**
      * @brief Resume the current timer
      * @retval TimerError - error number.
    */
    TimerError resume();     

    /**
      * @brief Enables the interrupt of the Timer
      * @param cb - The callback to run when interrupt occurs.
      * @param us - timeout in usec to trigger the callback.
      * @retval TimerError - error number.
    */
    TimerError attach_irq(timer_callback cb, us_time us);
    
    /**
      * @brief Disabling the Timer interrupt 
      * @retval TimerError - error number.
      *
    */
    TimerError detach_irq(void); 

private:
    timer_t timer;
        
};
#endif
