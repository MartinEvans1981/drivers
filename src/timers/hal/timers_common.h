#ifndef TIMERS_COMMON_H
#define TIMERS_COMMON_H
 
#include "ARMCM4_MII.h"
#include "timers_hw_defines.h"
#include "timers_definitions.h"


typedef void(* timer_callback)(void);
/********************************************************/

typedef enum
{
    TIMER_WD = 0,
    TIMER_2,
    TIMER_3
    
}TimerName;

/********************************************************/
typedef struct timer_s timer_t;

typedef void (*timer_irq_handler)(timer_t *obj);

struct timer_s
{
    TimerName                timer_name;         // TIMERx_OFFSET
    TimersPortmap *volatile  reg;
    uint32_t                 counter_load;       // Timer load counter value
    uint32_t                 wrappings_load;
    uint32_t                 wrapping_cnt;
    timer_callback           cb;
    timer_irq_handler       handler;
} ;

#endif