#include <ARMCM4_MII.h>
#include "timers_irq.h"


timer_t* timer_objects[NUM_TIMERS] = { 0 };


/**************************************************************/
/*                    GLOBAL IRQ                           */
/**************************************************************/

void timers_main_irq(timer_t * obj , timer_irq_handler handler)
{ 
   if(0 !=handler)
    {
        handler(obj);
    }
}

/**************************************************************/
/*                    SPECIFIC IRQs                           */
/**************************************************************/

void TIMER1_IRQHandler(void)
{
    timers_main_irq(timer_objects[TIMER_WD],
                    timer_objects[TIMER_WD]->handler);
   
}

/**************************************************************/

void TIMER2_IRQHandler(void)
{
    timers_main_irq(timer_objects[TIMER_2],
                    timer_objects[TIMER_2]->handler);
}
/**************************************************************/

void TIMER3_IRQHandler(void)
{
    timers_main_irq(timer_objects[TIMER_3],
                    timer_objects[TIMER_3]->handler);
}
