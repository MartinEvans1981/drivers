// Abstract: Configuration definition header file for DW_apb_timers
#define TIM_SYNC_DEPTH_7 2
#define TIM_SYNC_DEPTH_3 2
#define TIM_SYNC_DEPTH_2 2
#define TIM_SYNC_DEPTH_1 2
#define TIM_PULSE_EXTD_8 0
#define TIM_PULSE_EXTD_7 0
#define TIM_PULSE_EXTD_4 0
#define TIM_SYNC_DEPTH_8 2
#define TIM_PULSE_EXTD_3 0
#define TIMER_HAS_TOGGLE_2 0
#define TIMER_HAS_TOGGLE_3 1
#define TIM_COHERENCY_5 0
#define TIMER_HAS_TOGGLE_5 0
#define TIMER_WIDTH_6 32
#define TIM_SYNC_DEPTH_5 2
#define TIMER_0N100_PWM_HC_EN 0
#define TIMER_WIDTH_1 32
#define TIM_PULSE_EXTD_5 0
#define TIMER_HAS_TOGGLE_8 0
#define TIMER_WIDTH_4 32
#define NUM_TIMERS 3
#define INTR_SYNC2PCLK 0
#define SLVERR_RESP_EN 0
#define TIM_NEWMODE 1
#define APB_DATA_WIDTH 32
#define HC_PROT_LEVEL 0
#define TIM_COHERENCY_2 0
#define TIM_METASTABLE_8 1
#define TIM_INTR_IO 0
#define TIMER_HAS_TOGGLE_6 0
#define TIM_METASTABLE_7 1
#define TIM_SYNC_DEPTH_4 2
#define SLAVE_INTERFACE_TYPE 1
#define TIMER_WIDTH_8 32
#define TIM_METASTABLE_2 1
#define TIM_COHERENCY_8 0
#define TIM_SYNC_DEPTH_6 2
#define TIMER_HAS_TOGGLE_1 1
#define TIM_PULSE_EXTD_1 0
#define PROT_LEVEL_RST 0x2
#define TIMER_WIDTH_2 32
#define TIM_PULSE_EXTD_2 0
#define TIMER_WIDTH_3 32
#define TIMER_HAS_TOGGLE_7 0
#define TIMER_WIDTH_5 32
#define TIM_METASTABLE_3 1
#define TIM_0N100_PWM_MODE 0
#define TIMER_WIDTH_7 32
#define TIM_COHERENCY_1 0
#define TIM_COHERENCY_3 0
#define TIM_COHERENCY_4 0
#define TIM_COHERENCY_6 0
#define TIM_PULSE_EXTD_6 0
#define TIMER_HAS_TOGGLE_4 0
#define TIM_INTRPT_PLRITY 1
#define TIM_COHERENCY_7 0
#define TIM_METASTABLE_1 1
#define TIM_METASTABLE_4 1
#define TIM_METASTABLE_5 1
#define TIM_METASTABLE_6 1
