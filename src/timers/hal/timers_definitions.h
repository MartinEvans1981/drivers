#ifndef __TIMERS_DEFINITIONS__
#define __TIMERS_DEFINITIONS__

#include <stdint.h>
#include <stdbool.h>
#include "timers_hw_defines.h"
#include "ARMCM4_MII.h"

#define TIMER_BASE  TIMERs_MODULE_ADDR
#define TIMER_REG_DELTA_OFFSET 0x14
#define TIMER_IRQ_UNMASKED  0
#define TIMER_IRQ_MASKED 1
#define USER_DEFINED  1
#define FREE_RUNNING  0  
#define MAX_TIMERS_NUM 8
#define WD_TIMER_CLOCK 24000000
#define TIMER_CLOCK_2  24000000
#define TIMER_CLOCK_3  1000
#define TIMER_WD_MAX_VALUE 0xffffffff
#define TIMER_2_MAX_VALUE  0xffffffff
#define TIMER_3_MAX_VALUE  0xffffffff
#define TIMER_WD_WRAPING_TIME_USEC 1000000 * ((float)( TIMER_WD_MAX_VALUE) /( WD_TIMER_CLOCK))
#define TIMER2_WRAPING_TIME_USEC   1000000 * ((float)( TIMER_2_MAX_VALUE ) /( TIMER_CLOCK_2))
#define TIMER3_WRAPING_TIME_USEC   1000000 * ((float)( TIMER_3_MAX_VALUE ) /( TIMER_CLOCK_3))
#define RESERVE_TIMERS (MAX_TIMERS_NUM - NUM_TIMERS)
#define SIZEOF_TIMER_REGS_SIZE 5
#define RESERVE_TIMERS_SIZE  RESERVE_TIMERS * SIZEOF_TIMER_REGS_SIZE

/********************************************************/ 

typedef enum 
{
    DISABLE_TIMER = 0,
    ENABLE_TIMER
}TimerEnable;

/********************************************************/
typedef struct 
{
    uint32_t timer_clock[NUM_TIMERS];            // clock frequency of the N'th timer
    uint32_t wrapping_time_us [NUM_TIMERS];      // The timer counter wrapping to its maximum value
    bool     has_toggle [NUM_TIMERS];            // toggle output option
    uint8_t  timer_width[NUM_TIMERS];            // width of counter register
    bool     itr_io    ;                         // using one interrupt line 
    uint8_t timers_num;                          // number of timers in the HW

}Timers_HW_Params;

/********************************************************/

#define TIMER_PARAMS_HW_LOADER(prefix) {\
    prefix ## WD_TIMER_CLOCK,\
    prefix ## TIMER_CLOCK_2,\
    prefix ## TIMER_CLOCK_3,\
    prefix ## TIMER_WD_WRAPING_TIME_USEC,\
    prefix ## TIMER2_WRAPING_TIME_USEC,\
    prefix ## TIMER3_WRAPING_TIME_USEC,\
    prefix ## TIMER_HAS_TOGGLE_1,\
    prefix ## TIMER_HAS_TOGGLE_2,\
    prefix ## TIMER_HAS_TOGGLE_3,\
    prefix ## TIMER_WIDTH_1,\
    prefix ## TIMER_WIDTH_2,\
    prefix ## TIMER_WIDTH_3,\
    prefix ## TIM_INTR_IO,\
    prefix ## NUM_TIMERS\
}

/********************************************************/

typedef union// Control Register for Timer N Offset:  0x08 + (N-1)*0x14
{    
    uint32_t value; 
    struct 
    { 
        unsigned enable           :1;
        unsigned mode             :1;
        unsigned interrupt_mask   :1;
        unsigned pwm              :1;
        unsigned rsvd_timer_0n100pwm_en :1;
        unsigned rsvd_timerncontrolreg  :27;
    }fileds;
} TimerControlReg;

/********************************************************/ 

typedef volatile struct 
{
    struct 
    {
        uint32_t         load_count;              // Timer N Load Count Register Offset:  0x00 + (N-1)*0x14
        uint32_t         current_value;           // Current value of Timer N Offset:  0x04 + (N-1)*0x14
        TimerControlReg creg;                    // Control Register
        uint32_t  eoi;                            // Clears the interrupt from Timer N Offset: 0x0C + (N-1)*0x14
        uint32_t  int_status;                      // Timer N Interrupt Status Register Offset: 0x10 + (N-1)*0x14
    }timer_regs[NUM_TIMERS];
    uint32_t  rsv_38_A0 [RESERVE_TIMERS_SIZE ];
    uint32_t  timers_int_status;                  // Timers Interrupt Status Register Offset: 0xa0
    uint32_t  timers_eoi;                         // Timers End-of-Interrupt Register Returns all zeroes (0) and clears all active interrupts Offset: 0xa4
    uint32_t  timers_raw_int_stat;                // Contains the unmasked interrupt status of all timers in the component Offset: 0xa8
    uint32_t  timers_comp_version;                // Current revision number of the DW_apb_timers component Offset: 0xac
    uint32_t  load_count2[NUM_TIMERS];            // Value to be loaded into Timer N when toggle output changes from 0 to 1 Offset: 0xb0 + (N-1)*0x04

}TimersPortmap;

#endif