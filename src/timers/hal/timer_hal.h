#ifndef TIMERS_HAL_H
#define TIMERS_HAL_H
#include <stdint.h>
#include <stdbool.h>
#include "timers_irq.h"

/********************************************************/


#ifdef __cplusplus
extern "C" {
#endif

/********************************************************/    
// timers are 32 bit width 
#define TIMER_MAX_VALUE 0xFFFFFFFF
typedef uint64_t us_time;
typedef uint32_t ms_time;

/********************************************************/

typedef enum 
{
    TIMER_OK = 0,
    TIMER_INVALID_ARGU,
    TIMER_DISABLED,
    TIMER_BUSY,
    TIMER_UNKNOWN_ERROR
}TimerError;

/********************************************************/

/** Initialize the timer through the TimerNControlReg register.
  * @param obj - The timer object.
  * @retval TimerError - error number.
 */
TimerError timer_init(timer_t * obj);

/********************************************************/

/** Set the event handler and unmasking the interrupt
  * @param obj - The timer object.
  * @param cb - timer_callback function
  * @param timeout - timout in usec to trigger the callback.
  * @retval TimerError - error number.
  * @note due to long relativly long setup time the timer will have 
  *       a delay of opproximitly 19uS.
  */
TimerError timer_attach_irq( timer_t *const obj, timer_callback cb ,us_time timeout);

/********************************************************/

/** detaching the event handler and masking the timer's interrupt.
  * @param obj - The timer object.
  * @retval TimerError - error number.
  */
TimerError timer_detach_irq( timer_t *const obj);

/********************************************************/

/** Read the relative timer's value.
  * @param obj - The timer object.
  * @param The relative value of the timer
  * @retval TimerError - error number.
  */
TimerError timer_read_us(const timer_t *const obj, us_time *us_val);
    
/********************************************************/

/** Read the current (absolute) timer's value.
 * if the counter is disabled the counter will return its load value 
 * @param obj - The timer object.
 * @param return parameter The timer counter current value.
 * @retval TimerError - error number.
 */
TimerError timer_read(const timer_t *const obj, uint32_t *value);

/********************************************************/

/** Start this timer
  * @param obj - The timer object.
  * @retval TimerError - error number.
  */
TimerError timer_start( timer_t *const obj);

/********************************************************/

/** Suspend this timer
  * When suspended, reads will always return the same time and no
  * events will be dispatched. When suspended the common layer
  * will only ever call the interface function clear_interrupt()
  * and that is only if timer_irq_handler is called.
  * @param obj   The timer object.
  * @retval TimerError - error number.
  */
TimerError timer_suspend(const timer_t *const obj);

/********************************************************/

/** Resume this timer
  * When resumed the timer will ignore any time that has passed
  * and continue counting down where it left off.
  *
  * @param obj - The timer object.
  * @retval TimerError - error number.
  */
TimerError timer_resume(const timer_t *const obj);

/********************************************************/

/** Convert from us to clock timer clock cycles
  * @param obj - The timer object.
  * @param us - The time in usec to convert.
  * @param abs - the converted return param.
  * @retval TimerError - error number.
  */
TimerError timer_convert_us_to_abs(const timer_t *const obj,us_time us , uint32_t *abs);

/********************************************************/

/** convert the clock numbers to uSec
  * @param obj - The timer object.
  * @param abs - The time in clock cycles to convert.
  * @param us - The converted return param.
  * @retval TimerError - error number.
  */
TimerError timer_convert_abs_to_us(const timer_t *const obj, uint32_t abs, us_time*  us);

/********************************************************/

#ifdef __cplusplus
}
#endif

#endif
