#include "timer_hal.h"
#include "timers_definitions.h"

/********************************************************/
static const Timers_HW_Params timers_hw_params = TIMER_PARAMS_HW_LOADER();
static const IRQn_Type timers_irqn_vector [NUM_TIMERS] ={TIMER_WD_IRQn, TIMER2_IRQn, TIMER3_IRQn};
// Internal auxiliary functions:
static inline TimerError timer_verify(const timer_t *const obj);
void timer_user_defined_timeout_isr( timer_t * obj);
void timer_user_defined_wrappings_isr( timer_t * obj);
void timer_free_runing_isr(timer_t * obj);
static inline bool is_timer_enabled(const timer_t *const obj);
static inline void timer_unmask_irq(const timer_t *const obj);
static inline void timer_mask_irq(const timer_t *const obj);
TimerError timer_read(const timer_t *const obj, uint32_t *value);

/**************************************************************/
/*                                                            */
/*                                                            */
/*                    General functionality                   */
/*                                                            */
/*                                                            */
/*                                                            */
/**************************************************************/

TimerError timer_init( timer_t * obj)
{
    TimerError result = timer_verify(obj);
    TimerControlReg control;
    if(result != TIMER_OK)
    {
        goto cleanup;
    }
    timer_mask_irq(obj);
    
    obj->wrapping_cnt = 0;
    
    //setting the irq handler
    
    obj->handler = timer_free_runing_isr;
    obj->cb = 0;
    timer_objects[obj->timer_name] = obj;
    obj->reg->timer_regs[obj->timer_name].creg.fileds.enable = DISABLE_TIMER;
    obj->reg->timer_regs[obj->timer_name].creg.fileds.mode = FREE_RUNNING;
cleanup:
    return result;
    
}

/********************************************************/

TimerError timer_attach_irq( timer_t *const obj, timer_callback cb ,us_time timeout)
{
    us_time load_cnt_us = 0;
    TimerError result = timer_verify(obj);
    if(result != TIMER_OK)
    {
        goto cleanup;
    }
    obj->cb = cb;
    // reset wrapping value
    obj->wrapping_cnt = 0;
    // changing the timer mode to user defined
    obj->reg->timer_regs[obj->timer_name].creg.fileds.mode = USER_DEFINED;
    // unmasking the timer irq
    timer_unmask_irq(obj);
   
    // calculate the number of wrapping load 
    obj->wrappings_load = timeout / timers_hw_params.wrapping_time_us[obj->timer_name];
    // calculate the remaining part of the last wraping 
    load_cnt_us = timeout % timers_hw_params.wrapping_time_us[obj->timer_name];
    // converting it to absulute value and loading the load count register
    timer_convert_us_to_abs(obj, load_cnt_us, &obj->counter_load);
    // check if the timout is bigger than max wrapping size
    if(obj->wrappings_load > 0)
    {
        // loading the load count register to its maximum value
        obj->reg->timer_regs[obj->timer_name].load_count = (1 << timers_hw_params.timer_width[obj->timer_name]) - 1 ;
        obj->handler = timer_user_defined_wrappings_isr;
        // enabling the timer
        obj->reg->timer_regs[obj->timer_name].creg.fileds.enable = ENABLE_TIMER;
        // load the loadcount register for the last partial wrapping
        if(obj->wrappings_load == 1)
        {
            obj->reg->timer_regs[obj->timer_name].load_count = obj->counter_load;
        }
    }
    else
    {
        obj->reg->timer_regs[obj->timer_name].load_count = obj->counter_load;
        obj->handler = timer_user_defined_timeout_isr;
        // enabling the timer
        obj->reg->timer_regs[obj->timer_name].creg.fileds.enable = ENABLE_TIMER;
    }
cleanup:
    return result;
}

/********************************************************/

TimerError timer_detach_irq( timer_t *const obj)
{
    TimerError result = timer_verify(obj);;
    if(result != TIMER_OK)
    {
        goto cleanup;
    }
    obj->reg->timer_regs[obj->timer_name].creg.fileds.enable = DISABLE_TIMER;
    timer_mask_irq(obj);
    obj->cb = 0;
cleanup:
    return result;
}

/********************************************************/

TimerError timer_read_us(const timer_t *const obj, us_time *us_val)
{
    TimerError result = TIMER_OK;
    uint32_t current_time = 0;
    uint32_t wrapping_time_us = 0;
    if(!obj)
    {
        result = TIMER_INVALID_ARGU;
        goto cleanup;
    }
    if(is_timer_enabled(obj))
    {
        current_time = obj->reg->timer_regs[obj->timer_name].current_value;
    }
    else 
    {
        current_time = obj->reg->timer_regs[obj->timer_name].load_count;
    }
    uint32_t abs = obj->counter_load - current_time ;
    wrapping_time_us = timers_hw_params.wrapping_time_us[obj->timer_name] * obj->wrapping_cnt;
    timer_convert_abs_to_us(obj, abs, us_val);
    *us_val += wrapping_time_us;
cleanup:
    return result;
}

/********************************************************/

TimerError timer_start( timer_t *const obj)
{
    TimerError result = timer_verify(obj);;
    if(result != TIMER_OK)
    {
        goto cleanup;
    }
    obj->counter_load = (1 << timers_hw_params.timer_width[obj->timer_name]) - 1 ;
    obj->wrapping_cnt = 0;
    // Setting the handler
    obj->handler = timer_free_runing_isr;
    timer_unmask_irq(obj);
    obj->reg->timer_regs[obj->timer_name].load_count = obj->counter_load;
    obj->reg->timer_regs[obj->timer_name].creg.fileds.mode = FREE_RUNNING;
    //enable the timer
    obj->reg->timer_regs[obj->timer_name].creg.fileds.enable = ENABLE_TIMER;
cleanup:
    return result;
}

/********************************************************/

TimerError timer_suspend(const timer_t *const obj)
{
    TimerError result = TIMER_OK;
    uint32_t current_time = 0;
    if(!obj)
    {
        result = TIMER_INVALID_ARGU;
        goto cleanup;
    }
    if(!is_timer_enabled(obj))
    {
        result = TIMER_DISABLED;
        goto cleanup;
    }
    // we need to perform a read before disabling the timer.
    // because the current value register will reset its value right after we disable the timer.
    current_time = obj->reg->timer_regs[obj->timer_name].current_value;
    //loading the loadCounter register
    obj->reg->timer_regs[obj->timer_name].load_count = current_time;
    // starting the timer, unmasking interrupts
    obj->reg->timer_regs[obj->timer_name].creg.fileds.enable = DISABLE_TIMER;
cleanup:
    return result;
}

/********************************************************/

TimerError timer_resume(const timer_t *const obj)
{
    TimerError result = timer_verify(obj);;
    if(result != TIMER_OK)
    {
        goto cleanup;
    }
    // starting the timer, note that the timers loading count register is 
    // already loaded in the suspend function.
    obj->reg->timer_regs[obj->timer_name].creg.fileds.enable = ENABLE_TIMER;
cleanup:
    return result;
}

/********************************************************/

TimerError timer_convert_us_to_abs(const timer_t *const obj,us_time us , uint32_t *abs)
{
    TimerError result = TIMER_OK;
    float frac = 0;
    uint32_t clock = 0;
    if(!obj)
    {
        result = TIMER_INVALID_ARGU;
         goto cleanup;
    }
    clock = timers_hw_params.timer_clock[obj->timer_name];  
    frac =  ((float)us/1000000);
    *abs = frac * clock;
cleanup:
    return result;
}

/********************************************************/

TimerError timer_convert_abs_to_us(const timer_t *const obj, uint32_t abs, us_time*  us)
{
    TimerError result = TIMER_OK;
    double frac = 0;
    uint32_t clock = 0;
    if(!obj)
    {
        result = TIMER_INVALID_ARGU;
         goto cleanup;
    }
    clock = timers_hw_params.timer_clock[obj->timer_name];
    frac = (double) 1000000/clock;
    frac *= abs; 
    *us = (us_time) frac;
cleanup:
    return result;
}

/**************************************************************/
/*                                                            */
/*                                                            */
/*                    Interrupt services                      */
/*                                                            */
/*                                                            */
/*                                                            */
/**************************************************************/

void timer_user_defined_wrappings_isr(timer_t * obj)
{
    uint32_t clear = 0;
    //clearing the interrupt
    clear = obj->reg->timer_regs[obj->timer_name].eoi;
    //last wrapping check
    obj->wrapping_cnt += 1;
     if(obj->wrapping_cnt == obj->wrappings_load - 1)
    {
         obj->reg->timer_regs[obj->timer_name].load_count = obj->counter_load;
    }
    else 
    {
       if(obj->wrapping_cnt == obj->wrappings_load)
       {
            obj->handler = timer_user_defined_timeout_isr;
       }
    }
}

/**************************************************************/


void timer_user_defined_timeout_isr(timer_t * obj)
{
    uint32_t clear = 0;
    clear = obj->reg->timer_regs[obj->timer_name].eoi; // check if read only register/
    obj->reg->timer_regs[obj->timer_name].creg.fileds.enable = DISABLE_TIMER;
    if(obj->cb)    
    {
        obj->cb();   
    }
}

/**************************************************************/

void timer_free_runing_isr(timer_t * obj)
{
    uint32_t clear = 0;
    clear = obj->reg->timer_regs[obj->timer_name].eoi;
    obj->wrapping_cnt += 1;
}

/**************************************************************/
/*                                                            */
/*                                                            */
/*                    Auxiliary services                      */
/*                                                            */
/*                                                            */
/*                                                            */
/**************************************************************/
static inline TimerError timer_verify(const timer_t *const obj)
{
    TimerError result = TIMER_OK;
    if(!obj)
    {
        result = TIMER_INVALID_ARGU;
        goto cleanup;
    }
    if(obj->timer_name > NUM_TIMERS)
    {
        result = TIMER_UNKNOWN_ERROR;
        goto cleanup;
    }
    if(is_timer_enabled(obj))
    {
        result = TIMER_BUSY; 
        goto cleanup;
    }
cleanup:
    return result;
}
   
/********************************************************/

static inline bool is_timer_enabled(const timer_t *const obj)
{
    return (bool) obj->reg->timer_regs[obj->timer_name].creg.fileds.enable;
}

/********************************************************/

static inline void timer_mask_irq(const timer_t *const obj)
{
    IRQn_Type irq = timers_irqn_vector[obj->timer_name];
    obj->reg->timer_regs[obj->timer_name].creg.fileds.interrupt_mask = TIMER_IRQ_MASKED;
    NVIC_DisableIRQ(irq);
    NVIC_ClearPendingIRQ(irq);
}

/********************************************************/

static inline void timer_unmask_irq(const timer_t *const obj)
{
    IRQn_Type irq = timers_irqn_vector[obj->timer_name];
    obj->reg->timer_regs[obj->timer_name].creg.fileds.interrupt_mask = TIMER_IRQ_UNMASKED;
    NVIC_ClearPendingIRQ(irq);
    NVIC_EnableIRQ(irq);
    
}

