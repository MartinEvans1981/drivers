#include "Timers.h"

Timer::Timer(TimerName timer_name)
{
    timer.timer_name = timer_name;
    timer.counter_load = 0;
    timer.reg  = (TimersPortmap *) (TIMER_BASE);
    if (timer_init(&timer) !=  TIMER_OK)
    {
        while(1)
        {
             // We should implement a
            // common die function (like mbed_die)
        }
    }
    
}

/********************************************************/

Timer::~Timer()
{
    detach_irq();
}

/********************************************************/

TimerError Timer::start()
{
    TimerError result;
    result =  timer_start(&timer);
    return result;
    
    
}

/********************************************************/

TimerError Timer::get_timer_us(us_time* us)
{
    TimerError result = TIMER_OK;
    result = timer_read_us(&timer, us);
    return result;
}

/********************************************************/

TimerError Timer::suspend()
{
    return timer_suspend(&timer);
}

/********************************************************/

TimerError Timer::resume()
{
    return timer_resume(&timer);
}

/********************************************************/

TimerError Timer::attach_irq(timer_callback cb, us_time us)
{
    TimerError result = TIMER_OK;
    result = timer_attach_irq(&timer, cb, us);
    return result;
}

/********************************************************/

TimerError Timer::detach_irq()
{

    return timer_detach_irq(&timer);
}

