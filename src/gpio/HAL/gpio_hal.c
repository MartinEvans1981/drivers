#include "gpio_hal.h"
#include "gpio_irq.h"


/********************************************************/

// Base address of the GPIO peripheral
#define BASE_ADDRESS 0x400C0000

// Register size of the GPIO contril register
#define REG_SIZE (sizeof(GpioInitDef))

/********************************************************/

void gpio_init(PinName pin, GpioInitDef params)
{
    GpioInitDef *reg = (GpioInitDef*)(BASE_ADDRESS + (pin * REG_SIZE));

    *reg = params;
}

/********************************************************/

uint8_t gpio_read(PinName pin)
{
    GpioInitDef *reg = (GpioInitDef*)(BASE_ADDRESS  + (pin * REG_SIZE));

    return reg->fields.din;
}

/********************************************************/

void gpio_write(PinName pin, uint8_t state)
{
    GpioInitDef *reg = (GpioInitDef*)(BASE_ADDRESS  + (pin * REG_SIZE));

    reg->fields.dout = state;
}

/********************************************************/

void gpio_toggle(PinName pin)
{
    GpioInitDef *reg = (GpioInitDef*)(BASE_ADDRESS  + (pin * REG_SIZE));

    reg->fields.dout = !reg->fields.dout;
}

/********************************************************/

void gpio_set_mode(PinName pin, GpioPull mode)
{
    GpioInitDef *reg = (GpioInitDef*)(BASE_ADDRESS  + (pin * REG_SIZE));

    if(NO_PULL == mode)
    {
        reg->fields.pu = 0;
        reg->fields.pd = 0;
    }
    else if(PULL_UP == mode)
    {
        reg->fields.pu = 1;
    }
    else
    {
       reg->fields.pd = 1; 
    }
}

/********************************************************/

void gpio_set_direction(PinName pin, GpioDirection direction)
{
    GpioInitDef *reg = (GpioInitDef*)(BASE_ADDRESS  + (pin * REG_SIZE));

    reg->fields.oen = direction;
}

/********************************************************/

void gpio_attach_irq(PinName pin, GpioInterruptMode mode, interrupt_handler irq)
{
    GpioInitDef *reg = (GpioInitDef*)(BASE_ADDRESS  + (pin * REG_SIZE));
    
    reg->fields.int_mode = mode;


    gpio_interrupt_handlers[pin] = irq;
}


/********************************************************/

void gpio_detach_irq(PinName pin)
{
    GpioInitDef *reg = (GpioInitDef*)(BASE_ADDRESS  + (pin * REG_SIZE));
    
    reg->fields.int_mode = NO_IRQ;

    // Nullify the current pin irq
    gpio_interrupt_handlers[pin] = 0;
}

/********************************************************/

void gpio_set_alternate_function(PinName pin, uint8_t function)
{
    GpioInitDef *reg = (GpioInitDef*)(BASE_ADDRESS  + (pin * REG_SIZE));
    
    reg->fields.af = function;
}

/********************************************************/

