#include <ARMCM4_MII.h>
#include "gpio_irq.h"



interrupt_handler gpio_interrupt_handlers[NUM_OF_GPIOS] = { 0 };


/**************************************************************/
/*                    GENERAL IRQ                             */
/**************************************************************/

void gpio_main_irq(PinName pin)
{
    if(0 != gpio_interrupt_handlers[pin])
    {
        gpio_interrupt_handlers[pin]();
    }
}


/**************************************************************/
/*                    SPECIFIC IRQs                           */
/**************************************************************/

void GPIO_UART_OUT4_IRQHandler(void)
{
    gpio_main_irq(UART_OUT4_PIN);
}

/**************************************************************/

void GPIO_UART_OUT3_IRQHandler(void)
{
    gpio_main_irq(UART_OUT3_PIN);
}

/**************************************************************/

void GPIO_UART_OUT2_IRQHandler(void)
{
    gpio_main_irq(UART_OUT2_PIN);
}

/**************************************************************/

void GPIO_UART_OUT1_IRQHandler(void)
{
    gpio_main_irq(UART_OUT1_PIN);
}

/**************************************************************/

void GPIO_UART_IN4_IRQHandler(void)
{
    gpio_main_irq(UART_IN4_PIN);
} 

/**************************************************************/

void GPIO_UART_IN3_IRQHandler(void)
{
    gpio_main_irq(UART_IN3_PIN);
} 

/**************************************************************/

void GPIO_UART_IN2_IRQHandler(void)
{
    gpio_main_irq(UART_IN2_PIN);
} 

/**************************************************************/

void GPIO_UART_IN1_IRQHandler(void)
{
    gpio_main_irq(UART_IN1_PIN);
} 

/**************************************************************/

void GPIO_SPI_SCK_IRQHandler(void)
{
    gpio_main_irq(SPI_SCK_PIN);
}  

/**************************************************************/

void GPIO_SPI_DOUT_IRQHandler(void)
{
    gpio_main_irq(SPI_DOUT_PIN);
} 

/**************************************************************/

void GPIO_SPI_DIN_IRQHandler(void)
{
    gpio_main_irq(SPI_DIN_PIN);
}  

/**************************************************************/

void GPIO_SPI_CS_0_IRQHandler(void)
{
    gpio_main_irq(SPI_CS_0_PIN);    
} 

/**************************************************************/

void GPIO_H_FN_IRQHandler(void)
{
    gpio_main_irq(H_FN_PIN);
}   

/**************************************************************/

void GPIO_DE_IRQHandler(void)
{
    gpio_main_irq(DE_PIN);
}   

/**************************************************************/
