#ifndef GPIO_IRQ_H
#define GPIO_IRQ_H
#include "PinNames.h"

extern interrupt_handler gpio_interrupt_handlers[NUM_OF_GPIOS];

#endif

