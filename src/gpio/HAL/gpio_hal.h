#ifndef GPIO_HAL_H
#define GPIO_HAL_H

#include <ARMCM4_MII.h>
#include <stdint.h>

#include "PeripheralPins.h"

/********************************************************/

#ifdef __cplusplus
extern "C" {    // allow C++ to use these headers
#endif

/********************************************************/

typedef enum
{
    NO_PULL = 0,
    PULL_UP,
    PULL_DOWN
}GpioPull;

/********************************************************/

typedef enum
{
    OUTPUT = 0,
    INPUT,
}GpioDirection;

/********************************************************/ 

typedef enum
{
    NO_IRQ       = 0,
    RISING_EDGE  = 0x04,
    FALLING_EDGE = 0x05,
    HIGH_LEVEL   = 0x06,
    LOW_LEVEL    = 0x07
}GpioInterruptMode;      

/********************************************************/

typedef union
{
    uint32_t value; 
    struct 
    { 
        unsigned af :3;       // Alternate Function select/
        unsigned ds :1;       // Drive Strength.
        unsigned ie :1;       // Input Enable
        unsigned oen :1;      // Output Enable
        unsigned pd :1;       // Pull-Up
        unsigned pu :1;       // Pull-Down
        unsigned dout :1;     // Output Value
        unsigned din :1;      // Input Value
        unsigned int_mode :3; // Interrrupt Value
        unsigned rsv1 :19;    // Reserved
    }fields;
} GpioInitDef;

/********************************************************/

/**
  * @brief Initialize the GPIO with the given params.
  * @param pin - the GPIO you want to initialize 
  * @param params - the params you want to initialize the
  *                 gpio with(look inside `GpioInitDef`)
  * @retval None
  */
void gpio_init(PinName pin, GpioInitDef params);

/********************************************************/

/**
  * @brief Read the GPIO current value
  * @param pin - the GPIO you want to read
  * @retval 1 - set / 0 - reset
  */
uint8_t gpio_read(PinName pin);

/********************************************************/

/**
  * @brief Outputs the requested value to the GPIO.
  * @param pin - the GPIO you want to write into  
  * @param state - 1 - set / 0 - reset
  * @retval None.
  */
void gpio_write(PinName pin, uint8_t state);

/********************************************************/

/**
  * @brief Toggles the current GPIO's output value.
  * @param pin - the GPIO you want to toggle  
  * @retval None.
  */
void gpio_toggle(PinName pin);

/********************************************************/

/**
  * @brief Configure pull mode of the requested GPIO
  * @param pin - the GPIO you want to configure  
  * @param mode - the pull mode you want to set 
  *               for this GPIO  
  * @retval None.
  */
void gpio_set_mode(PinName pin, GpioPull mode);

/********************************************************/

/**
  * @brief Configures the given GPIO as input/output
  * @param pin - the GPIO you want to configure
  * @param direction - input / output
  * @retval 
  */
void gpio_set_direction(PinName pin, GpioDirection direction);

/********************************************************/

/**
  * @brief Set the requestes interrupt mode to the GPIO.
  * @param pin - the GPIO you want to configure
  * @param mode - the requsted interrupt mode
  * @param irq - the requsted callback when the GPIO's 
  *              interrupt occurs
  * @retval None.
  */
void gpio_attach_irq(PinName pin, GpioInterruptMode mode, interrupt_handler irq);

/********************************************************/

/**
  * @brief Disable the interrupt of the GPIO.
  * @param pin - the GPIO you want to configure
  * @retval None.
  */
void gpio_detach_irq(PinName pin);

/********************************************************/

/**
  * @brief Set the alternated function of the given GPIO
  *        look inside `PeripheralPins.c` for the full
  *        GPIO's AFs list
  * @param pin - the GPIO you want to configure
  * @param function - `TODO`
  * @retval None.
  */
void gpio_set_alternate_function(PinName pin, uint8_t function);

/********************************************************/

#ifdef __cplusplus
}
#endif

#endif
