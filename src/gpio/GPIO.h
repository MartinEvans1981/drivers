#ifndef GPIO_H
#define GPIO_H

#include <gpio_hal.h>

class GPIO
{
public:

    /**
      * @brief Create a GPIO connected to the specified pin
      * @param pin - pin to connect to
      * @retval None
    */
    GPIO(PinName pin);
    ~GPIO();

    /**
      * @brief Return the output setting, represented as 0 or 1
      * @retval an integer representing the output setting of 
      *         the pin if it is an output, or read the input
      *         if set as an input 
    */
    uint8_t read();

    /**
      * @brief Set the output, specified as 0 or 1
      * @param value - An integer specifying the pin output value,
      *                0 for logical 0, 1 for logical 1
    */
    void write(uint8_t state);

    /**
      * @brief Toggles the output of the pin if set as output
    */
    void toggle();

    /**
      * @brief Sets the pull mode for this GPIO
      * @param mode - The pull mode you want to configure
      *               for this GPIO 
    */
    void set_mode(GpioPull mode);
    /**
      * @brief Set the alternated function.
      *        look inside `PeripheralPins.c` for the full
      *        GPIO's AFs list
      * @param function - `TODO`
      * @retval true for success / false for failure
    */
    bool set_alternate_fucntion(PinFunc function);

    /**
      * @brief Set as an output 
    */
    void set_output();

    /**
      * @brief Set the input pin mode
    */
    void set_input();

    /**
      * @brief Enables the interrupt of th GPIO
      * @param mode - The required mode of interrupt
      * @param irq - The callback to run when interrupt occurs
    */
    void attach_irq(GpioInterruptMode mode, interrupt_handler irq);

    /**
      * @brief Disables the interrupt of the GPIO
    */
    void detach_irq();

private:
    
    PinName m_pin;
};

#endif

