#include <GPIO.h>

/********************************************************/

/*                     INTERNAL                         */

/********************************************************/
              
// A table to easily find the
// matchin IRQ of the current gpio
const IRQn_Type pin_irq[] = {
    GPIO_DE_IRQn,       
    GPIO_H_FN_IRQn,     
    GPIO_SPI_CS_0_IRQn, 
    GPIO_SPI_DIN_IRQn,  
    GPIO_SPI_DOUT_IRQn, 
    GPIO_SPI_SCK_IRQn,  
    GPIO_UART_IN1_IRQn, 
    GPIO_UART_IN2_IRQn, 
    GPIO_UART_IN3_IRQn, 
    GPIO_UART_IN4_IRQn, 
    GPIO_UART_OUT1_IRQn,
    GPIO_UART_OUT2_IRQn,
    GPIO_UART_OUT3_IRQn,
    GPIO_UART_OUT4_IRQn
};

/********************************************************/

/*                     IMPLEMENTATION                   */

/********************************************************/

GPIO::GPIO(PinName pin) : m_pin(pin)
{
    GpioInitDef params = { 0 };

    // Always set the ability to read
    // the pin value.
    params.fields.ie = 1;
    
    // Defaultly use a weak Pull-Up
    // on the pins so it won't float
    params.fields.pu = 1;

    gpio_init(m_pin, params);

    detach_irq();
}

/********************************************************/

GPIO::~GPIO()
{
    detach_irq();    
}

/********************************************************/

uint8_t GPIO::read()
{
    return gpio_read(m_pin);
}

/********************************************************/

void GPIO::write(uint8_t state)
{
    gpio_write(m_pin, state);
}

/********************************************************/

void GPIO::toggle()
{
   gpio_toggle(m_pin);
}

/********************************************************/

void GPIO::set_mode(GpioPull mode)
{
    gpio_set_mode(m_pin, mode);
}

/********************************************************/

void GPIO::set_output()
{
    gpio_set_direction(m_pin, OUTPUT);
}

/********************************************************/

void GPIO::set_input()
{
    gpio_set_direction(m_pin, INPUT);
}

/********************************************************/

bool GPIO::set_alternate_fucntion(PinFunc function)
{
    bool result = false;

    for(uint8_t i = 0; i < sizeof(pin_func_map[m_pin]); i++)
    {
        if(pin_func_map[m_pin][i] == function)
        {
            // Disable interrupt if there is one
            // as this GPIO in now belong to a peripheral
            detach_irq();
            
            gpio_set_alternate_function(m_pin, i);
            
            result = true;
            break;
        }
    }

    return result;
}


/********************************************************/

void GPIO::attach_irq(GpioInterruptMode mode, interrupt_handler irq)
{
   NVIC_ClearPendingIRQ(pin_irq[m_pin]);
   NVIC_EnableIRQ(pin_irq[m_pin]);

   gpio_attach_irq(m_pin, mode, irq);
}

/********************************************************/

void GPIO::detach_irq()
{
   NVIC_ClearPendingIRQ(pin_irq[m_pin]);
   NVIC_DisableIRQ(pin_irq[m_pin]);

   gpio_detach_irq(m_pin);
}

/********************************************************/

