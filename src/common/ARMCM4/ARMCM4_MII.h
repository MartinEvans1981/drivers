/**************************************************************************//**
 * @file     ARMCM4_FP.h
 * @brief    CMSIS Core Peripheral Access Layer Header File for
 *           ARMCM4 Device (configured for CM4 with FPU)
 * @version  V5.3.1
 * @date     09. July 2018
 ******************************************************************************/
/*
 * Copyright (c) 2009-2018 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the License); you may
 * not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ARMCM4_FP_H
#define ARMCM4_FP_H

#ifdef __cplusplus
extern "C" {
#endif


/* -------------------------  Interrupt Number Definition  ------------------------ */

typedef enum IRQn
{
	/******  Cortex-M4 Processor Exceptions Numbers *******************************************/
    Reset_IRQn          = -15,						  /*!<   1  Reset Vector, invoked on Power up and warm reset */
	NonMaskableInt_IRQn   = -14,              /*!< 2 Non Maskable Interrupt                 */
  HardFault_IRQn        = -13,              /*!< 3 Cortex-M3 Hard Fault Interrupt         */
  MemoryManagement_IRQn = -12,              /*!< 4 Cortex-M3 Memory Management Interrupt  */
  BusFault_IRQn         = -11,              /*!< 5 Cortex-M3 Bus Fault Interrupt          */
  UsageFault_IRQn       = -10,              /*!< 6 Cortex-M3 Usage Fault Interrupt        */
  SVCall_IRQn           = -5,               /*!< 11 Cortex-M3 SV Call Interrupt           */
  DebugMonitor_IRQn     = -4,               /*!< 12 Cortex-M3 Debug Monitor Interrupt     */
  PendSV_IRQn           = -2,               /*!< 14 Cortex-M3 Pend SV Interrupt           */
  SysTick_IRQn          = -1,               /*!< 15 Cortex-M3 System Tick Interrupt       */

/**************   Peripheral Interrupt Numbers **********************************************/
  DMA_TFR_IRQn              = 0,  
  DMA_BLOCK_IRQn            = 1,  
  DMA_SRCTRNS_IRQn          = 2,  
  DMA_DSTTRNS_IRQn          = 3,  
  DMA_ERR_IRQn              = 4,  
  DMA_COMBI_IRQn            = 5,  
  GPIO_UART_OUT4_IRQn       = 6,  
  GPIO_UART_OUT3_IRQn       = 7,  
  GPIO_UART_OUT2_IRQn       = 8,  
  GPIO_UART_OUT1_IRQn       = 9,  
  GPIO_UART_IN4_IRQn        = 10, 
  GPIO_UART_IN3_IRQn        = 11, 
  GPIO_UART_IN2_IRQn        = 12, 
  GPIO_UART_IN1_IRQn        = 13, 
  GPIO_SPI_SCK_IRQn         = 14, 
  GPIO_SPI_DOUT_IRQn        = 15, 
  GPIO_SPI_DIN_IRQn         = 16, 
  GPIO_SPI_CS_0_IRQn        = 17, 
  GPIO_H_FN_IRQn            = 18, 
  GPIO_DE_IRQn              = 19, 
  UART3_IRQn                = 20, 
  UART2_IRQn                = 21, 
  UART1_IRQn                = 22, 
  UART0_IRQn                = 23, 
  TIMER3_IRQn               = 24, 
  TIMER2_IRQn               = 25,  
  TIMER_WD_IRQn               = 26, 
	SPI_S_TXO_IRQn      			= 27,
	SPI_S_TXE_IRQn            = 28,
	SPI_S_RXU_IRQn            = 29,
	SPI_S_RXO_IRQn            = 30,
	SPI_S_RXF_IRQn            = 31,
	SPI_M_TXO_IRQn            = 32,
	SPI_M_TXE_IRQn            = 33,
	SPI_M_RXU_IRQn            = 34,
	SPI_M_RXO_IRQn            = 35,
	SPI_M_RXF_IRQn            = 36,
	SPI_M_MST_IRQn            = 37,
	IC_SCL_STUCK_IRQn         = 38,
	IC_TX_OVER_IRQn           = 39,
	IC_TX_EMPTY_IRQn          = 40,
	IC_TX_ARB_IRQn            = 41,
	IC_STOP_DET_IRQn          = 42,
	IC_START_DET_IRQn         = 43,
	IC_RX_UNDER_IRQn          = 44,
	IC_RX_OVER_IRQn           = 45,
	IC_RX_FULL_IRQn           = 46,
	IC_RX_DONE_IRQn           = 47,
	IC_RD_REQ_IRQn            = 48,
	IC_GEN_CALL_IRQn          = 49,
	IC_ACTIVITY_IRQn          = 50,
	EFUSE_PROG_IRQn           = 51,
	CLK_1K_COUNT_IRQn         = 52,
	RS485_RX_DET_IRQn         = 53
	
} IRQn_Type;                      


/* ================================================================================ */
/* ================      Processor and Core Peripheral Section     ================ */
/* ================================================================================ */

/* -------  Start of section using anonymous unions and disabling warnings  ------- */
#if   defined (__CC_ARM)
  #pragma push
  #pragma anon_unions
#elif defined (__ICCARM__)
  #pragma language=extended
#elif defined(__ARMCC_VERSION) && (__ARMCC_VERSION >= 6010050)
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wc11-extensions"
  #pragma clang diagnostic ignored "-Wreserved-id-macro"
#elif defined (__GNUC__)
  /* anonymous unions are enabled by default */
#elif defined (__TMS470__)
  /* anonymous unions are enabled by default */
#elif defined (__TASKING__)
  #pragma warning 586
#elif defined (__CSMC__)
  /* anonymous unions are enabled by default */
#else
  #warning Not supported compiler type
#endif

/** @addtogroup Device_Peripheral_Registers
  * @{
  */

/* --------  Configuration of Core Peripherals  ----------------------------------- */
#define __CM4_REV                 0x0001U   /* Core revision r0p1 */
#define __MPU_PRESENT             1U        /* MPU present */
#define __VTOR_PRESENT            1U        /* VTOR present */
#define __NVIC_PRIO_BITS          3U        /* Number of Bits used for Priority Levels */
#define __Vendor_SysTickConfig    0U        /* Set to 1 if different SysTick Config is used */
#define __FPU_PRESENT             1U        /* FPU present */

#include "core_cm4.h"                       /* Processor and core peripherals */
#include "system_ARMCM4.h"                  /* System Header */

#define MII_MAIN_PCLOCK_HZ 24000000
//define offsets
#define ANALOG_PT_POR_CTRL_OFFSET  (0x0)
#define ANALOG_PT_RING_OSC_CRTL_OFFSET  (0x4)
#define ANALOG_PT_LDO_CTRL_OFFSET  (0x8)
#define ANALOG_PT_TEST_ANA_MUX_CTRL_OFFSET  (0xc)
#define ANALOG_PT_ADC_CTRL_OFFSET  (0x10)
#define ANALOG_PT_ADC_RES_OFFSET  (0x14)
#define ANALOG_PT_ADC_CALIB_OFFSET  (0x18)
#define ANALOG_PT_GEN_SPARE_OFFSET  (0x1c)


typedef union _ANALOG_PT_POR_CTRL_U_ {
    unsigned int w32; 
    struct { 
	unsigned force_por :1;
	unsigned rsv0 :31;
    } b;
} ANALOG_PT_POR_CTRL_UT;

typedef union _ANALOG_PT_RING_OSC_CRTL_U_ {
    unsigned int w32; 
    struct { 
	unsigned clk_1m_dis :1;
	unsigned rsv0 :3;
	unsigned clk_24m_dis :1;
	unsigned rsv1 :3;
	unsigned clk_24m_calib :10;
	unsigned rsv2 :14;
    } b;
} ANALOG_PT_RING_OSC_CRTL_UT;

typedef union _ANALOG_PT_LDO_CTRL_U_ {
    unsigned int w32; 
    struct { 
	unsigned ldo2p5_en :1;
	unsigned pad_2p5_en :1;
	unsigned rsv0 :2;
	unsigned ldo1p2_en :1;
	unsigned rsv1 :3;
	unsigned efuse_vpp_en :1;
	unsigned rsv2 :23;
    } b;
} ANALOG_PT_LDO_CTRL_UT;

typedef union _ANALOG_PT_TEST_ANA_MUX_CTRL_U_ {
    unsigned int w32; 
    struct { 
	unsigned test_src_sel :3;
	unsigned rsv0 :1;
	unsigned test_en :1;
	unsigned rsv1 :27;
    } b;
} ANALOG_PT_TEST_ANA_MUX_CTRL_UT;

typedef union _ANALOG_PT_ADC_CTRL_U_ {
    unsigned int w32; 
    struct { 
	unsigned en_adc :1;
	unsigned adc_mux_en :1;
	unsigned en_adc_1p2v :1;
	unsigned en_adc_temp :1;
	unsigned en_adc_diff :1;
	unsigned rst_adc_peak_detect :1;
	unsigned rsv0 :2;
	unsigned adc_mux_sel :3;
	unsigned rsv1 :21;
    } b;
} ANALOG_PT_ADC_CTRL_UT;

typedef union _ANALOG_PT_ADC_RES_U_ {
    unsigned int w32; 
    struct { 
	unsigned adc_res :10;
	unsigned rsv0 :2;
	unsigned adc_res_ovr :1;
	unsigned rsv1 :19;
    } b;
} ANALOG_PT_ADC_RES_UT;

typedef union _ANALOG_PT_ADC_CALIB_U_ {
    unsigned int w32; 
    struct { 
	unsigned adc_calib :32;
    } b;
} ANALOG_PT_ADC_CALIB_UT;

typedef union _ANALOG_PT_GEN_SPARE_U_ {
    unsigned int w32; 
    struct { 
	unsigned spare :32;
    } b;
} ANALOG_PT_GEN_SPARE_UT;

//module typedef
typedef volatile struct _ANALOG_PT_MODULE_S
{
	ANALOG_PT_POR_CTRL_UT POR_CTRL;
	ANALOG_PT_RING_OSC_CRTL_UT RING_OSC_CRTL;
	ANALOG_PT_LDO_CTRL_UT LDO_CTRL;
	ANALOG_PT_TEST_ANA_MUX_CTRL_UT TEST_ANA_MUX_CTRL;
	ANALOG_PT_ADC_CTRL_UT ADC_CTRL;
	ANALOG_PT_ADC_RES_UT ADC_RES;
	ANALOG_PT_ADC_CALIB_UT ADC_CALIB;
	ANALOG_PT_GEN_SPARE_UT GEN_SPARE;
} ANALOG_PT_MODULE_ST;



//define parameterized depths


//define enumerated types


//define offsets
#define DMA_CH0_PT_SAR0_LOW_OFFSET  (0x0)
#define DMA_CH0_PT_SAR0_HIGH_OFFSET  (0x4)
#define DMA_CH0_PT_DAR0_LOW_OFFSET  (0x8)
#define DMA_CH0_PT_DAR0_HIGH_OFFSET  (0xc)
#define DMA_CH0_PT_CTL0_LOW_OFFSET  (0x18)
#define DMA_CH0_PT_CTL0_HIGH_OFFSET  (0x1c)
#define DMA_CH0_PT_CFG0_LOW_OFFSET  (0x40)
#define DMA_CH0_PT_CFG0_HIGH_OFFSET  (0x44)


typedef union _DMA_CH0_PT_SAR0_LOW_U_ {
    unsigned int w32; 
    struct { 
	unsigned sar :32;
    } b;
} DMA_CH0_PT_SAR0_LOW_UT;

typedef union _DMA_CH0_PT_SAR0_HIGH_U_ {
    unsigned int w32; 
    struct { 
	unsigned rsvd_sar :32;
    } b;
} DMA_CH0_PT_SAR0_HIGH_UT;

typedef union _DMA_CH0_PT_DAR0_LOW_U_ {
    unsigned int w32; 
    struct { 
	unsigned dar :32;
    } b;
} DMA_CH0_PT_DAR0_LOW_UT;

typedef union _DMA_CH0_PT_DAR0_HIGH_U_ {
    unsigned int w32; 
    struct { 
	unsigned rsvd_dar :32;
    } b;
} DMA_CH0_PT_DAR0_HIGH_UT;

typedef union _DMA_CH0_PT_CTL0_LOW_U_ {
    unsigned int w32; 
    struct { 
	unsigned int_en :1;
	unsigned rsvd_dst_tr_width :3;
	unsigned rsvd_src_tr_width :3;
	unsigned dinc :2;
	unsigned sinc :2;
	unsigned dest_msize :3;
	unsigned src_msize :3;
	unsigned rsvd_src_gather_en :1;
	unsigned rsvd_dst_scatter_en :1;
	unsigned rsvd_ctl :1;
	unsigned tt_fc :3;
	unsigned rsvd_dms :2;
	unsigned rsvd_sms :2;
	unsigned rsvd_llp_dst_en :1;
	unsigned rsvd_llp_src_en :1;
	unsigned rsvd_1_ctl :3;
    } b;
} DMA_CH0_PT_CTL0_LOW_UT;

typedef union _DMA_CH0_PT_CTL0_HIGH_U_ {
    unsigned int w32; 
    struct { 
	unsigned block_ts :5;
	unsigned rsvd_2_ctl :7;
	unsigned done :1;
	unsigned rsvd_3_ctl :19;
    } b;
} DMA_CH0_PT_CTL0_HIGH_UT;

typedef union _DMA_CH0_PT_CFG0_LOW_U_ {
    unsigned int w32; 
    struct { 
	unsigned rsvd_cfg :5;
	unsigned ch_prior :3;
	unsigned ch_susp :1;
	unsigned fifo_empty :1;
	unsigned hs_sel_dst :1;
	unsigned hs_sel_src :1;
	unsigned rsvd_lock_ch_l :2;
	unsigned rsvd_lock_b_l :2;
	unsigned rsvd_lock_ch :1;
	unsigned rsvd_lock_b :1;
	unsigned dst_hs_pol :1;
	unsigned src_hs_pol :1;
	unsigned max_abrst :10;
	unsigned rsvd_reload_src :1;
	unsigned rsvd_reload_dst :1;
    } b;
} DMA_CH0_PT_CFG0_LOW_UT;

typedef union _DMA_CH0_PT_CFG0_HIGH_U_ {
    unsigned int w32; 
    struct { 
	unsigned fcmode :1;
	unsigned fifo_mode :1;
	unsigned protctl :3;
	unsigned rsvd_ds_upd_en :1;
	unsigned rsvd_ss_upd_en :1;
	unsigned src_per :4;
	unsigned dest_per :4;
	unsigned rsvd_3_cfg :17;
    } b;
} DMA_CH0_PT_CFG0_HIGH_UT;

//module typedef
typedef volatile struct _DMA_CH0_PT_MODULE_S
{
	DMA_CH0_PT_SAR0_LOW_UT SAR0_LOW;
	DMA_CH0_PT_SAR0_HIGH_UT SAR0_HIGH;
	DMA_CH0_PT_DAR0_LOW_UT DAR0_LOW;
	DMA_CH0_PT_DAR0_HIGH_UT DAR0_HIGH;
	unsigned rsv_c_18 [2];
	DMA_CH0_PT_CTL0_LOW_UT CTL0_LOW;
	DMA_CH0_PT_CTL0_HIGH_UT CTL0_HIGH;
	unsigned rsv_1c_40 [8];
	DMA_CH0_PT_CFG0_LOW_UT CFG0_LOW;
	DMA_CH0_PT_CFG0_HIGH_UT CFG0_HIGH;
} DMA_CH0_PT_MODULE_ST;



//define parameterized depths


//define enumerated types


//define offsets
#define DMA_CH1_PT_SAR1_LOW_OFFSET  (0x0)
#define DMA_CH1_PT_SAR1_HIGH_OFFSET  (0x4)
#define DMA_CH1_PT_DAR1_LOW_OFFSET  (0x8)
#define DMA_CH1_PT_DAR1_HIGH_OFFSET  (0xc)
#define DMA_CH1_PT_CTL1_LOW_OFFSET  (0x18)
#define DMA_CH1_PT_CTL1_HIGH_OFFSET  (0x1c)
#define DMA_CH1_PT_CFG1_LOW_OFFSET  (0x40)
#define DMA_CH1_PT_CFG1_HIGH_OFFSET  (0x44)


typedef union _DMA_CH1_PT_SAR1_LOW_U_ {
    unsigned int w32; 
    struct { 
	unsigned sar :32;
    } b;
} DMA_CH1_PT_SAR1_LOW_UT;

typedef union _DMA_CH1_PT_SAR1_HIGH_U_ {
    unsigned int w32; 
    struct { 
	unsigned rsvd_sar :32;
    } b;
} DMA_CH1_PT_SAR1_HIGH_UT;

typedef union _DMA_CH1_PT_DAR1_LOW_U_ {
    unsigned int w32; 
    struct { 
	unsigned dar :32;
    } b;
} DMA_CH1_PT_DAR1_LOW_UT;

typedef union _DMA_CH1_PT_DAR1_HIGH_U_ {
    unsigned int w32; 
    struct { 
	unsigned rsvd_dar :32;
    } b;
} DMA_CH1_PT_DAR1_HIGH_UT;

typedef union _DMA_CH1_PT_CTL1_LOW_U_ {
    unsigned int w32; 
    struct { 
	unsigned int_en :1;
	unsigned rsvd_dst_tr_width :3;
	unsigned rsvd_src_tr_width :3;
	unsigned dinc :2;
	unsigned sinc :2;
	unsigned dest_msize :3;
	unsigned src_msize :3;
	unsigned rsvd_src_gather_en :1;
	unsigned rsvd_dst_scatter_en :1;
	unsigned rsvd_ctl :1;
	unsigned tt_fc :3;
	unsigned rsvd_dms :2;
	unsigned rsvd_sms :2;
	unsigned rsvd_llp_dst_en :1;
	unsigned rsvd_llp_src_en :1;
	unsigned rsvd_1_ctl :3;
    } b;
} DMA_CH1_PT_CTL1_LOW_UT;

typedef union _DMA_CH1_PT_CTL1_HIGH_U_ {
    unsigned int w32; 
    struct { 
	unsigned block_ts :5;
	unsigned rsvd_2_ctl :7;
	unsigned done :1;
	unsigned rsvd_3_ctl :19;
    } b;
} DMA_CH1_PT_CTL1_HIGH_UT;

typedef union _DMA_CH1_PT_CFG1_LOW_U_ {
    unsigned int w32; 
    struct { 
	unsigned rsvd_cfg :5;
	unsigned ch_prior :3;
	unsigned ch_susp :1;
	unsigned fifo_empty :1;
	unsigned hs_sel_dst :1;
	unsigned hs_sel_src :1;
	unsigned rsvd_lock_ch_l :2;
	unsigned rsvd_lock_b_l :2;
	unsigned rsvd_lock_ch :1;
	unsigned rsvd_lock_b :1;
	unsigned dst_hs_pol :1;
	unsigned src_hs_pol :1;
	unsigned max_abrst :10;
	unsigned rsvd_reload_src :1;
	unsigned rsvd_reload_dst :1;
    } b;
} DMA_CH1_PT_CFG1_LOW_UT;

typedef union _DMA_CH1_PT_CFG1_HIGH_U_ {
    unsigned int w32; 
    struct { 
	unsigned fcmode :1;
	unsigned fifo_mode :1;
	unsigned protctl :3;
	unsigned rsvd_ds_upd_en :1;
	unsigned rsvd_ss_upd_en :1;
	unsigned src_per :4;
	unsigned dest_per :4;
	unsigned rsvd_3_cfg :17;
    } b;
} DMA_CH1_PT_CFG1_HIGH_UT;

//module typedef
typedef volatile struct _DMA_CH1_PT_MODULE_S
{
	DMA_CH1_PT_SAR1_LOW_UT SAR1_LOW;
	DMA_CH1_PT_SAR1_HIGH_UT SAR1_HIGH;
	DMA_CH1_PT_DAR1_LOW_UT DAR1_LOW;
	DMA_CH1_PT_DAR1_HIGH_UT DAR1_HIGH;
	unsigned rsv_c_18 [2];
	DMA_CH1_PT_CTL1_LOW_UT CTL1_LOW;
	DMA_CH1_PT_CTL1_HIGH_UT CTL1_HIGH;
	unsigned rsv_1c_40 [8];
	DMA_CH1_PT_CFG1_LOW_UT CFG1_LOW;
	DMA_CH1_PT_CFG1_HIGH_UT CFG1_HIGH;
} DMA_CH1_PT_MODULE_ST;



//define parameterized depths


//define enumerated types


//define offsets
#define DMA_CH2_PT_SAR2_LOW_OFFSET  (0x0)
#define DMA_CH2_PT_SAR2_HIGH_OFFSET  (0x4)
#define DMA_CH2_PT_DAR2_LOW_OFFSET  (0x8)
#define DMA_CH2_PT_DAR2_HIGH_OFFSET  (0xc)
#define DMA_CH2_PT_CTL2_LOW_OFFSET  (0x18)
#define DMA_CH2_PT_CTL2_HIGH_OFFSET  (0x1c)
#define DMA_CH2_PT_CFG2_LOW_OFFSET  (0x40)
#define DMA_CH2_PT_CFG2_HIGH_OFFSET  (0x44)


typedef union _DMA_CH2_PT_SAR2_LOW_U_ {
    unsigned int w32; 
    struct { 
	unsigned sar :32;
    } b;
} DMA_CH2_PT_SAR2_LOW_UT;

typedef union _DMA_CH2_PT_SAR2_HIGH_U_ {
    unsigned int w32; 
    struct { 
	unsigned rsvd_sar :32;
    } b;
} DMA_CH2_PT_SAR2_HIGH_UT;

typedef union _DMA_CH2_PT_DAR2_LOW_U_ {
    unsigned int w32; 
    struct { 
	unsigned dar :32;
    } b;
} DMA_CH2_PT_DAR2_LOW_UT;

typedef union _DMA_CH2_PT_DAR2_HIGH_U_ {
    unsigned int w32; 
    struct { 
	unsigned rsvd_dar :32;
    } b;
} DMA_CH2_PT_DAR2_HIGH_UT;

typedef union _DMA_CH2_PT_CTL2_LOW_U_ {
    unsigned int w32; 
    struct { 
	unsigned int_en :1;
	unsigned rsvd_dst_tr_width :3;
	unsigned rsvd_src_tr_width :3;
	unsigned dinc :2;
	unsigned sinc :2;
	unsigned dest_msize :3;
	unsigned src_msize :3;
	unsigned rsvd_src_gather_en :1;
	unsigned rsvd_dst_scatter_en :1;
	unsigned rsvd_ctl :1;
	unsigned tt_fc :3;
	unsigned rsvd_dms :2;
	unsigned rsvd_sms :2;
	unsigned rsvd_llp_dst_en :1;
	unsigned rsvd_llp_src_en :1;
	unsigned rsvd_1_ctl :3;
    } b;
} DMA_CH2_PT_CTL2_LOW_UT;

typedef union _DMA_CH2_PT_CTL2_HIGH_U_ {
    unsigned int w32; 
    struct { 
	unsigned block_ts :5;
	unsigned rsvd_2_ctl :7;
	unsigned done :1;
	unsigned rsvd_3_ctl :19;
    } b;
} DMA_CH2_PT_CTL2_HIGH_UT;

typedef union _DMA_CH2_PT_CFG2_LOW_U_ {
    unsigned int w32; 
    struct { 
	unsigned rsvd_cfg :5;
	unsigned ch_prior :3;
	unsigned ch_susp :1;
	unsigned fifo_empty :1;
	unsigned hs_sel_dst :1;
	unsigned hs_sel_src :1;
	unsigned rsvd_lock_ch_l :2;
	unsigned rsvd_lock_b_l :2;
	unsigned rsvd_lock_ch :1;
	unsigned rsvd_lock_b :1;
	unsigned dst_hs_pol :1;
	unsigned src_hs_pol :1;
	unsigned max_abrst :10;
	unsigned rsvd_reload_src :1;
	unsigned rsvd_reload_dst :1;
    } b;
} DMA_CH2_PT_CFG2_LOW_UT;

typedef union _DMA_CH2_PT_CFG2_HIGH_U_ {
    unsigned int w32; 
    struct { 
	unsigned fcmode :1;
	unsigned fifo_mode :1;
	unsigned protctl :3;
	unsigned rsvd_ds_upd_en :1;
	unsigned rsvd_ss_upd_en :1;
	unsigned src_per :4;
	unsigned dest_per :4;
	unsigned rsvd_3_cfg :17;
    } b;
} DMA_CH2_PT_CFG2_HIGH_UT;

//module typedef
typedef volatile struct _DMA_CH2_PT_MODULE_S
{
	DMA_CH2_PT_SAR2_LOW_UT SAR2_LOW;
	DMA_CH2_PT_SAR2_HIGH_UT SAR2_HIGH;
	DMA_CH2_PT_DAR2_LOW_UT DAR2_LOW;
	DMA_CH2_PT_DAR2_HIGH_UT DAR2_HIGH;
	unsigned rsv_c_18 [2];
	DMA_CH2_PT_CTL2_LOW_UT CTL2_LOW;
	DMA_CH2_PT_CTL2_HIGH_UT CTL2_HIGH;
	unsigned rsv_1c_40 [8];
	DMA_CH2_PT_CFG2_LOW_UT CFG2_LOW;
	DMA_CH2_PT_CFG2_HIGH_UT CFG2_HIGH;
} DMA_CH2_PT_MODULE_ST;



//define parameterized depths


//define enumerated types


//define offsets
#define DMA_CH3_PT_SAR3_LOW_OFFSET  (0x0)
#define DMA_CH3_PT_SAR3_HIGH_OFFSET  (0x4)
#define DMA_CH3_PT_DAR3_LOW_OFFSET  (0x8)
#define DMA_CH3_PT_DAR3_HIGH_OFFSET  (0xc)
#define DMA_CH3_PT_CTL3_LOW_OFFSET  (0x18)
#define DMA_CH3_PT_CTL3_HIGH_OFFSET  (0x1c)
#define DMA_CH3_PT_CFG3_LOW_OFFSET  (0x40)
#define DMA_CH3_PT_CFG3_HIGH_OFFSET  (0x44)


typedef union _DMA_CH3_PT_SAR3_LOW_U_ {
    unsigned int w32; 
    struct { 
	unsigned sar :32;
    } b;
} DMA_CH3_PT_SAR3_LOW_UT;

typedef union _DMA_CH3_PT_SAR3_HIGH_U_ {
    unsigned int w32; 
    struct { 
	unsigned rsvd_sar :32;
    } b;
} DMA_CH3_PT_SAR3_HIGH_UT;

typedef union _DMA_CH3_PT_DAR3_LOW_U_ {
    unsigned int w32; 
    struct { 
	unsigned dar :32;
    } b;
} DMA_CH3_PT_DAR3_LOW_UT;

typedef union _DMA_CH3_PT_DAR3_HIGH_U_ {
    unsigned int w32; 
    struct { 
	unsigned rsvd_dar :32;
    } b;
} DMA_CH3_PT_DAR3_HIGH_UT;

typedef union _DMA_CH3_PT_CTL3_LOW_U_ {
    unsigned int w32; 
    struct { 
	unsigned int_en :1;
	unsigned rsvd_dst_tr_width :3;
	unsigned rsvd_src_tr_width :3;
	unsigned dinc :2;
	unsigned sinc :2;
	unsigned dest_msize :3;
	unsigned src_msize :3;
	unsigned rsvd_src_gather_en :1;
	unsigned rsvd_dst_scatter_en :1;
	unsigned rsvd_ctl :1;
	unsigned tt_fc :3;
	unsigned rsvd_dms :2;
	unsigned rsvd_sms :2;
	unsigned rsvd_llp_dst_en :1;
	unsigned rsvd_llp_src_en :1;
	unsigned rsvd_1_ctl :3;
    } b;
} DMA_CH3_PT_CTL3_LOW_UT;

typedef union _DMA_CH3_PT_CTL3_HIGH_U_ {
    unsigned int w32; 
    struct { 
	unsigned block_ts :5;
	unsigned rsvd_2_ctl :7;
	unsigned done :1;
	unsigned rsvd_3_ctl :19;
    } b;
} DMA_CH3_PT_CTL3_HIGH_UT;

typedef union _DMA_CH3_PT_CFG3_LOW_U_ {
    unsigned int w32; 
    struct { 
	unsigned rsvd_cfg :5;
	unsigned ch_prior :3;
	unsigned ch_susp :1;
	unsigned fifo_empty :1;
	unsigned hs_sel_dst :1;
	unsigned hs_sel_src :1;
	unsigned rsvd_lock_ch_l :2;
	unsigned rsvd_lock_b_l :2;
	unsigned rsvd_lock_ch :1;
	unsigned rsvd_lock_b :1;
	unsigned dst_hs_pol :1;
	unsigned src_hs_pol :1;
	unsigned max_abrst :10;
	unsigned rsvd_reload_src :1;
	unsigned rsvd_reload_dst :1;
    } b;
} DMA_CH3_PT_CFG3_LOW_UT;

typedef union _DMA_CH3_PT_CFG3_HIGH_U_ {
    unsigned int w32; 
    struct { 
	unsigned fcmode :1;
	unsigned fifo_mode :1;
	unsigned protctl :3;
	unsigned rsvd_ds_upd_en :1;
	unsigned rsvd_ss_upd_en :1;
	unsigned src_per :4;
	unsigned dest_per :4;
	unsigned rsvd_3_cfg :17;
    } b;
} DMA_CH3_PT_CFG3_HIGH_UT;

//module typedef
typedef volatile struct _DMA_CH3_PT_MODULE_S
{
	DMA_CH3_PT_SAR3_LOW_UT SAR3_LOW;
	DMA_CH3_PT_SAR3_HIGH_UT SAR3_HIGH;
	DMA_CH3_PT_DAR3_LOW_UT DAR3_LOW;
	DMA_CH3_PT_DAR3_HIGH_UT DAR3_HIGH;
	unsigned rsv_c_18 [2];
	DMA_CH3_PT_CTL3_LOW_UT CTL3_LOW;
	DMA_CH3_PT_CTL3_HIGH_UT CTL3_HIGH;
	unsigned rsv_1c_40 [8];
	DMA_CH3_PT_CFG3_LOW_UT CFG3_LOW;
	DMA_CH3_PT_CFG3_HIGH_UT CFG3_HIGH;
} DMA_CH3_PT_MODULE_ST;



//define parameterized depths


//define enumerated types


//define offsets
#define DMA_CH4_PT_SAR4_LOW_OFFSET  (0x0)
#define DMA_CH4_PT_SAR4_HIGH_OFFSET  (0x4)
#define DMA_CH4_PT_DAR4_LOW_OFFSET  (0x8)
#define DMA_CH4_PT_DAR4_HIGH_OFFSET  (0xc)
#define DMA_CH4_PT_CTL4_LOW_OFFSET  (0x18)
#define DMA_CH4_PT_CTL4_HIGH_OFFSET  (0x1c)
#define DMA_CH4_PT_CFG4_LOW_OFFSET  (0x40)
#define DMA_CH4_PT_CFG4_HIGH_OFFSET  (0x44)


typedef union _DMA_CH4_PT_SAR4_LOW_U_ {
    unsigned int w32; 
    struct { 
	unsigned sar :32;
    } b;
} DMA_CH4_PT_SAR4_LOW_UT;

typedef union _DMA_CH4_PT_SAR4_HIGH_U_ {
    unsigned int w32; 
    struct { 
	unsigned rsvd_sar :32;
    } b;
} DMA_CH4_PT_SAR4_HIGH_UT;

typedef union _DMA_CH4_PT_DAR4_LOW_U_ {
    unsigned int w32; 
    struct { 
	unsigned dar :32;
    } b;
} DMA_CH4_PT_DAR4_LOW_UT;

typedef union _DMA_CH4_PT_DAR4_HIGH_U_ {
    unsigned int w32; 
    struct { 
	unsigned rsvd_dar :32;
    } b;
} DMA_CH4_PT_DAR4_HIGH_UT;

typedef union _DMA_CH4_PT_CTL4_LOW_U_ {
    unsigned int w32; 
    struct { 
	unsigned int_en :1;
	unsigned rsvd_dst_tr_width :3;
	unsigned rsvd_src_tr_width :3;
	unsigned dinc :2;
	unsigned sinc :2;
	unsigned dest_msize :3;
	unsigned src_msize :3;
	unsigned rsvd_src_gather_en :1;
	unsigned rsvd_dst_scatter_en :1;
	unsigned rsvd_ctl :1;
	unsigned tt_fc :3;
	unsigned rsvd_dms :2;
	unsigned rsvd_sms :2;
	unsigned rsvd_llp_dst_en :1;
	unsigned rsvd_llp_src_en :1;
	unsigned rsvd_1_ctl :3;
    } b;
} DMA_CH4_PT_CTL4_LOW_UT;

typedef union _DMA_CH4_PT_CTL4_HIGH_U_ {
    unsigned int w32; 
    struct { 
	unsigned block_ts :5;
	unsigned rsvd_2_ctl :7;
	unsigned done :1;
	unsigned rsvd_3_ctl :19;
    } b;
} DMA_CH4_PT_CTL4_HIGH_UT;

typedef union _DMA_CH4_PT_CFG4_LOW_U_ {
    unsigned int w32; 
    struct { 
	unsigned rsvd_cfg :5;
	unsigned ch_prior :3;
	unsigned ch_susp :1;
	unsigned fifo_empty :1;
	unsigned hs_sel_dst :1;
	unsigned hs_sel_src :1;
	unsigned rsvd_lock_ch_l :2;
	unsigned rsvd_lock_b_l :2;
	unsigned rsvd_lock_ch :1;
	unsigned rsvd_lock_b :1;
	unsigned dst_hs_pol :1;
	unsigned src_hs_pol :1;
	unsigned max_abrst :10;
	unsigned rsvd_reload_src :1;
	unsigned rsvd_reload_dst :1;
    } b;
} DMA_CH4_PT_CFG4_LOW_UT;

typedef union _DMA_CH4_PT_CFG4_HIGH_U_ {
    unsigned int w32; 
    struct { 
	unsigned fcmode :1;
	unsigned fifo_mode :1;
	unsigned protctl :3;
	unsigned rsvd_ds_upd_en :1;
	unsigned rsvd_ss_upd_en :1;
	unsigned src_per :4;
	unsigned dest_per :4;
	unsigned rsvd_3_cfg :17;
    } b;
} DMA_CH4_PT_CFG4_HIGH_UT;

//module typedef
typedef volatile struct _DMA_CH4_PT_MODULE_S
{
	DMA_CH4_PT_SAR4_LOW_UT SAR4_LOW;
	DMA_CH4_PT_SAR4_HIGH_UT SAR4_HIGH;
	DMA_CH4_PT_DAR4_LOW_UT DAR4_LOW;
	DMA_CH4_PT_DAR4_HIGH_UT DAR4_HIGH;
	unsigned rsv_c_18 [2];
	DMA_CH4_PT_CTL4_LOW_UT CTL4_LOW;
	DMA_CH4_PT_CTL4_HIGH_UT CTL4_HIGH;
	unsigned rsv_1c_40 [8];
	DMA_CH4_PT_CFG4_LOW_UT CFG4_LOW;
	DMA_CH4_PT_CFG4_HIGH_UT CFG4_HIGH;
} DMA_CH4_PT_MODULE_ST;



//define parameterized depths


//define enumerated types


//define offsets
#define DMA_CH5_PT_SAR5_LOW_OFFSET  (0x0)
#define DMA_CH5_PT_SAR5_HIGH_OFFSET  (0x4)
#define DMA_CH5_PT_DAR5_LOW_OFFSET  (0x8)
#define DMA_CH5_PT_DAR5_HIGH_OFFSET  (0xc)
#define DMA_CH5_PT_CTL5_LOW_OFFSET  (0x18)
#define DMA_CH5_PT_CTL5_HIGH_OFFSET  (0x1c)
#define DMA_CH5_PT_CFG5_LOW_OFFSET  (0x40)
#define DMA_CH5_PT_CFG5_HIGH_OFFSET  (0x44)


typedef union _DMA_CH5_PT_SAR5_LOW_U_ {
    unsigned int w32; 
    struct { 
	unsigned sar :32;
    } b;
} DMA_CH5_PT_SAR5_LOW_UT;

typedef union _DMA_CH5_PT_SAR5_HIGH_U_ {
    unsigned int w32; 
    struct { 
	unsigned rsvd_sar :32;
    } b;
} DMA_CH5_PT_SAR5_HIGH_UT;

typedef union _DMA_CH5_PT_DAR5_LOW_U_ {
    unsigned int w32; 
    struct { 
	unsigned dar :32;
    } b;
} DMA_CH5_PT_DAR5_LOW_UT;

typedef union _DMA_CH5_PT_DAR5_HIGH_U_ {
    unsigned int w32; 
    struct { 
	unsigned rsvd_dar :32;
    } b;
} DMA_CH5_PT_DAR5_HIGH_UT;

typedef union _DMA_CH5_PT_CTL5_LOW_U_ {
    unsigned int w32; 
    struct { 
	unsigned int_en :1;
	unsigned rsvd_dst_tr_width :3;
	unsigned rsvd_src_tr_width :3;
	unsigned dinc :2;
	unsigned sinc :2;
	unsigned dest_msize :3;
	unsigned src_msize :3;
	unsigned rsvd_src_gather_en :1;
	unsigned rsvd_dst_scatter_en :1;
	unsigned rsvd_ctl :1;
	unsigned tt_fc :3;
	unsigned rsvd_dms :2;
	unsigned rsvd_sms :2;
	unsigned rsvd_llp_dst_en :1;
	unsigned rsvd_llp_src_en :1;
	unsigned rsvd_1_ctl :3;
    } b;
} DMA_CH5_PT_CTL5_LOW_UT;

typedef union _DMA_CH5_PT_CTL5_HIGH_U_ {
    unsigned int w32; 
    struct { 
	unsigned block_ts :5;
	unsigned rsvd_2_ctl :7;
	unsigned done :1;
	unsigned rsvd_3_ctl :19;
    } b;
} DMA_CH5_PT_CTL5_HIGH_UT;

typedef union _DMA_CH5_PT_CFG5_LOW_U_ {
    unsigned int w32; 
    struct { 
	unsigned rsvd_cfg :5;
	unsigned ch_prior :3;
	unsigned ch_susp :1;
	unsigned fifo_empty :1;
	unsigned hs_sel_dst :1;
	unsigned hs_sel_src :1;
	unsigned rsvd_lock_ch_l :2;
	unsigned rsvd_lock_b_l :2;
	unsigned rsvd_lock_ch :1;
	unsigned rsvd_lock_b :1;
	unsigned dst_hs_pol :1;
	unsigned src_hs_pol :1;
	unsigned max_abrst :10;
	unsigned rsvd_reload_src :1;
	unsigned rsvd_reload_dst :1;
    } b;
} DMA_CH5_PT_CFG5_LOW_UT;

typedef union _DMA_CH5_PT_CFG5_HIGH_U_ {
    unsigned int w32; 
    struct { 
	unsigned fcmode :1;
	unsigned fifo_mode :1;
	unsigned protctl :3;
	unsigned rsvd_ds_upd_en :1;
	unsigned rsvd_ss_upd_en :1;
	unsigned src_per :4;
	unsigned dest_per :4;
	unsigned rsvd_3_cfg :17;
    } b;
} DMA_CH5_PT_CFG5_HIGH_UT;

//module typedef
typedef volatile struct _DMA_CH5_PT_MODULE_S
{
	DMA_CH5_PT_SAR5_LOW_UT SAR5_LOW;
	DMA_CH5_PT_SAR5_HIGH_UT SAR5_HIGH;
	DMA_CH5_PT_DAR5_LOW_UT DAR5_LOW;
	DMA_CH5_PT_DAR5_HIGH_UT DAR5_HIGH;
	unsigned rsv_c_18 [2];
	DMA_CH5_PT_CTL5_LOW_UT CTL5_LOW;
	DMA_CH5_PT_CTL5_HIGH_UT CTL5_HIGH;
	unsigned rsv_1c_40 [8];
	DMA_CH5_PT_CFG5_LOW_UT CFG5_LOW;
	DMA_CH5_PT_CFG5_HIGH_UT CFG5_HIGH;
} DMA_CH5_PT_MODULE_ST;



//define parameterized depths


//define enumerated types


//define offsets
#define DMA_CH6_PT_SAR6_LOW_OFFSET  (0x0)
#define DMA_CH6_PT_SAR6_HIGH_OFFSET  (0x4)
#define DMA_CH6_PT_DAR6_LOW_OFFSET  (0x8)
#define DMA_CH6_PT_DAR6_HIGH_OFFSET  (0xc)
#define DMA_CH6_PT_CTL6_LOW_OFFSET  (0x18)
#define DMA_CH6_PT_CTL6_HIGH_OFFSET  (0x1c)
#define DMA_CH6_PT_CFG6_LOW_OFFSET  (0x40)
#define DMA_CH6_PT_CFG6_HIGH_OFFSET  (0x44)


typedef union _DMA_CH6_PT_SAR6_LOW_U_ {
    unsigned int w32; 
    struct { 
	unsigned sar :32;
    } b;
} DMA_CH6_PT_SAR6_LOW_UT;

typedef union _DMA_CH6_PT_SAR6_HIGH_U_ {
    unsigned int w32; 
    struct { 
	unsigned rsvd_sar :32;
    } b;
} DMA_CH6_PT_SAR6_HIGH_UT;

typedef union _DMA_CH6_PT_DAR6_LOW_U_ {
    unsigned int w32; 
    struct { 
	unsigned dar :32;
    } b;
} DMA_CH6_PT_DAR6_LOW_UT;

typedef union _DMA_CH6_PT_DAR6_HIGH_U_ {
    unsigned int w32; 
    struct { 
	unsigned rsvd_dar :32;
    } b;
} DMA_CH6_PT_DAR6_HIGH_UT;

typedef union _DMA_CH6_PT_CTL6_LOW_U_ {
    unsigned int w32; 
    struct { 
	unsigned int_en :1;
	unsigned rsvd_dst_tr_width :3;
	unsigned rsvd_src_tr_width :3;
	unsigned dinc :2;
	unsigned sinc :2;
	unsigned dest_msize :3;
	unsigned src_msize :3;
	unsigned rsvd_src_gather_en :1;
	unsigned rsvd_dst_scatter_en :1;
	unsigned rsvd_ctl :1;
	unsigned tt_fc :3;
	unsigned rsvd_dms :2;
	unsigned rsvd_sms :2;
	unsigned rsvd_llp_dst_en :1;
	unsigned rsvd_llp_src_en :1;
	unsigned rsvd_1_ctl :3;
    } b;
} DMA_CH6_PT_CTL6_LOW_UT;

typedef union _DMA_CH6_PT_CTL6_HIGH_U_ {
    unsigned int w32; 
    struct { 
	unsigned block_ts :5;
	unsigned rsvd_2_ctl :7;
	unsigned done :1;
	unsigned rsvd_3_ctl :19;
    } b;
} DMA_CH6_PT_CTL6_HIGH_UT;

typedef union _DMA_CH6_PT_CFG6_LOW_U_ {
    unsigned int w32; 
    struct { 
	unsigned rsvd_cfg :5;
	unsigned ch_prior :3;
	unsigned ch_susp :1;
	unsigned fifo_empty :1;
	unsigned hs_sel_dst :1;
	unsigned hs_sel_src :1;
	unsigned rsvd_lock_ch_l :2;
	unsigned rsvd_lock_b_l :2;
	unsigned rsvd_lock_ch :1;
	unsigned rsvd_lock_b :1;
	unsigned dst_hs_pol :1;
	unsigned src_hs_pol :1;
	unsigned max_abrst :10;
	unsigned rsvd_reload_src :1;
	unsigned rsvd_reload_dst :1;
    } b;
} DMA_CH6_PT_CFG6_LOW_UT;

typedef union _DMA_CH6_PT_CFG6_HIGH_U_ {
    unsigned int w32; 
    struct { 
	unsigned fcmode :1;
	unsigned fifo_mode :1;
	unsigned protctl :3;
	unsigned rsvd_ds_upd_en :1;
	unsigned rsvd_ss_upd_en :1;
	unsigned src_per :4;
	unsigned dest_per :4;
	unsigned rsvd_3_cfg :17;
    } b;
} DMA_CH6_PT_CFG6_HIGH_UT;

//module typedef
typedef volatile struct _DMA_CH6_PT_MODULE_S
{
	DMA_CH6_PT_SAR6_LOW_UT SAR6_LOW;
	DMA_CH6_PT_SAR6_HIGH_UT SAR6_HIGH;
	DMA_CH6_PT_DAR6_LOW_UT DAR6_LOW;
	DMA_CH6_PT_DAR6_HIGH_UT DAR6_HIGH;
	unsigned rsv_c_18 [2];
	DMA_CH6_PT_CTL6_LOW_UT CTL6_LOW;
	DMA_CH6_PT_CTL6_HIGH_UT CTL6_HIGH;
	unsigned rsv_1c_40 [8];
	DMA_CH6_PT_CFG6_LOW_UT CFG6_LOW;
	DMA_CH6_PT_CFG6_HIGH_UT CFG6_HIGH;
} DMA_CH6_PT_MODULE_ST;



//define parameterized depths


//define enumerated types


//define offsets
#define DMA_CH7_PT_SAR7_LOW_OFFSET  (0x0)
#define DMA_CH7_PT_SAR7_HIGH_OFFSET  (0x4)
#define DMA_CH7_PT_DAR7_LOW_OFFSET  (0x8)
#define DMA_CH7_PT_DAR7_HIGH_OFFSET  (0xc)
#define DMA_CH7_PT_CTL7_LOW_OFFSET  (0x18)
#define DMA_CH7_PT_CTL7_HIGH_OFFSET  (0x1c)
#define DMA_CH7_PT_CFG7_LOW_OFFSET  (0x40)
#define DMA_CH7_PT_CFG7_HIGH_OFFSET  (0x44)


typedef union _DMA_CH7_PT_SAR7_LOW_U_ {
    unsigned int w32; 
    struct { 
	unsigned sar :32;
    } b;
} DMA_CH7_PT_SAR7_LOW_UT;

typedef union _DMA_CH7_PT_SAR7_HIGH_U_ {
    unsigned int w32; 
    struct { 
	unsigned rsvd_sar :32;
    } b;
} DMA_CH7_PT_SAR7_HIGH_UT;

typedef union _DMA_CH7_PT_DAR7_LOW_U_ {
    unsigned int w32; 
    struct { 
	unsigned dar :32;
    } b;
} DMA_CH7_PT_DAR7_LOW_UT;

typedef union _DMA_CH7_PT_DAR7_HIGH_U_ {
    unsigned int w32; 
    struct { 
	unsigned rsvd_dar :32;
    } b;
} DMA_CH7_PT_DAR7_HIGH_UT;

typedef union _DMA_CH7_PT_CTL7_LOW_U_ {
    unsigned int w32; 
    struct { 
	unsigned int_en :1;
	unsigned rsvd_dst_tr_width :3;
	unsigned rsvd_src_tr_width :3;
	unsigned dinc :2;
	unsigned sinc :2;
	unsigned dest_msize :3;
	unsigned src_msize :3;
	unsigned rsvd_src_gather_en :1;
	unsigned rsvd_dst_scatter_en :1;
	unsigned rsvd_ctl :1;
	unsigned tt_fc :3;
	unsigned rsvd_dms :2;
	unsigned rsvd_sms :2;
	unsigned rsvd_llp_dst_en :1;
	unsigned rsvd_llp_src_en :1;
	unsigned rsvd_1_ctl :3;
    } b;
} DMA_CH7_PT_CTL7_LOW_UT;

typedef union _DMA_CH7_PT_CTL7_HIGH_U_ {
    unsigned int w32; 
    struct { 
	unsigned block_ts :5;
	unsigned rsvd_2_ctl :7;
	unsigned done :1;
	unsigned rsvd_3_ctl :19;
    } b;
} DMA_CH7_PT_CTL7_HIGH_UT;

typedef union _DMA_CH7_PT_CFG7_LOW_U_ {
    unsigned int w32; 
    struct { 
	unsigned rsvd_cfg :5;
	unsigned ch_prior :3;
	unsigned ch_susp :1;
	unsigned fifo_empty :1;
	unsigned hs_sel_dst :1;
	unsigned hs_sel_src :1;
	unsigned rsvd_lock_ch_l :2;
	unsigned rsvd_lock_b_l :2;
	unsigned rsvd_lock_ch :1;
	unsigned rsvd_lock_b :1;
	unsigned dst_hs_pol :1;
	unsigned src_hs_pol :1;
	unsigned max_abrst :10;
	unsigned rsvd_reload_src :1;
	unsigned rsvd_reload_dst :1;
    } b;
} DMA_CH7_PT_CFG7_LOW_UT;

typedef union _DMA_CH7_PT_CFG7_HIGH_U_ {
    unsigned int w32; 
    struct { 
	unsigned fcmode :1;
	unsigned fifo_mode :1;
	unsigned protctl :3;
	unsigned rsvd_ds_upd_en :1;
	unsigned rsvd_ss_upd_en :1;
	unsigned src_per :4;
	unsigned dest_per :4;
	unsigned rsvd_3_cfg :17;
    } b;
} DMA_CH7_PT_CFG7_HIGH_UT;

//module typedef
typedef volatile struct _DMA_CH7_PT_MODULE_S
{
	DMA_CH7_PT_SAR7_LOW_UT SAR7_LOW;
	DMA_CH7_PT_SAR7_HIGH_UT SAR7_HIGH;
	DMA_CH7_PT_DAR7_LOW_UT DAR7_LOW;
	DMA_CH7_PT_DAR7_HIGH_UT DAR7_HIGH;
	unsigned rsv_c_18 [2];
	DMA_CH7_PT_CTL7_LOW_UT CTL7_LOW;
	DMA_CH7_PT_CTL7_HIGH_UT CTL7_HIGH;
	unsigned rsv_1c_40 [8];
	DMA_CH7_PT_CFG7_LOW_UT CFG7_LOW;
	DMA_CH7_PT_CFG7_HIGH_UT CFG7_HIGH;
} DMA_CH7_PT_MODULE_ST;



//define parameterized depths


//define enumerated types


//define offsets
#define DMA_INTERRUPT_PT_RawTfr_LOW_OFFSET  (0x0)
#define DMA_INTERRUPT_PT_RawTfr_HIGH_OFFSET  (0x4)
#define DMA_INTERRUPT_PT_RawBlock_LOW_OFFSET  (0x8)
#define DMA_INTERRUPT_PT_RawBlock_HIGH_OFFSET  (0xc)
#define DMA_INTERRUPT_PT_RawSrcTran_LOW_OFFSET  (0x10)
#define DMA_INTERRUPT_PT_RawSrcTran_HIGH_OFFSET  (0x14)
#define DMA_INTERRUPT_PT_RawDstTran_LOW_OFFSET  (0x18)
#define DMA_INTERRUPT_PT_RawDstTran_HIGH_OFFSET  (0x1c)
#define DMA_INTERRUPT_PT_RawErr_LOW_OFFSET  (0x20)
#define DMA_INTERRUPT_PT_RawErr_HIGH_OFFSET  (0x24)
#define DMA_INTERRUPT_PT_StatusTfr_LOW_OFFSET  (0x28)
#define DMA_INTERRUPT_PT_StatusTfr_HIGH_OFFSET  (0x2c)
#define DMA_INTERRUPT_PT_StatusBlock_LOW_OFFSET  (0x30)
#define DMA_INTERRUPT_PT_StatusBlock_HIGH_OFFSET  (0x34)
#define DMA_INTERRUPT_PT_StatusSrcTran_LOW_OFFSET  (0x38)
#define DMA_INTERRUPT_PT_StatusSrcTran_HIGH_OFFSET  (0x3c)
#define DMA_INTERRUPT_PT_StatusDstTran_LOW_OFFSET  (0x40)
#define DMA_INTERRUPT_PT_StatusDstTran_HIGH_OFFSET  (0x44)
#define DMA_INTERRUPT_PT_StatusErr_LOW_OFFSET  (0x48)
#define DMA_INTERRUPT_PT_StatusErr_HIGH_OFFSET  (0x4c)
#define DMA_INTERRUPT_PT_MaskTfr_LOW_OFFSET  (0x50)
#define DMA_INTERRUPT_PT_MaskTfr_HIGH_OFFSET  (0x54)
#define DMA_INTERRUPT_PT_MaskBlock_LOW_OFFSET  (0x58)
#define DMA_INTERRUPT_PT_MaskBlock_HIGH_OFFSET  (0x5c)
#define DMA_INTERRUPT_PT_MaskSrcTran_LOW_OFFSET  (0x60)
#define DMA_INTERRUPT_PT_MaskSrcTran_HIGH_OFFSET  (0x64)
#define DMA_INTERRUPT_PT_MaskDstTran_LOW_OFFSET  (0x68)
#define DMA_INTERRUPT_PT_MaskDstTran_HIGH_OFFSET  (0x6c)
#define DMA_INTERRUPT_PT_MaskErr_LOW_OFFSET  (0x70)
#define DMA_INTERRUPT_PT_MaskErr_HIGH_OFFSET  (0x74)
#define DMA_INTERRUPT_PT_ClearTfr_LOW_OFFSET  (0x78)
#define DMA_INTERRUPT_PT_ClearTfr_HIGH_OFFSET  (0x7c)
#define DMA_INTERRUPT_PT_ClearBlock_LOW_OFFSET  (0x80)
#define DMA_INTERRUPT_PT_ClearBlock_HIGH_OFFSET  (0x84)
#define DMA_INTERRUPT_PT_ClearSrcTran_LOW_OFFSET  (0x88)
#define DMA_INTERRUPT_PT_ClearSrcTran_HIGH_OFFSET  (0x8c)
#define DMA_INTERRUPT_PT_ClearDstTran_LOW_OFFSET  (0x90)
#define DMA_INTERRUPT_PT_ClearDstTran_HIGH_OFFSET  (0x94)
#define DMA_INTERRUPT_PT_ClearErr_LOW_OFFSET  (0x98)
#define DMA_INTERRUPT_PT_ClearErr_HIGH_OFFSET  (0x9c)
#define DMA_INTERRUPT_PT_StatusInt_LOW_OFFSET  (0xA0)
#define DMA_INTERRUPT_PT_StatusInt_HIGH_OFFSET  (0xa4)


typedef union _DMA_INTERRUPT_PT_RawTfr_LOW_U_ {
    unsigned int w32; 
    struct { 
	unsigned raw :8;
	unsigned rsvd_rawtfr :24;
    } b;
} DMA_INTERRUPT_PT_RawTfr_LOW_UT;

typedef union _DMA_INTERRUPT_PT_RawTfr_HIGH_U_ {
    unsigned int w32; 
    struct { 
	unsigned rsvd_rawtfr :32;
    } b;
} DMA_INTERRUPT_PT_RawTfr_HIGH_UT;

typedef union _DMA_INTERRUPT_PT_RawBlock_LOW_U_ {
    unsigned int w32; 
    struct { 
	unsigned raw :8;
	unsigned rsvd_rawblock :24;
    } b;
} DMA_INTERRUPT_PT_RawBlock_LOW_UT;

typedef union _DMA_INTERRUPT_PT_RawBlock_HIGH_U_ {
    unsigned int w32; 
    struct { 
	unsigned rsvd_rawblock :32;
    } b;
} DMA_INTERRUPT_PT_RawBlock_HIGH_UT;

typedef union _DMA_INTERRUPT_PT_RawSrcTran_LOW_U_ {
    unsigned int w32; 
    struct { 
	unsigned raw :8;
	unsigned rsvd_rawsrctran :24;
    } b;
} DMA_INTERRUPT_PT_RawSrcTran_LOW_UT;

typedef union _DMA_INTERRUPT_PT_RawSrcTran_HIGH_U_ {
    unsigned int w32; 
    struct { 
	unsigned rsvd_rawsrctran :32;
    } b;
} DMA_INTERRUPT_PT_RawSrcTran_HIGH_UT;

typedef union _DMA_INTERRUPT_PT_RawDstTran_LOW_U_ {
    unsigned int w32; 
    struct { 
	unsigned raw :8;
	unsigned rsvd_rawdsttran :24;
    } b;
} DMA_INTERRUPT_PT_RawDstTran_LOW_UT;

typedef union _DMA_INTERRUPT_PT_RawDstTran_HIGH_U_ {
    unsigned int w32; 
    struct { 
	unsigned rsvd_rawdsttran :32;
    } b;
} DMA_INTERRUPT_PT_RawDstTran_HIGH_UT;

typedef union _DMA_INTERRUPT_PT_RawErr_LOW_U_ {
    unsigned int w32; 
    struct { 
	unsigned raw :8;
	unsigned rsvd_rawerr :24;
    } b;
} DMA_INTERRUPT_PT_RawErr_LOW_UT;

typedef union _DMA_INTERRUPT_PT_RawErr_HIGH_U_ {
    unsigned int w32; 
    struct { 
	unsigned rsvd_rawerr :32;
    } b;
} DMA_INTERRUPT_PT_RawErr_HIGH_UT;

typedef union _DMA_INTERRUPT_PT_StatusTfr_LOW_U_ {
    unsigned int w32; 
    struct { 
	unsigned status :8;
	unsigned rsvd_statustfr :24;
    } b;
} DMA_INTERRUPT_PT_StatusTfr_LOW_UT;

typedef union _DMA_INTERRUPT_PT_StatusTfr_HIGH_U_ {
    unsigned int w32; 
    struct { 
	unsigned rsvd_statustfr :32;
    } b;
} DMA_INTERRUPT_PT_StatusTfr_HIGH_UT;

typedef union _DMA_INTERRUPT_PT_StatusBlock_LOW_U_ {
    unsigned int w32; 
    struct { 
	unsigned status :8;
	unsigned rsvd_statusblock :24;
    } b;
} DMA_INTERRUPT_PT_StatusBlock_LOW_UT;

typedef union _DMA_INTERRUPT_PT_StatusBlock_HIGH_U_ {
    unsigned int w32; 
    struct { 
	unsigned rsvd_statusblock :32;
    } b;
} DMA_INTERRUPT_PT_StatusBlock_HIGH_UT;

typedef union _DMA_INTERRUPT_PT_StatusSrcTran_LOW_U_ {
    unsigned int w32; 
    struct { 
	unsigned status :8;
	unsigned rsvd_statussrctran :24;
    } b;
} DMA_INTERRUPT_PT_StatusSrcTran_LOW_UT;

typedef union _DMA_INTERRUPT_PT_StatusSrcTran_HIGH_U_ {
    unsigned int w32; 
    struct { 
	unsigned rsvd_statussrctran :32;
    } b;
} DMA_INTERRUPT_PT_StatusSrcTran_HIGH_UT;

typedef union _DMA_INTERRUPT_PT_StatusDstTran_LOW_U_ {
    unsigned int w32; 
    struct { 
	unsigned status :8;
	unsigned rsvd_statusdsttran :24;
    } b;
} DMA_INTERRUPT_PT_StatusDstTran_LOW_UT;

typedef union _DMA_INTERRUPT_PT_StatusDstTran_HIGH_U_ {
    unsigned int w32; 
    struct { 
	unsigned rsvd_statusdsttran :32;
    } b;
} DMA_INTERRUPT_PT_StatusDstTran_HIGH_UT;

typedef union _DMA_INTERRUPT_PT_StatusErr_LOW_U_ {
    unsigned int w32; 
    struct { 
	unsigned status :8;
	unsigned rsvd_statuserr :24;
    } b;
} DMA_INTERRUPT_PT_StatusErr_LOW_UT;

typedef union _DMA_INTERRUPT_PT_StatusErr_HIGH_U_ {
    unsigned int w32; 
    struct { 
	unsigned rsvd_statuserr :32;
    } b;
} DMA_INTERRUPT_PT_StatusErr_HIGH_UT;

typedef union _DMA_INTERRUPT_PT_MaskTfr_LOW_U_ {
    unsigned int w32; 
    struct { 
	unsigned int_mask :8;
	unsigned int_mask_we :8;
	unsigned rsvd_1_masktfr :16;
    } b;
} DMA_INTERRUPT_PT_MaskTfr_LOW_UT;

typedef union _DMA_INTERRUPT_PT_MaskTfr_HIGH_U_ {
    unsigned int w32; 
    struct { 
	unsigned rsvd_1_masktfr :32;
    } b;
} DMA_INTERRUPT_PT_MaskTfr_HIGH_UT;

typedef union _DMA_INTERRUPT_PT_MaskBlock_LOW_U_ {
    unsigned int w32; 
    struct { 
	unsigned int_mask :8;
	unsigned int_mask_we :8;
	unsigned rsvd_1_maskblock :16;
    } b;
} DMA_INTERRUPT_PT_MaskBlock_LOW_UT;

typedef union _DMA_INTERRUPT_PT_MaskBlock_HIGH_U_ {
    unsigned int w32; 
    struct { 
	unsigned rsvd_1_maskblock :32;
    } b;
} DMA_INTERRUPT_PT_MaskBlock_HIGH_UT;

typedef union _DMA_INTERRUPT_PT_MaskSrcTran_LOW_U_ {
    unsigned int w32; 
    struct { 
	unsigned int_mask :8;
	unsigned int_mask_we :8;
	unsigned rsvd_1_masksrctran :16;
    } b;
} DMA_INTERRUPT_PT_MaskSrcTran_LOW_UT;

typedef union _DMA_INTERRUPT_PT_MaskSrcTran_HIGH_U_ {
    unsigned int w32; 
    struct { 
	unsigned rsvd_1_masksrctran :32;
    } b;
} DMA_INTERRUPT_PT_MaskSrcTran_HIGH_UT;

typedef union _DMA_INTERRUPT_PT_MaskDstTran_LOW_U_ {
    unsigned int w32; 
    struct { 
	unsigned int_mask :8;
	unsigned int_mask_we :8;
	unsigned rsvd_1_maskdsttran :16;
    } b;
} DMA_INTERRUPT_PT_MaskDstTran_LOW_UT;

typedef union _DMA_INTERRUPT_PT_MaskDstTran_HIGH_U_ {
    unsigned int w32; 
    struct { 
	unsigned rsvd_1_maskdsttran :32;
    } b;
} DMA_INTERRUPT_PT_MaskDstTran_HIGH_UT;

typedef union _DMA_INTERRUPT_PT_MaskErr_LOW_U_ {
    unsigned int w32; 
    struct { 
	unsigned int_mask :8;
	unsigned int_mask_we :8;
	unsigned rsvd_1_maskerr :16;
    } b;
} DMA_INTERRUPT_PT_MaskErr_LOW_UT;

typedef union _DMA_INTERRUPT_PT_MaskErr_HIGH_U_ {
    unsigned int w32; 
    struct { 
	unsigned rsvd_1_maskerr :32;
    } b;
} DMA_INTERRUPT_PT_MaskErr_HIGH_UT;

typedef union _DMA_INTERRUPT_PT_ClearTfr_LOW_U_ {
    unsigned int w32; 
    struct { 
	unsigned clear :8;
	unsigned rsvd_cleartfr :24;
    } b;
} DMA_INTERRUPT_PT_ClearTfr_LOW_UT;

typedef union _DMA_INTERRUPT_PT_ClearTfr_HIGH_U_ {
    unsigned int w32; 
    struct { 
	unsigned rsvd_cleartfr :32;
    } b;
} DMA_INTERRUPT_PT_ClearTfr_HIGH_UT;

typedef union _DMA_INTERRUPT_PT_ClearBlock_LOW_U_ {
    unsigned int w32; 
    struct { 
	unsigned clear :8;
	unsigned rsvd_clearblock :24;
    } b;
} DMA_INTERRUPT_PT_ClearBlock_LOW_UT;

typedef union _DMA_INTERRUPT_PT_ClearBlock_HIGH_U_ {
    unsigned int w32; 
    struct { 
	unsigned rsvd_clearblock :32;
    } b;
} DMA_INTERRUPT_PT_ClearBlock_HIGH_UT;

typedef union _DMA_INTERRUPT_PT_ClearSrcTran_LOW_U_ {
    unsigned int w32; 
    struct { 
	unsigned clear :8;
	unsigned rsvd_clearsrctran :24;
    } b;
} DMA_INTERRUPT_PT_ClearSrcTran_LOW_UT;

typedef union _DMA_INTERRUPT_PT_ClearSrcTran_HIGH_U_ {
    unsigned int w32; 
    struct { 
	unsigned rsvd_clearsrctran :32;
    } b;
} DMA_INTERRUPT_PT_ClearSrcTran_HIGH_UT;

typedef union _DMA_INTERRUPT_PT_ClearDstTran_LOW_U_ {
    unsigned int w32; 
    struct { 
	unsigned clear :8;
	unsigned rsvd_cleardsttran :24;
    } b;
} DMA_INTERRUPT_PT_ClearDstTran_LOW_UT;

typedef union _DMA_INTERRUPT_PT_ClearDstTran_HIGH_U_ {
    unsigned int w32; 
    struct { 
	unsigned rsvd_cleardsttran :32;
    } b;
} DMA_INTERRUPT_PT_ClearDstTran_HIGH_UT;

typedef union _DMA_INTERRUPT_PT_ClearErr_LOW_U_ {
    unsigned int w32; 
    struct { 
	unsigned clear :8;
	unsigned rsvd_clearerr :24;
    } b;
} DMA_INTERRUPT_PT_ClearErr_LOW_UT;

typedef union _DMA_INTERRUPT_PT_ClearErr_HIGH_U_ {
    unsigned int w32; 
    struct { 
	unsigned rsvd_clearerr :32;
    } b;
} DMA_INTERRUPT_PT_ClearErr_HIGH_UT;

typedef union _DMA_INTERRUPT_PT_StatusInt_LOW_U_ {
    unsigned int w32; 
    struct { 
	unsigned tfr :1;
	unsigned block :1;
	unsigned srct :1;
	unsigned dstt :1;
	unsigned err :1;
	unsigned rsvd_statusint :27;
    } b;
} DMA_INTERRUPT_PT_StatusInt_LOW_UT;

typedef union _DMA_INTERRUPT_PT_StatusInt_HIGH_U_ {
    unsigned int w32; 
    struct { 
	unsigned rsvd_statusint :32;
    } b;
} DMA_INTERRUPT_PT_StatusInt_HIGH_UT;

//module typedef
typedef volatile struct _DMA_INTERRUPT_PT_MODULE_S
{
	DMA_INTERRUPT_PT_RawTfr_LOW_UT RawTfr_LOW;
	DMA_INTERRUPT_PT_RawTfr_HIGH_UT RawTfr_HIGH;
	DMA_INTERRUPT_PT_RawBlock_LOW_UT RawBlock_LOW;
	DMA_INTERRUPT_PT_RawBlock_HIGH_UT RawBlock_HIGH;
	DMA_INTERRUPT_PT_RawSrcTran_LOW_UT RawSrcTran_LOW;
	DMA_INTERRUPT_PT_RawSrcTran_HIGH_UT RawSrcTran_HIGH;
	DMA_INTERRUPT_PT_RawDstTran_LOW_UT RawDstTran_LOW;
	DMA_INTERRUPT_PT_RawDstTran_HIGH_UT RawDstTran_HIGH;
	DMA_INTERRUPT_PT_RawErr_LOW_UT RawErr_LOW;
	DMA_INTERRUPT_PT_RawErr_HIGH_UT RawErr_HIGH;
	DMA_INTERRUPT_PT_StatusTfr_LOW_UT StatusTfr_LOW;
	DMA_INTERRUPT_PT_StatusTfr_HIGH_UT StatusTfr_HIGH;
	DMA_INTERRUPT_PT_StatusBlock_LOW_UT StatusBlock_LOW;
	DMA_INTERRUPT_PT_StatusBlock_HIGH_UT StatusBlock_HIGH;
	DMA_INTERRUPT_PT_StatusSrcTran_LOW_UT StatusSrcTran_LOW;
	DMA_INTERRUPT_PT_StatusSrcTran_HIGH_UT StatusSrcTran_HIGH;
	DMA_INTERRUPT_PT_StatusDstTran_LOW_UT StatusDstTran_LOW;
	DMA_INTERRUPT_PT_StatusDstTran_HIGH_UT StatusDstTran_HIGH;
	DMA_INTERRUPT_PT_StatusErr_LOW_UT StatusErr_LOW;
	DMA_INTERRUPT_PT_StatusErr_HIGH_UT StatusErr_HIGH;
	DMA_INTERRUPT_PT_MaskTfr_LOW_UT MaskTfr_LOW;
	DMA_INTERRUPT_PT_MaskTfr_HIGH_UT MaskTfr_HIGH;
	DMA_INTERRUPT_PT_MaskBlock_LOW_UT MaskBlock_LOW;
	DMA_INTERRUPT_PT_MaskBlock_HIGH_UT MaskBlock_HIGH;
	DMA_INTERRUPT_PT_MaskSrcTran_LOW_UT MaskSrcTran_LOW;
	DMA_INTERRUPT_PT_MaskSrcTran_HIGH_UT MaskSrcTran_HIGH;
	DMA_INTERRUPT_PT_MaskDstTran_LOW_UT MaskDstTran_LOW;
	DMA_INTERRUPT_PT_MaskDstTran_HIGH_UT MaskDstTran_HIGH;
	DMA_INTERRUPT_PT_MaskErr_LOW_UT MaskErr_LOW;
	DMA_INTERRUPT_PT_MaskErr_HIGH_UT MaskErr_HIGH;
	DMA_INTERRUPT_PT_ClearTfr_LOW_UT ClearTfr_LOW;
	DMA_INTERRUPT_PT_ClearTfr_HIGH_UT ClearTfr_HIGH;
	DMA_INTERRUPT_PT_ClearBlock_LOW_UT ClearBlock_LOW;
	DMA_INTERRUPT_PT_ClearBlock_HIGH_UT ClearBlock_HIGH;
	DMA_INTERRUPT_PT_ClearSrcTran_LOW_UT ClearSrcTran_LOW;
	DMA_INTERRUPT_PT_ClearSrcTran_HIGH_UT ClearSrcTran_HIGH;
	DMA_INTERRUPT_PT_ClearDstTran_LOW_UT ClearDstTran_LOW;
	DMA_INTERRUPT_PT_ClearDstTran_HIGH_UT ClearDstTran_HIGH;
	DMA_INTERRUPT_PT_ClearErr_LOW_UT ClearErr_LOW;
	DMA_INTERRUPT_PT_ClearErr_HIGH_UT ClearErr_HIGH;
	DMA_INTERRUPT_PT_StatusInt_LOW_UT StatusInt_LOW;
	DMA_INTERRUPT_PT_StatusInt_HIGH_UT StatusInt_HIGH;
} DMA_INTERRUPT_PT_MODULE_ST;



//define parameterized depths


//define enumerated types


//define offsets
#define DMA_MISC_PT_DmaCfgReg_LOW_OFFSET  (0x0)
#define DMA_MISC_PT_DmaCfgReg_HIGH_OFFSET  (0x4)
#define DMA_MISC_PT_ChEnReg_LOW_OFFSET  (0x8)
#define DMA_MISC_PT_ChEnReg_HIGH_OFFSET  (0xc)
#define DMA_MISC_PT_DmaIdReg_LOW_OFFSET  (0x10)
#define DMA_MISC_PT_DmaIdReg_HIGH_OFFSET  (0x14)
#define DMA_MISC_PT_DmaTestReg_LOW_OFFSET  (0x18)
#define DMA_MISC_PT_DmaTestReg_HIGH_OFFSET  (0x1c)
#define DMA_MISC_PT_DMA_COMP_PARAMS_6_LOW_OFFSET  (0x30)
#define DMA_MISC_PT_DMA_COMP_PARAMS_6_HIGH_OFFSET  (0x34)
#define DMA_MISC_PT_DMA_COMP_PARAMS_5_LOW_OFFSET  (0x38)
#define DMA_MISC_PT_DMA_COMP_PARAMS_5_HIGH_OFFSET  (0x3c)
#define DMA_MISC_PT_DMA_COMP_PARAMS_4_LOW_OFFSET  (0x40)
#define DMA_MISC_PT_DMA_COMP_PARAMS_4_HIGH_OFFSET  (0x44)
#define DMA_MISC_PT_DMA_COMP_PARAMS_3_LOW_OFFSET  (0x48)
#define DMA_MISC_PT_DMA_COMP_PARAMS_3_HIGH_OFFSET  (0x4c)
#define DMA_MISC_PT_DMA_COMP_PARAMS_2_LOW_OFFSET  (0x50)
#define DMA_MISC_PT_DMA_COMP_PARAMS_2_HIGH_OFFSET  (0x54)
#define DMA_MISC_PT_DMA_COMP_PARAMS_1_LOW_OFFSET  (0x58)
#define DMA_MISC_PT_DMA_COMP_PARAMS_1_HIGH_OFFSET  (0x5c)
#define DMA_MISC_PT_DmaCompsID_LOW_OFFSET  (0x60)
#define DMA_MISC_PT_DmaCompsID_HIGH_OFFSET  (0x64)


typedef union _DMA_MISC_PT_DmaCfgReg_LOW_U_ {
    unsigned int w32; 
    struct { 
	unsigned dma_en :1;
	unsigned rsvd_dmacfgreg :31;
    } b;
} DMA_MISC_PT_DmaCfgReg_LOW_UT;

typedef union _DMA_MISC_PT_DmaCfgReg_HIGH_U_ {
    unsigned int w32; 
    struct { 
	unsigned rsvd_dmacfgreg :32;
    } b;
} DMA_MISC_PT_DmaCfgReg_HIGH_UT;

typedef union _DMA_MISC_PT_ChEnReg_LOW_U_ {
    unsigned int w32; 
    struct { 
	unsigned ch_en :8;
	unsigned ch_en_we :8;
	unsigned rsvd_1_chenreg :16;
    } b;
} DMA_MISC_PT_ChEnReg_LOW_UT;

typedef union _DMA_MISC_PT_ChEnReg_HIGH_U_ {
    unsigned int w32; 
    struct { 
	unsigned rsvd_1_chenreg :32;
    } b;
} DMA_MISC_PT_ChEnReg_HIGH_UT;

typedef union _DMA_MISC_PT_DmaIdReg_LOW_U_ {
    unsigned int w32; 
    struct { 
	unsigned dma_id :32;
    } b;
} DMA_MISC_PT_DmaIdReg_LOW_UT;

typedef union _DMA_MISC_PT_DmaIdReg_HIGH_U_ {
    unsigned int w32; 
    struct { 
	unsigned rsvd_dmaidreg :32;
    } b;
} DMA_MISC_PT_DmaIdReg_HIGH_UT;

typedef union _DMA_MISC_PT_DmaTestReg_LOW_U_ {
    unsigned int w32; 
    struct { 
	unsigned test_slv_if :1;
	unsigned rsvd_dmatestreg :31;
    } b;
} DMA_MISC_PT_DmaTestReg_LOW_UT;

typedef union _DMA_MISC_PT_DmaTestReg_HIGH_U_ {
    unsigned int w32; 
    struct { 
	unsigned rsvd_dmatestreg :32;
    } b;
} DMA_MISC_PT_DmaTestReg_HIGH_UT;

typedef union _DMA_MISC_PT_DMA_COMP_PARAMS_6_LOW_U_ {
    unsigned int w32; 
    struct { 
	unsigned rsvd_dma_comp_params_6 :32;
    } b;
} DMA_MISC_PT_DMA_COMP_PARAMS_6_LOW_UT;

typedef union _DMA_MISC_PT_DMA_COMP_PARAMS_6_HIGH_U_ {
    unsigned int w32; 
    struct { 
	unsigned ch7_dtw :3;
	unsigned ch7_stw :3;
	unsigned ch7_stat_dst :1;
	unsigned ch7_stat_src :1;
	unsigned ch7_dst_sca_en :1;
	unsigned ch7_src_gat_en :1;
	unsigned ch7_lock_en :1;
	unsigned ch7_multi_blk_en :1;
	unsigned ch7_ctl_wb_en :1;
	unsigned ch7_hc_llp :1;
	unsigned ch7_fc :2;
	unsigned ch7_max_mult_size :3;
	unsigned ch7_dms :3;
	unsigned ch7_lms :3;
	unsigned ch7_sms :3;
	unsigned ch7_fifo_depth :3;
	unsigned rsvd_1_dma_comp_params_6 :1;
    } b;
} DMA_MISC_PT_DMA_COMP_PARAMS_6_HIGH_UT;

typedef union _DMA_MISC_PT_DMA_COMP_PARAMS_5_LOW_U_ {
    unsigned int w32; 
    struct { 
	unsigned ch6_dtw :3;
	unsigned ch6_stw :3;
	unsigned ch6_stat_dst :1;
	unsigned ch6_stat_src :1;
	unsigned ch6_dst_sca_en :1;
	unsigned ch6_src_gat_en :1;
	unsigned ch6_lock_en :1;
	unsigned ch6_multi_blk_en :1;
	unsigned ch6_ctl_wb_en :1;
	unsigned ch6_hc_llp :1;
	unsigned ch6_fc :2;
	unsigned ch6_max_mult_size :3;
	unsigned ch6_dms :3;
	unsigned ch6_lms :3;
	unsigned ch6_sms :3;
	unsigned ch6_fifo_depth :3;
	unsigned rsvd_dma_comp_params_5 :1;
    } b;
} DMA_MISC_PT_DMA_COMP_PARAMS_5_LOW_UT;

typedef union _DMA_MISC_PT_DMA_COMP_PARAMS_5_HIGH_U_ {
    unsigned int w32; 
    struct { 
	unsigned ch5_dtw :3;
	unsigned ch5_stw :3;
	unsigned ch5_stat_dst :1;
	unsigned ch5_stat_src :1;
	unsigned ch5_dst_sca_en :1;
	unsigned ch5_src_gat_en :1;
	unsigned ch5_lock_en :1;
	unsigned ch5_multi_blk_en :1;
	unsigned ch5_ctl_wb_en :1;
	unsigned ch5_hc_llp :1;
	unsigned ch5_fc :2;
	unsigned ch5_max_mult_size :3;
	unsigned ch5_dms :3;
	unsigned ch5_lms :3;
	unsigned ch5_sms :3;
	unsigned ch5_fifo_depth :3;
	unsigned rsvd_1_dma_comp_params_5 :1;
    } b;
} DMA_MISC_PT_DMA_COMP_PARAMS_5_HIGH_UT;

typedef union _DMA_MISC_PT_DMA_COMP_PARAMS_4_LOW_U_ {
    unsigned int w32; 
    struct { 
	unsigned ch4_dtw :3;
	unsigned ch4_stw :3;
	unsigned ch4_stat_dst :1;
	unsigned ch4_stat_src :1;
	unsigned ch4_dst_sca_en :1;
	unsigned ch4_src_gat_en :1;
	unsigned ch4_lock_en :1;
	unsigned ch4_multi_blk_en :1;
	unsigned ch4_ctl_wb_en :1;
	unsigned ch4_hc_llp :1;
	unsigned ch4_fc :2;
	unsigned ch4_max_mult_size :3;
	unsigned ch4_dms :3;
	unsigned ch4_lms :3;
	unsigned ch4_sms :3;
	unsigned ch4_fifo_depth :3;
	unsigned rsvd_dma_comp_params_4 :1;
    } b;
} DMA_MISC_PT_DMA_COMP_PARAMS_4_LOW_UT;

typedef union _DMA_MISC_PT_DMA_COMP_PARAMS_4_HIGH_U_ {
    unsigned int w32; 
    struct { 
	unsigned ch3_dtw :3;
	unsigned ch3_stw :3;
	unsigned ch3_stat_dst :1;
	unsigned ch3_stat_src :1;
	unsigned ch3_dst_sca_en :1;
	unsigned ch3_src_gat_en :1;
	unsigned ch3_lock_en :1;
	unsigned ch3_multi_blk_en :1;
	unsigned ch3_ctl_wb_en :1;
	unsigned ch3_hc_llp :1;
	unsigned ch3_fc :2;
	unsigned ch3_max_mult_size :3;
	unsigned ch3_dms :3;
	unsigned ch3_lms :3;
	unsigned ch3_sms :3;
	unsigned ch3_fifo_depth :3;
	unsigned rsvd_1_dma_comp_params_4 :1;
    } b;
} DMA_MISC_PT_DMA_COMP_PARAMS_4_HIGH_UT;

typedef union _DMA_MISC_PT_DMA_COMP_PARAMS_3_LOW_U_ {
    unsigned int w32; 
    struct { 
	unsigned ch2_dtw :3;
	unsigned ch2_stw :3;
	unsigned ch2_stat_dst :1;
	unsigned ch2_stat_src :1;
	unsigned ch2_dst_sca_en :1;
	unsigned ch2_src_gat_en :1;
	unsigned ch2_lock_en :1;
	unsigned ch2_multi_blk_en :1;
	unsigned ch2_ctl_wb_en :1;
	unsigned ch2_hc_llp :1;
	unsigned ch2_fc :2;
	unsigned ch2_max_mult_size :3;
	unsigned ch2_dms :3;
	unsigned ch2_lms :3;
	unsigned ch2_sms :3;
	unsigned ch2_fifo_depth :3;
	unsigned rsvd_dma_comp_params_3 :1;
    } b;
} DMA_MISC_PT_DMA_COMP_PARAMS_3_LOW_UT;

typedef union _DMA_MISC_PT_DMA_COMP_PARAMS_3_HIGH_U_ {
    unsigned int w32; 
    struct { 
	unsigned ch1_dtw :3;
	unsigned ch1_stw :3;
	unsigned ch1_stat_dst :1;
	unsigned ch1_stat_src :1;
	unsigned ch1_dst_sca_en :1;
	unsigned ch1_src_gat_en :1;
	unsigned ch1_lock_en :1;
	unsigned ch1_multi_blk_en :1;
	unsigned ch1_ctl_wb_en :1;
	unsigned ch1_hc_llp :1;
	unsigned ch1_fc :2;
	unsigned ch1_max_mult_size :3;
	unsigned ch1_dms :3;
	unsigned ch1_lms :3;
	unsigned ch1_sms :3;
	unsigned ch1_fifo_depth :3;
	unsigned rsvd_1_dma_comp_params_3 :1;
    } b;
} DMA_MISC_PT_DMA_COMP_PARAMS_3_HIGH_UT;

typedef union _DMA_MISC_PT_DMA_COMP_PARAMS_2_LOW_U_ {
    unsigned int w32; 
    struct { 
	unsigned ch0_dtw :3;
	unsigned ch0_stw :3;
	unsigned ch0_stat_dst :1;
	unsigned ch0_stat_src :1;
	unsigned ch0_dst_sca_en :1;
	unsigned ch0_src_gat_en :1;
	unsigned ch0_lock_en :1;
	unsigned ch0_multi_blk_en :1;
	unsigned ch0_ctl_wb_en :1;
	unsigned ch0_hc_llp :1;
	unsigned ch0_fc :2;
	unsigned ch0_max_mult_size :3;
	unsigned ch0_dms :3;
	unsigned ch0_lms :3;
	unsigned ch0_sms :3;
	unsigned ch0_fifo_depth :3;
	unsigned rsvd_dma_comp_params_2 :1;
    } b;
} DMA_MISC_PT_DMA_COMP_PARAMS_2_LOW_UT;

typedef union _DMA_MISC_PT_DMA_COMP_PARAMS_2_HIGH_U_ {
    unsigned int w32; 
    struct { 
	unsigned cho_multi_blk_type :4;
	unsigned ch1_multi_blk_type :4;
	unsigned ch2_multi_blk_type :4;
	unsigned ch3_multi_blk_type :4;
	unsigned ch4_multi_blk_type :4;
	unsigned ch5_multi_blk_type :4;
	unsigned ch6_multi_blk_type :4;
	unsigned ch7_multi_blk_type :4;
    } b;
} DMA_MISC_PT_DMA_COMP_PARAMS_2_HIGH_UT;

typedef union _DMA_MISC_PT_DMA_COMP_PARAMS_1_LOW_U_ {
    unsigned int w32; 
    struct { 
	unsigned cho_max_blk_size :4;
	unsigned ch1_max_blk_size :4;
	unsigned ch2_max_blk_size :4;
	unsigned ch3_max_blk_size :4;
	unsigned ch4_max_blk_size :4;
	unsigned ch5_max_blk_size :4;
	unsigned ch6_max_blk_size :4;
	unsigned ch7_max_blk_size :4;
    } b;
} DMA_MISC_PT_DMA_COMP_PARAMS_1_LOW_UT;

typedef union _DMA_MISC_PT_DMA_COMP_PARAMS_1_HIGH_U_ {
    unsigned int w32; 
    struct { 
	unsigned big_endian :1;
	unsigned intr_io :2;
	unsigned max_abrst :1;
	unsigned rsvd_dma_comp_params_1 :4;
	unsigned num_channels :3;
	unsigned num_master_int :2;
	unsigned s_hdata_width :2;
	unsigned m4_hdata_width :2;
	unsigned m3_hdata_width :2;
	unsigned m2_hdata_width :2;
	unsigned m1_hdata_width :2;
	unsigned num_hs_int :5;
	unsigned add_encoded_params :1;
	unsigned static_endian_select :1;
	unsigned rsvd_1_dma_comp_params_1 :2;
    } b;
} DMA_MISC_PT_DMA_COMP_PARAMS_1_HIGH_UT;

typedef union _DMA_MISC_PT_DmaCompsID_LOW_U_ {
    unsigned int w32; 
    struct { 
	unsigned dma_comp_type :32;
    } b;
} DMA_MISC_PT_DmaCompsID_LOW_UT;

typedef union _DMA_MISC_PT_DmaCompsID_HIGH_U_ {
    unsigned int w32; 
    struct { 
	unsigned dma_comp_version :32;
    } b;
} DMA_MISC_PT_DmaCompsID_HIGH_UT;

//module typedef
typedef volatile struct _DMA_MISC_PT_MODULE_S
{
	DMA_MISC_PT_DmaCfgReg_LOW_UT DmaCfgReg_LOW;
	DMA_MISC_PT_DmaCfgReg_HIGH_UT DmaCfgReg_HIGH;
	DMA_MISC_PT_ChEnReg_LOW_UT ChEnReg_LOW;
	DMA_MISC_PT_ChEnReg_HIGH_UT ChEnReg_HIGH;
	DMA_MISC_PT_DmaIdReg_LOW_UT DmaIdReg_LOW;
	DMA_MISC_PT_DmaIdReg_HIGH_UT DmaIdReg_HIGH;
	DMA_MISC_PT_DmaTestReg_LOW_UT DmaTestReg_LOW;
	DMA_MISC_PT_DmaTestReg_HIGH_UT DmaTestReg_HIGH;
	unsigned rsv_1c_30 [4];
	DMA_MISC_PT_DMA_COMP_PARAMS_6_LOW_UT DMA_COMP_PARAMS_6_LOW;
	DMA_MISC_PT_DMA_COMP_PARAMS_6_HIGH_UT DMA_COMP_PARAMS_6_HIGH;
	DMA_MISC_PT_DMA_COMP_PARAMS_5_LOW_UT DMA_COMP_PARAMS_5_LOW;
	DMA_MISC_PT_DMA_COMP_PARAMS_5_HIGH_UT DMA_COMP_PARAMS_5_HIGH;
	DMA_MISC_PT_DMA_COMP_PARAMS_4_LOW_UT DMA_COMP_PARAMS_4_LOW;
	DMA_MISC_PT_DMA_COMP_PARAMS_4_HIGH_UT DMA_COMP_PARAMS_4_HIGH;
	DMA_MISC_PT_DMA_COMP_PARAMS_3_LOW_UT DMA_COMP_PARAMS_3_LOW;
	DMA_MISC_PT_DMA_COMP_PARAMS_3_HIGH_UT DMA_COMP_PARAMS_3_HIGH;
	DMA_MISC_PT_DMA_COMP_PARAMS_2_LOW_UT DMA_COMP_PARAMS_2_LOW;
	DMA_MISC_PT_DMA_COMP_PARAMS_2_HIGH_UT DMA_COMP_PARAMS_2_HIGH;
	DMA_MISC_PT_DMA_COMP_PARAMS_1_LOW_UT DMA_COMP_PARAMS_1_LOW;
	DMA_MISC_PT_DMA_COMP_PARAMS_1_HIGH_UT DMA_COMP_PARAMS_1_HIGH;
	DMA_MISC_PT_DmaCompsID_LOW_UT DmaCompsID_LOW;
	DMA_MISC_PT_DmaCompsID_HIGH_UT DmaCompsID_HIGH;
} DMA_MISC_PT_MODULE_ST;



//define parameterized depths


//define enumerated types


//define offsets
#define DMA_SW_HS_PT_ReqSrcReg_LOW_OFFSET  (0x0)
#define DMA_SW_HS_PT_ReqSrcReg_HIGH_OFFSET  (0x4)
#define DMA_SW_HS_PT_ReqDstReg_LOW_OFFSET  (0x8)
#define DMA_SW_HS_PT_ReqDstReg_HIGH_OFFSET  (0xc)
#define DMA_SW_HS_PT_SglRqSrcReg_LOW_OFFSET  (0x10)
#define DMA_SW_HS_PT_SglRqSrcReg_HIGH_OFFSET  (0x14)
#define DMA_SW_HS_PT_SglRqDstReg_LOW_OFFSET  (0x18)
#define DMA_SW_HS_PT_SglRqDstReg_HIGH_OFFSET  (0x1c)
#define DMA_SW_HS_PT_LstSrcReg_LOW_OFFSET  (0x20)
#define DMA_SW_HS_PT_LstSrcReg_HIGH_OFFSET  (0x24)
#define DMA_SW_HS_PT_LstDstReg_LOW_OFFSET  (0x28)
#define DMA_SW_HS_PT_LstDstReg_HIGH_OFFSET  (0x2c)


typedef union _DMA_SW_HS_PT_ReqSrcReg_LOW_U_ {
    unsigned int w32; 
    struct { 
	unsigned src_req :8;
	unsigned src_req_we :8;
	unsigned rsvd_1_reqsrcreg :16;
    } b;
} DMA_SW_HS_PT_ReqSrcReg_LOW_UT;

typedef union _DMA_SW_HS_PT_ReqSrcReg_HIGH_U_ {
    unsigned int w32; 
    struct { 
	unsigned rsvd_1_reqsrcreg :32;
    } b;
} DMA_SW_HS_PT_ReqSrcReg_HIGH_UT;

typedef union _DMA_SW_HS_PT_ReqDstReg_LOW_U_ {
    unsigned int w32; 
    struct { 
	unsigned dst_req :8;
	unsigned dst_req_we :8;
	unsigned rsvd_1_reqdstreg :16;
    } b;
} DMA_SW_HS_PT_ReqDstReg_LOW_UT;

typedef union _DMA_SW_HS_PT_ReqDstReg_HIGH_U_ {
    unsigned int w32; 
    struct { 
	unsigned rsvd_1_reqdstreg :32;
    } b;
} DMA_SW_HS_PT_ReqDstReg_HIGH_UT;

typedef union _DMA_SW_HS_PT_SglRqSrcReg_LOW_U_ {
    unsigned int w32; 
    struct { 
	unsigned src_sglreq :8;
	unsigned src_sglreq_we :8;
	unsigned rsvd_1_sglrqsrcreg :16;
    } b;
} DMA_SW_HS_PT_SglRqSrcReg_LOW_UT;

typedef union _DMA_SW_HS_PT_SglRqSrcReg_HIGH_U_ {
    unsigned int w32; 
    struct { 
	unsigned rsvd_1_sglrqsrcreg :32;
    } b;
} DMA_SW_HS_PT_SglRqSrcReg_HIGH_UT;

typedef union _DMA_SW_HS_PT_SglRqDstReg_LOW_U_ {
    unsigned int w32; 
    struct { 
	unsigned dst_sglreq :8;
	unsigned dst_sglreq_we :8;
	unsigned rsvd_1_sglrqdstreg :16;
    } b;
} DMA_SW_HS_PT_SglRqDstReg_LOW_UT;

typedef union _DMA_SW_HS_PT_SglRqDstReg_HIGH_U_ {
    unsigned int w32; 
    struct { 
	unsigned rsvd_1_sglrqdstreg :32;
    } b;
} DMA_SW_HS_PT_SglRqDstReg_HIGH_UT;

typedef union _DMA_SW_HS_PT_LstSrcReg_LOW_U_ {
    unsigned int w32; 
    struct { 
	unsigned lstsrc :8;
	unsigned lstsrc_we :8;
	unsigned rsvd_1_lstsrcreg :16;
    } b;
} DMA_SW_HS_PT_LstSrcReg_LOW_UT;

typedef union _DMA_SW_HS_PT_LstSrcReg_HIGH_U_ {
    unsigned int w32; 
    struct { 
	unsigned rsvd_1_lstsrcreg :32;
    } b;
} DMA_SW_HS_PT_LstSrcReg_HIGH_UT;

typedef union _DMA_SW_HS_PT_LstDstReg_LOW_U_ {
    unsigned int w32; 
    struct { 
	unsigned lstdst :8;
	unsigned lstdst_we :8;
	unsigned rsvd_1_lstdstreg :16;
    } b;
} DMA_SW_HS_PT_LstDstReg_LOW_UT;

typedef union _DMA_SW_HS_PT_LstDstReg_HIGH_U_ {
    unsigned int w32; 
    struct { 
	unsigned rsvd_1_lstdstreg :32;
    } b;
} DMA_SW_HS_PT_LstDstReg_HIGH_UT;

//module typedef
typedef volatile struct _DMA_SW_HS_PT_MODULE_S
{
	DMA_SW_HS_PT_ReqSrcReg_LOW_UT ReqSrcReg_LOW;
	DMA_SW_HS_PT_ReqSrcReg_HIGH_UT ReqSrcReg_HIGH;
	DMA_SW_HS_PT_ReqDstReg_LOW_UT ReqDstReg_LOW;
	DMA_SW_HS_PT_ReqDstReg_HIGH_UT ReqDstReg_HIGH;
	DMA_SW_HS_PT_SglRqSrcReg_LOW_UT SglRqSrcReg_LOW;
	DMA_SW_HS_PT_SglRqSrcReg_HIGH_UT SglRqSrcReg_HIGH;
	DMA_SW_HS_PT_SglRqDstReg_LOW_UT SglRqDstReg_LOW;
	DMA_SW_HS_PT_SglRqDstReg_HIGH_UT SglRqDstReg_HIGH;
	DMA_SW_HS_PT_LstSrcReg_LOW_UT LstSrcReg_LOW;
	DMA_SW_HS_PT_LstSrcReg_HIGH_UT LstSrcReg_HIGH;
	DMA_SW_HS_PT_LstDstReg_LOW_UT LstDstReg_LOW;
	DMA_SW_HS_PT_LstDstReg_HIGH_UT LstDstReg_HIGH;
} DMA_SW_HS_PT_MODULE_ST;



//define parameterized depths


//define enumerated types


//define offsets
#define EFUSE_PT_EFUSE_CTRL_OFFSET  (0x0)
#define EFUSE_PT_EFUSE_CMD_OFFSET  (0x4)
#define EFUSE_PT_EFUSE_STAT_OFFSET  (0x8)


typedef union _EFUSE_PT_EFUSE_CTRL_U_ {
    unsigned int w32; 
    struct { 
	unsigned write_byte :8;
	unsigned addr :8;
	unsigned prog_cnt_val :12;
	unsigned rsv0 :4;
    } b;
} EFUSE_PT_EFUSE_CTRL_UT;

typedef union _EFUSE_PT_EFUSE_CMD_U_ {
    unsigned int w32; 
    struct { 
	unsigned prog :1;
	unsigned read :1;
	unsigned clr_intr :1;
	unsigned rsv0 :1;
	unsigned mread_tm :3;
	unsigned rsv1 :25;
    } b;
} EFUSE_PT_EFUSE_CMD_UT;

typedef union _EFUSE_PT_EFUSE_STAT_U_ {
    unsigned int w32; 
    struct { 
	unsigned read_byte :8;
	unsigned read_done :1;
	unsigned prog_done :1;
	unsigned prog_intr :1;
	unsigned rsv0 :21;
    } b;
} EFUSE_PT_EFUSE_STAT_UT;

//module typedef
typedef volatile struct _EFUSE_PT_MODULE_S
{
	EFUSE_PT_EFUSE_CTRL_UT EFUSE_CTRL;
	EFUSE_PT_EFUSE_CMD_UT EFUSE_CMD;
	EFUSE_PT_EFUSE_STAT_UT EFUSE_STAT;
} EFUSE_PT_MODULE_ST;



//define parameterized depths


//define enumerated types


//define offsets
#define I2C_PT_IC_CON_OFFSET  (0x0)
#define I2C_PT_IC_TAR_OFFSET  (0x4)
#define I2C_PT_IC_SAR_OFFSET  (0x8)
#define I2C_PT_IC_HS_MADDR_OFFSET  (0xC)
#define I2C_PT_IC_DATA_CMD_OFFSET  (0x10)
#define I2C_PT_IC_SS_SCL_HCNT_OFFSET  (0x14)
#define I2C_PT_IC_SS_SCL_LCNT_OFFSET  (0x18)
#define I2C_PT_IC_FS_SCL_HCNT_OFFSET  (0x1C)
#define I2C_PT_IC_FS_SCL_LCNT_OFFSET  (0x20)
#define I2C_PT_IC_HS_SCL_HCNT_OFFSET  (0x24)
#define I2C_PT_IC_HS_SCL_LCNT_OFFSET  (0x28)
#define I2C_PT_IC_INTR_STAT_OFFSET  (0x2C)
#define I2C_PT_IC_INTR_MASK_OFFSET  (0x30)
#define I2C_PT_IC_RAW_INTR_STAT_OFFSET  (0x34)
#define I2C_PT_IC_RX_TL_OFFSET  (0x38)
#define I2C_PT_IC_TX_TL_OFFSET  (0x3C)
#define I2C_PT_IC_CLR_INTR_OFFSET  (0x40)
#define I2C_PT_IC_CLR_RX_UNDER_OFFSET  (0x44)
#define I2C_PT_IC_CLR_RX_OVER_OFFSET  (0x48)
#define I2C_PT_IC_CLR_TX_OVER_OFFSET  (0x4C)
#define I2C_PT_IC_CLR_RD_REQ_OFFSET  (0x50)
#define I2C_PT_IC_CLR_TX_ABRT_OFFSET  (0x54)
#define I2C_PT_IC_CLR_RX_DONE_OFFSET  (0x58)
#define I2C_PT_IC_CLR_ACTIVITY_OFFSET  (0x5C)
#define I2C_PT_IC_CLR_STOP_DET_OFFSET  (0x60)
#define I2C_PT_IC_CLR_START_DET_OFFSET  (0x64)
#define I2C_PT_IC_CLR_GEN_CALL_OFFSET  (0x68)
#define I2C_PT_IC_ENABLE_OFFSET  (0x6C)
#define I2C_PT_IC_STATUS_OFFSET  (0x70)
#define I2C_PT_IC_TXFLR_OFFSET  (0x74)
#define I2C_PT_IC_RXFLR_OFFSET  (0x78)
#define I2C_PT_IC_SDA_HOLD_OFFSET  (0x7C)
#define I2C_PT_IC_TX_ABRT_SOURCE_OFFSET  (0x80)
#define I2C_PT_IC_DMA_CR_OFFSET  (0x88)
#define I2C_PT_IC_DMA_TDLR_OFFSET  (0x8C)
#define I2C_PT_IC_DMA_RDLR_OFFSET  (0x90)
#define I2C_PT_IC_SDA_SETUP_OFFSET  (0x94)
#define I2C_PT_IC_ACK_GENERAL_CALL_OFFSET  (0x98)
#define I2C_PT_IC_ENABLE_STATUS_OFFSET  (0x9C)
#define I2C_PT_IC_FS_SPKLEN_OFFSET  (0xA0)
#define I2C_PT_IC_HS_SPKLEN_OFFSET  (0xA4)
#define I2C_PT_IC_SCL_STUCK_AT_LOW_TIMEOUT_OFFSET  (0xAC)
#define I2C_PT_IC_SDA_STUCK_AT_LOW_TIMEOUT_OFFSET  (0xB0)
#define I2C_PT_IC_CLR_SCL_STUCK_DET_OFFSET  (0xB4)
#define I2C_PT_IC_COMP_PARAM_1_OFFSET  (0xF4)
#define I2C_PT_IC_COMP_VERSION_OFFSET  (0xF8)
#define I2C_PT_IC_COMP_TYPE_OFFSET  (0xFC)


typedef union _I2C_PT_IC_CON_U_ {
    unsigned int w32; 
    struct { 
	unsigned master_mode :1;
	unsigned speed :2;
	unsigned ic_10bitaddr_slave :1;
	unsigned ic_10bitaddr_master :1;
	unsigned ic_restart_en :1;
	unsigned ic_slave_disable :1;
	unsigned stop_det_ifaddressed :1;
	unsigned tx_empty_ctrl :1;
	unsigned rx_fifo_full_hld_ctrl :1;
	unsigned stop_det_if_master_active :1;
	unsigned bus_clear_feature_ctrl :1;
	unsigned rsvd_ic_con_1 :4;
	unsigned rsvd_optional_sar_ctrl :1;
	unsigned rsvd_smbus_slave_quick_en :1;
	unsigned rsvd_smbus_arp_en :1;
	unsigned rsvd_smbus_persistent_slv_addr_en :1;
	unsigned rsvd_ic_con_2 :12;
    } b;
} I2C_PT_IC_CON_UT;

typedef union _I2C_PT_IC_TAR_U_ {
    unsigned int w32; 
    struct { 
	unsigned ic_tar :10;
	unsigned gc_or_start :1;
	unsigned special :1;
	unsigned rsvd_ic_10bitaddr_master :1;
	unsigned rsvd_device_id :1;
	unsigned rsvd_ic_tar_1 :2;
	unsigned rsvd_smbus_quick_cmd :1;
	unsigned rsvd_ic_tar_2 :15;
    } b;
} I2C_PT_IC_TAR_UT;

typedef union _I2C_PT_IC_SAR_U_ {
    unsigned int w32; 
    struct { 
	unsigned ic_sar :10;
	unsigned rsvd_ic_sar :22;
    } b;
} I2C_PT_IC_SAR_UT;

typedef union _I2C_PT_IC_HS_MADDR_U_ {
    unsigned int w32; 
    struct { 
	unsigned ic_hs_mar :3;
	unsigned rsvd_ic_hs_mar :29;
    } b;
} I2C_PT_IC_HS_MADDR_UT;

typedef union _I2C_PT_IC_DATA_CMD_U_ {
    unsigned int w32; 
    struct { 
	unsigned dat :8;
	unsigned cmd :1;
	unsigned rsvd_stop :1;
	unsigned rsvd_restart :1;
	unsigned rsvd_first_data_byte :1;
	unsigned rsvd_ic_data_cmd :20;
    } b;
} I2C_PT_IC_DATA_CMD_UT;

typedef union _I2C_PT_IC_SS_SCL_HCNT_U_ {
    unsigned int w32; 
    struct { 
	unsigned ic_ss_scl_hcnt :16;
	unsigned rsvd_ic_ss_scl_high_count :16;
    } b;
} I2C_PT_IC_SS_SCL_HCNT_UT;

typedef union _I2C_PT_IC_SS_SCL_LCNT_U_ {
    unsigned int w32; 
    struct { 
	unsigned ic_ss_scl_lcnt :16;
	unsigned rsvd_ic_ss_scl_low_count :16;
    } b;
} I2C_PT_IC_SS_SCL_LCNT_UT;

typedef union _I2C_PT_IC_FS_SCL_HCNT_U_ {
    unsigned int w32; 
    struct { 
	unsigned ic_fs_scl_hcnt :16;
	unsigned rsvd_ic_fs_scl_hcnt :16;
    } b;
} I2C_PT_IC_FS_SCL_HCNT_UT;

typedef union _I2C_PT_IC_FS_SCL_LCNT_U_ {
    unsigned int w32; 
    struct { 
	unsigned ic_fs_scl_lcnt :16;
	unsigned rsvd_ic_fs_scl_lcnt :16;
    } b;
} I2C_PT_IC_FS_SCL_LCNT_UT;

typedef union _I2C_PT_IC_HS_SCL_HCNT_U_ {
    unsigned int w32; 
    struct { 
	unsigned ic_hs_scl_hcnt :16;
	unsigned rsvd_ic_hs_scl_hcnt :16;
    } b;
} I2C_PT_IC_HS_SCL_HCNT_UT;

typedef union _I2C_PT_IC_HS_SCL_LCNT_U_ {
    unsigned int w32; 
    struct { 
	unsigned ic_hs_scl_lcnt :16;
	unsigned rsvd_ic_hs_scl_low_cnt :16;
    } b;
} I2C_PT_IC_HS_SCL_LCNT_UT;

typedef union _I2C_PT_IC_INTR_STAT_U_ {
    unsigned int w32; 
    struct { 
	unsigned r_rx_under :1;
	unsigned r_rx_over :1;
	unsigned r_rx_full :1;
	unsigned r_tx_over :1;
	unsigned r_tx_empty :1;
	unsigned r_rd_req :1;
	unsigned r_tx_abrt :1;
	unsigned r_rx_done :1;
	unsigned r_activity :1;
	unsigned r_stop_det :1;
	unsigned r_start_det :1;
	unsigned r_gen_call :1;
	unsigned r_restart_det :1;
	unsigned r_master_on_hold :1;
	unsigned r_scl_stuck_at_low :1;
	unsigned rsvd_ic_intr_stat :17;
    } b;
} I2C_PT_IC_INTR_STAT_UT;

typedef union _I2C_PT_IC_INTR_MASK_U_ {
    unsigned int w32; 
    struct { 
	unsigned m_rx_under :1;
	unsigned m_rx_over :1;
	unsigned m_rx_full :1;
	unsigned m_tx_over :1;
	unsigned m_tx_empty :1;
	unsigned m_rd_req :1;
	unsigned m_tx_abrt :1;
	unsigned m_rx_done :1;
	unsigned m_activity :1;
	unsigned m_stop_det :1;
	unsigned m_start_det :1;
	unsigned m_gen_call :1;
	unsigned m_restart_det_read_only :1;
	unsigned m_master_on_hold_read_only :1;
	unsigned m_scl_stuck_at_low :1;
	unsigned rsvd_ic_intr_stat :17;
    } b;
} I2C_PT_IC_INTR_MASK_UT;

typedef union _I2C_PT_IC_RAW_INTR_STAT_U_ {
    unsigned int w32; 
    struct { 
	unsigned rx_under :1;
	unsigned rx_over :1;
	unsigned rx_full :1;
	unsigned tx_over :1;
	unsigned tx_empty :1;
	unsigned rd_req :1;
	unsigned tx_abrt :1;
	unsigned rx_done :1;
	unsigned activity :1;
	unsigned stop_det :1;
	unsigned start_det :1;
	unsigned gen_call :1;
	unsigned restart_det :1;
	unsigned master_on_hold :1;
	unsigned scl_stuck_at_low :1;
	unsigned rsvd_ic_raw_intr_stat :17;
    } b;
} I2C_PT_IC_RAW_INTR_STAT_UT;

typedef union _I2C_PT_IC_RX_TL_U_ {
    unsigned int w32; 
    struct { 
	unsigned rx_tl :8;
	unsigned rsvd_ic_rx_tl :24;
    } b;
} I2C_PT_IC_RX_TL_UT;

typedef union _I2C_PT_IC_TX_TL_U_ {
    unsigned int w32; 
    struct { 
	unsigned tx_tl :8;
	unsigned rsvd_ic_tx_tl :24;
    } b;
} I2C_PT_IC_TX_TL_UT;

typedef union _I2C_PT_IC_CLR_INTR_U_ {
    unsigned int w32; 
    struct { 
	unsigned clr_intr :1;
	unsigned rsvd_ic_clr_intr :31;
    } b;
} I2C_PT_IC_CLR_INTR_UT;

typedef union _I2C_PT_IC_CLR_RX_UNDER_U_ {
    unsigned int w32; 
    struct { 
	unsigned clr_rx_under :1;
	unsigned rsvd_ic_clr_rx_under :31;
    } b;
} I2C_PT_IC_CLR_RX_UNDER_UT;

typedef union _I2C_PT_IC_CLR_RX_OVER_U_ {
    unsigned int w32; 
    struct { 
	unsigned clr_rx_over :1;
	unsigned rsvd_ic_clr_rx_over :31;
    } b;
} I2C_PT_IC_CLR_RX_OVER_UT;

typedef union _I2C_PT_IC_CLR_TX_OVER_U_ {
    unsigned int w32; 
    struct { 
	unsigned clr_tx_over :1;
	unsigned rsvd_ic_clr_tx_over :31;
    } b;
} I2C_PT_IC_CLR_TX_OVER_UT;

typedef union _I2C_PT_IC_CLR_RD_REQ_U_ {
    unsigned int w32; 
    struct { 
	unsigned clr_rd_req :1;
	unsigned rsvd_ic_clr_rd_req :31;
    } b;
} I2C_PT_IC_CLR_RD_REQ_UT;

typedef union _I2C_PT_IC_CLR_TX_ABRT_U_ {
    unsigned int w32; 
    struct { 
	unsigned clr_tx_abrt :1;
	unsigned rsvd_ic_clr_tx_abrt :31;
    } b;
} I2C_PT_IC_CLR_TX_ABRT_UT;

typedef union _I2C_PT_IC_CLR_RX_DONE_U_ {
    unsigned int w32; 
    struct { 
	unsigned clr_rx_done :1;
	unsigned rsvd_ic_clr_rx_done :31;
    } b;
} I2C_PT_IC_CLR_RX_DONE_UT;

typedef union _I2C_PT_IC_CLR_ACTIVITY_U_ {
    unsigned int w32; 
    struct { 
	unsigned clr_activity :1;
	unsigned rsvd_ic_clr_activity :31;
    } b;
} I2C_PT_IC_CLR_ACTIVITY_UT;

typedef union _I2C_PT_IC_CLR_STOP_DET_U_ {
    unsigned int w32; 
    struct { 
	unsigned clr_stop_det :1;
	unsigned rsvd_ic_clr_stop_det :31;
    } b;
} I2C_PT_IC_CLR_STOP_DET_UT;

typedef union _I2C_PT_IC_CLR_START_DET_U_ {
    unsigned int w32; 
    struct { 
	unsigned clr_start_det :1;
	unsigned rsvd_ic_clr_start_det :31;
    } b;
} I2C_PT_IC_CLR_START_DET_UT;

typedef union _I2C_PT_IC_CLR_GEN_CALL_U_ {
    unsigned int w32; 
    struct { 
	unsigned clr_gen_call :1;
	unsigned rsvd_ic_clr_gen_call :31;
    } b;
} I2C_PT_IC_CLR_GEN_CALL_UT;

typedef union _I2C_PT_IC_ENABLE_U_ {
    unsigned int w32; 
    struct { 
	unsigned enable :1;
	unsigned abort :1;
	unsigned tx_cmd_block :1;
	unsigned sda_stuck_recovery_enable :1;
	unsigned rsvd_ic_enable_1 :12;
	unsigned rsvd_smbus_clk_reset :1;
	unsigned rsvd_smbus_suspend_en :1;
	unsigned rsvd_smbus_alert_en :1;
	unsigned rsvd_ic_enable_2 :13;
    } b;
} I2C_PT_IC_ENABLE_UT;

typedef union _I2C_PT_IC_STATUS_U_ {
    unsigned int w32; 
    struct { 
	unsigned activity :1;
	unsigned tfnf :1;
	unsigned tfe :1;
	unsigned rfne :1;
	unsigned rff :1;
	unsigned mst_activity :1;
	unsigned slv_activity :1;
	unsigned rsvd_mst_hold_tx_fifo_empty :1;
	unsigned rsvd_mst_hold_rx_fifo_full :1;
	unsigned rsvd_slv_hold_tx_fifo_empty :1;
	unsigned rsvd_slv_hold_rx_fifo_full :1;
	unsigned sda_stuck_not_recovered :1;
	unsigned rsvd_ic_status_1 :4;
	unsigned rsvd_smbus_quick_cmd_bit :1;
	unsigned rsvd_smbus_slave_addr_valid :1;
	unsigned rsvd_smbus_slave_addr_resolved :1;
	unsigned rsvd_smbus_suspend_status :1;
	unsigned rsvd_smbus_alert_status :1;
	unsigned rsvd_ic_status_2 :11;
    } b;
} I2C_PT_IC_STATUS_UT;

typedef union _I2C_PT_IC_TXFLR_U_ {
    unsigned int w32; 
    struct { 
	unsigned txflr :4;
	unsigned rsvd_txflr :28;
    } b;
} I2C_PT_IC_TXFLR_UT;

typedef union _I2C_PT_IC_RXFLR_U_ {
    unsigned int w32; 
    struct { 
	unsigned rxflr :4;
	unsigned rsvd_rxflr :28;
    } b;
} I2C_PT_IC_RXFLR_UT;

typedef union _I2C_PT_IC_SDA_HOLD_U_ {
    unsigned int w32; 
    struct { 
	unsigned ic_sda_tx_hold :16;
	unsigned ic_sda_rx_hold :8;
	unsigned rsvd_ic_sda_hold :8;
    } b;
} I2C_PT_IC_SDA_HOLD_UT;

typedef union _I2C_PT_IC_TX_ABRT_SOURCE_U_ {
    unsigned int w32; 
    struct { 
	unsigned abrt_7b_addr_noack :1;
	unsigned abrt_10addr1_noack :1;
	unsigned abrt_10addr2_noack :1;
	unsigned abrt_txdata_noack :1;
	unsigned abrt_gcall_noack :1;
	unsigned abrt_gcall_read :1;
	unsigned abrt_hs_ackdet :1;
	unsigned abrt_sbyte_ackdet :1;
	unsigned abrt_hs_norstrt :1;
	unsigned abrt_sbyte_norstrt :1;
	unsigned abrt_10b_rd_norstrt :1;
	unsigned abrt_master_dis :1;
	unsigned arb_lost :1;
	unsigned abrt_slvflush_txfifo :1;
	unsigned abrt_slv_arblost :1;
	unsigned abrt_slvrd_intx :1;
	unsigned abrt_user_abrt :1;
	unsigned abrt_sda_stuck_at_low :1;
	unsigned rsvd_abrt_device_write :3;
	unsigned rsvd_ic_tx_abrt_source :2;
	unsigned tx_flush_cnt :9;
    } b;
} I2C_PT_IC_TX_ABRT_SOURCE_UT;

typedef union _I2C_PT_IC_DMA_CR_U_ {
    unsigned int w32; 
    struct { 
	unsigned rdmae :1;
	unsigned tdmae :1;
	unsigned rsvd_ic_dma_cr_2_31 :30;
    } b;
} I2C_PT_IC_DMA_CR_UT;

typedef union _I2C_PT_IC_DMA_TDLR_U_ {
    unsigned int w32; 
    struct { 
	unsigned dmatdl :3;
	unsigned rsvd_dma_tdlr :29;
    } b;
} I2C_PT_IC_DMA_TDLR_UT;

typedef union _I2C_PT_IC_DMA_RDLR_U_ {
    unsigned int w32; 
    struct { 
	unsigned dmardl :3;
	unsigned rsvd_dma_rdlr :29;
    } b;
} I2C_PT_IC_DMA_RDLR_UT;

typedef union _I2C_PT_IC_SDA_SETUP_U_ {
    unsigned int w32; 
    struct { 
	unsigned sda_setup :8;
	unsigned rsvd_ic_sda_setup :24;
    } b;
} I2C_PT_IC_SDA_SETUP_UT;

typedef union _I2C_PT_IC_ACK_GENERAL_CALL_U_ {
    unsigned int w32; 
    struct { 
	unsigned ack_gen_call :1;
	unsigned rsvd_ic_ack_gen_1_31 :31;
    } b;
} I2C_PT_IC_ACK_GENERAL_CALL_UT;

typedef union _I2C_PT_IC_ENABLE_STATUS_U_ {
    unsigned int w32; 
    struct { 
	unsigned ic_en :1;
	unsigned slv_disabled_while_busy :1;
	unsigned slv_rx_data_lost :1;
	unsigned rsvd_ic_enable_status :29;
    } b;
} I2C_PT_IC_ENABLE_STATUS_UT;

typedef union _I2C_PT_IC_FS_SPKLEN_U_ {
    unsigned int w32; 
    struct { 
	unsigned ic_fs_spklen :8;
	unsigned rsvd_ic_fs_spklen :24;
    } b;
} I2C_PT_IC_FS_SPKLEN_UT;

typedef union _I2C_PT_IC_HS_SPKLEN_U_ {
    unsigned int w32; 
    struct { 
	unsigned ic_hs_spklen :8;
	unsigned rsvd_ic_hs_spklen :24;
    } b;
} I2C_PT_IC_HS_SPKLEN_UT;

typedef union _I2C_PT_IC_SCL_STUCK_AT_LOW_TIMEOUT_U_ {
    unsigned int w32; 
    struct { 
	unsigned ic_scl_stuck_low_timeout :32;
    } b;
} I2C_PT_IC_SCL_STUCK_AT_LOW_TIMEOUT_UT;

typedef union _I2C_PT_IC_SDA_STUCK_AT_LOW_TIMEOUT_U_ {
    unsigned int w32; 
    struct { 
	unsigned ic_sda_stuck_low_timeout :32;
    } b;
} I2C_PT_IC_SDA_STUCK_AT_LOW_TIMEOUT_UT;

typedef union _I2C_PT_IC_CLR_SCL_STUCK_DET_U_ {
    unsigned int w32; 
    struct { 
	unsigned clr_scl_stuck_det :1;
	unsigned rsvd_clr_scl_stuck_det :31;
    } b;
} I2C_PT_IC_CLR_SCL_STUCK_DET_UT;

typedef union _I2C_PT_IC_COMP_PARAM_1_U_ {
    unsigned int w32; 
    struct { 
	unsigned apb_data_width :2;
	unsigned max_speed_mode :2;
	unsigned hc_count_values :1;
	unsigned intr_io :1;
	unsigned has_dma :1;
	unsigned add_encoded_params :1;
	unsigned rx_buffer_depth :8;
	unsigned tx_buffer_depth :8;
	unsigned rsvd_ic_comp_param_1 :8;
    } b;
} I2C_PT_IC_COMP_PARAM_1_UT;

typedef union _I2C_PT_IC_COMP_VERSION_U_ {
    unsigned int w32; 
    struct { 
	unsigned ic_comp_version :32;
    } b;
} I2C_PT_IC_COMP_VERSION_UT;

typedef union _I2C_PT_IC_COMP_TYPE_U_ {
    unsigned int w32; 
    struct { 
	unsigned ic_comp_type :32;
    } b;
} I2C_PT_IC_COMP_TYPE_UT;

//module typedef
typedef volatile struct _I2C_PT_MODULE_S
{
	I2C_PT_IC_CON_UT IC_CON;
	I2C_PT_IC_TAR_UT IC_TAR;
	I2C_PT_IC_SAR_UT IC_SAR;
	I2C_PT_IC_HS_MADDR_UT IC_HS_MADDR;
	I2C_PT_IC_DATA_CMD_UT IC_DATA_CMD;
	I2C_PT_IC_SS_SCL_HCNT_UT IC_SS_SCL_HCNT;
	I2C_PT_IC_SS_SCL_LCNT_UT IC_SS_SCL_LCNT;
	I2C_PT_IC_FS_SCL_HCNT_UT IC_FS_SCL_HCNT;
	I2C_PT_IC_FS_SCL_LCNT_UT IC_FS_SCL_LCNT;
	I2C_PT_IC_HS_SCL_HCNT_UT IC_HS_SCL_HCNT;
	I2C_PT_IC_HS_SCL_LCNT_UT IC_HS_SCL_LCNT;
	I2C_PT_IC_INTR_STAT_UT IC_INTR_STAT;
	I2C_PT_IC_INTR_MASK_UT IC_INTR_MASK;
	I2C_PT_IC_RAW_INTR_STAT_UT IC_RAW_INTR_STAT;
	I2C_PT_IC_RX_TL_UT IC_RX_TL;
	I2C_PT_IC_TX_TL_UT IC_TX_TL;
	I2C_PT_IC_CLR_INTR_UT IC_CLR_INTR;
	I2C_PT_IC_CLR_RX_UNDER_UT IC_CLR_RX_UNDER;
	I2C_PT_IC_CLR_RX_OVER_UT IC_CLR_RX_OVER;
	I2C_PT_IC_CLR_TX_OVER_UT IC_CLR_TX_OVER;
	I2C_PT_IC_CLR_RD_REQ_UT IC_CLR_RD_REQ;
	I2C_PT_IC_CLR_TX_ABRT_UT IC_CLR_TX_ABRT;
	I2C_PT_IC_CLR_RX_DONE_UT IC_CLR_RX_DONE;
	I2C_PT_IC_CLR_ACTIVITY_UT IC_CLR_ACTIVITY;
	I2C_PT_IC_CLR_STOP_DET_UT IC_CLR_STOP_DET;
	I2C_PT_IC_CLR_START_DET_UT IC_CLR_START_DET;
	I2C_PT_IC_CLR_GEN_CALL_UT IC_CLR_GEN_CALL;
	I2C_PT_IC_ENABLE_UT IC_ENABLE;
	I2C_PT_IC_STATUS_UT IC_STATUS;
	I2C_PT_IC_TXFLR_UT IC_TXFLR;
	I2C_PT_IC_RXFLR_UT IC_RXFLR;
	I2C_PT_IC_SDA_HOLD_UT IC_SDA_HOLD;
	I2C_PT_IC_TX_ABRT_SOURCE_UT IC_TX_ABRT_SOURCE;
	unsigned rsv_80_88 [1];
	I2C_PT_IC_DMA_CR_UT IC_DMA_CR;
	I2C_PT_IC_DMA_TDLR_UT IC_DMA_TDLR;
	I2C_PT_IC_DMA_RDLR_UT IC_DMA_RDLR;
	I2C_PT_IC_SDA_SETUP_UT IC_SDA_SETUP;
	I2C_PT_IC_ACK_GENERAL_CALL_UT IC_ACK_GENERAL_CALL;
	I2C_PT_IC_ENABLE_STATUS_UT IC_ENABLE_STATUS;
	I2C_PT_IC_FS_SPKLEN_UT IC_FS_SPKLEN;
	I2C_PT_IC_HS_SPKLEN_UT IC_HS_SPKLEN;
	unsigned rsv_A4_AC [1];
	I2C_PT_IC_SCL_STUCK_AT_LOW_TIMEOUT_UT IC_SCL_STUCK_AT_LOW_TIMEOUT;
	I2C_PT_IC_SDA_STUCK_AT_LOW_TIMEOUT_UT IC_SDA_STUCK_AT_LOW_TIMEOUT;
	I2C_PT_IC_CLR_SCL_STUCK_DET_UT IC_CLR_SCL_STUCK_DET;
	unsigned rsv_B4_F4 [15];
	I2C_PT_IC_COMP_PARAM_1_UT IC_COMP_PARAM_1;
	I2C_PT_IC_COMP_VERSION_UT IC_COMP_VERSION;
	I2C_PT_IC_COMP_TYPE_UT IC_COMP_TYPE;
} I2C_PT_MODULE_ST;



//define parameterized depths


//define enumerated types


//define offsets
#define IO_CTRL_PT_DE_OFFSET  (0x0)
#define IO_CTRL_PT_H_Fn_OFFSET  (0x4)
#define IO_CTRL_PT_SPI_CS_0_OFFSET  (0x8)
#define IO_CTRL_PT_SPI_DIN_OFFSET  (0xc)
#define IO_CTRL_PT_SPI_DOUT_OFFSET  (0x10)
#define IO_CTRL_PT_SPI_SCK_OFFSET  (0x14)
#define IO_CTRL_PT_UART_IN1_OFFSET  (0x18)
#define IO_CTRL_PT_UART_IN2_OFFSET  (0x1c)
#define IO_CTRL_PT_UART_IN3_OFFSET  (0x20)
#define IO_CTRL_PT_UART_IN4_OFFSET  (0x24)
#define IO_CTRL_PT_UART_OUT1_OFFSET  (0x28)
#define IO_CTRL_PT_UART_OUT2_OFFSET  (0x2c)
#define IO_CTRL_PT_UART_OUT3_OFFSET  (0x30)
#define IO_CTRL_PT_UART_OUT4_OFFSET  (0x34)


typedef union _IO_CTRL_PT_DE_U_ {
    unsigned int w32; 
    struct { 
	unsigned muxout_sel :3;
	unsigned ds :1;
	unsigned ie :1;
	unsigned oen :1;
	unsigned pd :1;
	unsigned pu :1;
	unsigned dout :1;
	unsigned din :1;
	unsigned int_mode :3;
	unsigned rsv0 :19;
    } b;
} IO_CTRL_PT_DE_UT;

typedef union _IO_CTRL_PT_H_Fn_U_ {
    unsigned int w32; 
    struct { 
	unsigned muxout_sel :3;
	unsigned ds :1;
	unsigned ie :1;
	unsigned oen :1;
	unsigned pd :1;
	unsigned pu :1;
	unsigned dout :1;
	unsigned din :1;
	unsigned int_mode :3;
	unsigned rsv0 :19;
    } b;
} IO_CTRL_PT_H_Fn_UT;

typedef union _IO_CTRL_PT_SPI_CS_0_U_ {
    unsigned int w32; 
    struct { 
	unsigned muxout_sel :3;
	unsigned ds :1;
	unsigned ie :1;
	unsigned oen :1;
	unsigned pd :1;
	unsigned pu :1;
	unsigned dout :1;
	unsigned din :1;
	unsigned int_mode :3;
	unsigned rsv0 :19;
    } b;
} IO_CTRL_PT_SPI_CS_0_UT;

typedef union _IO_CTRL_PT_SPI_DIN_U_ {
    unsigned int w32; 
    struct { 
	unsigned muxout_sel :3;
	unsigned ds :1;
	unsigned ie :1;
	unsigned oen :1;
	unsigned pd :1;
	unsigned pu :1;
	unsigned dout :1;
	unsigned din :1;
	unsigned int_mode :3;
	unsigned rsv0 :19;
    } b;
} IO_CTRL_PT_SPI_DIN_UT;

typedef union _IO_CTRL_PT_SPI_DOUT_U_ {
    unsigned int w32; 
    struct { 
	unsigned muxout_sel :3;
	unsigned ds :1;
	unsigned ie :1;
	unsigned oen :1;
	unsigned pd :1;
	unsigned pu :1;
	unsigned dout :1;
	unsigned din :1;
	unsigned int_mode :3;
	unsigned rsv0 :19;
    } b;
} IO_CTRL_PT_SPI_DOUT_UT;

typedef union _IO_CTRL_PT_SPI_SCK_U_ {
    unsigned int w32; 
    struct { 
	unsigned muxout_sel :3;
	unsigned ds :1;
	unsigned ie :1;
	unsigned oen :1;
	unsigned pd :1;
	unsigned pu :1;
	unsigned dout :1;
	unsigned din :1;
	unsigned int_mode :3;
	unsigned rsv0 :19;
    } b;
} IO_CTRL_PT_SPI_SCK_UT;

typedef union _IO_CTRL_PT_UART_IN1_U_ {
    unsigned int w32; 
    struct { 
	unsigned muxout_sel :3;
	unsigned ds :1;
	unsigned ie :1;
	unsigned oen :1;
	unsigned pd :1;
	unsigned pu :1;
	unsigned dout :1;
	unsigned din :1;
	unsigned int_mode :3;
	unsigned rsv0 :19;
    } b;
} IO_CTRL_PT_UART_IN1_UT;

typedef union _IO_CTRL_PT_UART_IN2_U_ {
    unsigned int w32; 
    struct { 
	unsigned muxout_sel :3;
	unsigned ds :1;
	unsigned ie :1;
	unsigned oen :1;
	unsigned pd :1;
	unsigned pu :1;
	unsigned dout :1;
	unsigned din :1;
	unsigned int_mode :3;
	unsigned rsv0 :19;
    } b;
} IO_CTRL_PT_UART_IN2_UT;

typedef union _IO_CTRL_PT_UART_IN3_U_ {
    unsigned int w32; 
    struct { 
	unsigned muxout_sel :3;
	unsigned ds :1;
	unsigned ie :1;
	unsigned oen :1;
	unsigned pd :1;
	unsigned pu :1;
	unsigned dout :1;
	unsigned din :1;
	unsigned int_mode :3;
	unsigned rsv0 :19;
    } b;
} IO_CTRL_PT_UART_IN3_UT;

typedef union _IO_CTRL_PT_UART_IN4_U_ {
    unsigned int w32; 
    struct { 
	unsigned muxout_sel :3;
	unsigned ds :1;
	unsigned ie :1;
	unsigned oen :1;
	unsigned pd :1;
	unsigned pu :1;
	unsigned dout :1;
	unsigned din :1;
	unsigned int_mode :3;
	unsigned rsv0 :19;
    } b;
} IO_CTRL_PT_UART_IN4_UT;

typedef union _IO_CTRL_PT_UART_OUT1_U_ {
    unsigned int w32; 
    struct { 
	unsigned muxout_sel :3;
	unsigned ds :1;
	unsigned ie :1;
	unsigned oen :1;
	unsigned pd :1;
	unsigned pu :1;
	unsigned dout :1;
	unsigned din :1;
	unsigned int_mode :3;
	unsigned rsv0 :19;
    } b;
} IO_CTRL_PT_UART_OUT1_UT;

typedef union _IO_CTRL_PT_UART_OUT2_U_ {
    unsigned int w32; 
    struct { 
	unsigned muxout_sel :3;
	unsigned ds :1;
	unsigned ie :1;
	unsigned oen :1;
	unsigned pd :1;
	unsigned pu :1;
	unsigned dout :1;
	unsigned din :1;
	unsigned int_mode :3;
	unsigned rsv0 :19;
    } b;
} IO_CTRL_PT_UART_OUT2_UT;

typedef union _IO_CTRL_PT_UART_OUT3_U_ {
    unsigned int w32; 
    struct { 
	unsigned muxout_sel :3;
	unsigned ds :1;
	unsigned ie :1;
	unsigned oen :1;
	unsigned pd :1;
	unsigned pu :1;
	unsigned dout :1;
	unsigned din :1;
	unsigned int_mode :3;
	unsigned rsv0 :19;
    } b;
} IO_CTRL_PT_UART_OUT3_UT;

typedef union _IO_CTRL_PT_UART_OUT4_U_ {
    unsigned int w32; 
    struct { 
	unsigned muxout_sel :3;
	unsigned ds :1;
	unsigned ie :1;
	unsigned oen :1;
	unsigned pd :1;
	unsigned pu :1;
	unsigned dout :1;
	unsigned din :1;
	unsigned int_mode :3;
	unsigned rsv0 :19;
    } b;
} IO_CTRL_PT_UART_OUT4_UT;

//module typedef
typedef volatile struct _IO_CTRL_PT_MODULE_S
{
	IO_CTRL_PT_DE_UT DE;
	IO_CTRL_PT_H_Fn_UT H_Fn;
	IO_CTRL_PT_SPI_CS_0_UT SPI_CS_0;
	IO_CTRL_PT_SPI_DIN_UT SPI_DIN;
	IO_CTRL_PT_SPI_DOUT_UT SPI_DOUT;
	IO_CTRL_PT_SPI_SCK_UT SPI_SCK;
	IO_CTRL_PT_UART_IN1_UT UART_IN1;
	IO_CTRL_PT_UART_IN2_UT UART_IN2;
	IO_CTRL_PT_UART_IN3_UT UART_IN3;
	IO_CTRL_PT_UART_IN4_UT UART_IN4;
	IO_CTRL_PT_UART_OUT1_UT UART_OUT1;
	IO_CTRL_PT_UART_OUT2_UT UART_OUT2;
	IO_CTRL_PT_UART_OUT3_UT UART_OUT3;
	IO_CTRL_PT_UART_OUT4_UT UART_OUT4;
} IO_CTRL_PT_MODULE_ST;



//define parameterized depths


//define enumerated types


//define offsets


//module typedef
typedef volatile struct _ROM_PT_MODULE_S
{
} ROM_PT_MODULE_ST;

#define MEMORY_Code_ROM_ADDR 0x0
#define MEMORY_Code_ROM_SIZE 131072


//define parameterized depths


//define enumerated types


//define offsets
#define MASTER_SPI_PT_CTRLR0_OFFSET  (0x0)
#define MASTER_SPI_PT_CTRLR1_OFFSET  (0x4)
#define MASTER_SPI_PT_SSIENR_OFFSET  (0x8)
#define MASTER_SPI_PT_MWCR_OFFSET  (0xC)
#define MASTER_SPI_PT_SER_OFFSET  (0x10)
#define MASTER_SPI_PT_BAUDR_OFFSET  (0x14)
#define MASTER_SPI_PT_TXFTLR_OFFSET  (0x18)
#define MASTER_SPI_PT_RXFTLR_OFFSET  (0x1C)
#define MASTER_SPI_PT_TXFLR_OFFSET  (0x20)
#define MASTER_SPI_PT_RXFLR_OFFSET  (0x24)
#define MASTER_SPI_PT_SR_OFFSET  (0x28)
#define MASTER_SPI_PT_IMR_OFFSET  (0x2C)
#define MASTER_SPI_PT_ISR_OFFSET  (0x30)
#define MASTER_SPI_PT_RISR_OFFSET  (0x34)
#define MASTER_SPI_PT_TXOICR_OFFSET  (0x38)
#define MASTER_SPI_PT_RXOICR_OFFSET  (0x3C)
#define MASTER_SPI_PT_RXUICR_OFFSET  (0x40)
#define MASTER_SPI_PT_MSTICR_OFFSET  (0x44)
#define MASTER_SPI_PT_ICR_OFFSET  (0x48)
#define MASTER_SPI_PT_DMACR_OFFSET  (0x4C)
#define MASTER_SPI_PT_DMATDLR_OFFSET  (0x50)
#define MASTER_SPI_PT_DMARDLR_OFFSET  (0x54)
#define MASTER_SPI_PT_IDR_OFFSET  (0x58)
#define MASTER_SPI_PT_SSI_VERSION_ID_OFFSET  (0x5C)
#define MASTER_SPI_PT_DR0_OFFSET  (0x60)
#define MASTER_SPI_PT_DR1_OFFSET  (0x64)
#define MASTER_SPI_PT_DR2_OFFSET  (0x68)
#define MASTER_SPI_PT_DR3_OFFSET  (0x6C)
#define MASTER_SPI_PT_DR4_OFFSET  (0x70)
#define MASTER_SPI_PT_DR5_OFFSET  (0x74)
#define MASTER_SPI_PT_DR6_OFFSET  (0x78)
#define MASTER_SPI_PT_DR7_OFFSET  (0x7C)
#define MASTER_SPI_PT_DR8_OFFSET  (0x80)
#define MASTER_SPI_PT_DR9_OFFSET  (0x84)
#define MASTER_SPI_PT_DR10_OFFSET  (0x88)
#define MASTER_SPI_PT_DR11_OFFSET  (0x8C)
#define MASTER_SPI_PT_DR12_OFFSET  (0x90)
#define MASTER_SPI_PT_DR13_OFFSET  (0x94)
#define MASTER_SPI_PT_DR14_OFFSET  (0x98)
#define MASTER_SPI_PT_DR15_OFFSET  (0x9C)
#define MASTER_SPI_PT_DR16_OFFSET  (0xA0)
#define MASTER_SPI_PT_DR17_OFFSET  (0xA4)
#define MASTER_SPI_PT_DR18_OFFSET  (0xA8)
#define MASTER_SPI_PT_DR19_OFFSET  (0xAC)
#define MASTER_SPI_PT_DR20_OFFSET  (0xB0)
#define MASTER_SPI_PT_DR21_OFFSET  (0xB4)
#define MASTER_SPI_PT_DR22_OFFSET  (0xB8)
#define MASTER_SPI_PT_DR23_OFFSET  (0xBC)
#define MASTER_SPI_PT_DR24_OFFSET  (0xC0)
#define MASTER_SPI_PT_DR25_OFFSET  (0xC4)
#define MASTER_SPI_PT_DR26_OFFSET  (0xC8)
#define MASTER_SPI_PT_DR27_OFFSET  (0xCC)
#define MASTER_SPI_PT_DR28_OFFSET  (0xD0)
#define MASTER_SPI_PT_DR29_OFFSET  (0xD4)
#define MASTER_SPI_PT_DR30_OFFSET  (0xD8)
#define MASTER_SPI_PT_DR31_OFFSET  (0xDC)
#define MASTER_SPI_PT_DR32_OFFSET  (0xE0)
#define MASTER_SPI_PT_DR33_OFFSET  (0xE4)
#define MASTER_SPI_PT_DR34_OFFSET  (0xE8)
#define MASTER_SPI_PT_DR35_OFFSET  (0xEC)
#define MASTER_SPI_PT_RSVD_OFFSET  (0xFC)


typedef union _MASTER_SPI_PT_CTRLR0_U_ {
    uint32_t value; 
    struct { 
	unsigned dfs :4;
	unsigned frf :2;
	unsigned scph :1;
	unsigned scpol :1;
	unsigned tmod :2;
	unsigned rsvd_slv_oe :1;
	unsigned srl :1;
	unsigned cfs :4;
	unsigned dfs_32 :5;
	unsigned spi_frf :2;
	unsigned rsvd_ctrlr0_23 :1;
	unsigned sste :1;
	unsigned rsvd_ctrlr0 :7;
    } fields;
} MasterSpiCtrl0;

typedef union _MASTER_SPI_PT_CTRLR1_U_ {
    uint32_t value; 
    struct { 
	unsigned ndf :16;
	unsigned rsvd_ctrlr1 :16;
    } fields;
} MasterSpiCtrl1;

typedef union _MASTER_SPI_PT_SSIENR_U_ {
    uint32_t value; 
    struct { 
	unsigned ssi_en :1;
	unsigned rsvd_ssienr :31;
    } fields;
} MasterSpiSsiEnR;

typedef union _MASTER_SPI_PT_MWCR_U_ {
    uint32_t value; 
    struct { 
	unsigned mwmod :1;
	unsigned mdd :1;
	unsigned mhs :1;
	unsigned rsvd_mwcr :29;
    } fields;
} MasterSpiMWCR;

typedef union _MASTER_SPI_PT_SER_U_ {
    uint32_t value; 
    struct { 
	unsigned ser :1;
	unsigned rsvd_ser :31;
    } fields;
} MasterSpiSER;

typedef union _MASTER_SPI_PT_BAUDR_U_ {
    uint32_t value; 
    struct { 
	unsigned sckdv :16;
	unsigned rsvd_baudr :16;
    } fields;
} MasterSpiBaudR;

typedef union _MASTER_SPI_PT_TXFTLR_U_ {
    uint32_t value; 
    struct { 
	unsigned tft :3;
	unsigned rsvd_txftlr :29;
    } fields;
} MasterSpiTxFTLR;

typedef union _MASTER_SPI_PT_RXFTLR_U_ {
    uint32_t value; 
    struct { 
	unsigned rft :3;
	unsigned rsvd_rxftlr :29;
    } fields;
} MasterSpiRxFTLR;

typedef union _MASTER_SPI_PT_TXFLR_U_ {
    uint32_t value; 
    struct { 
	unsigned txtfl :4;
	unsigned rsvd_txflr :28;
    } fields;
} MasterSpiTxFLR;

typedef union _MASTER_SPI_PT_RXFLR_U_ {
    uint32_t value; 
    struct { 
	unsigned rxtfl :4;
	unsigned rsvd_rxflr :28;
    } fields;
} MasterSpiRxFLR;

typedef union _MASTER_SPI_PT_SR_U_ {
    uint32_t value;
    struct { 
	unsigned busy :1;
	unsigned tfnf :1;
	unsigned tfe :1;
	unsigned rfne :1;
	unsigned rff :1;
	unsigned rsvd_txe :1;
	unsigned dcol :1;
	unsigned rsvd_sr :25;
    }fields;
} MasterSpiSR;

typedef union _MASTER_SPI_PT_IMR_U_ {
    uint32_t value;
    struct { 
	unsigned txeim :1;
	unsigned txoim :1;
	unsigned rxuim :1;
	unsigned rxoim :1;
	unsigned rxfim :1;
	unsigned mstim :1;
	unsigned rsvd_imr :26;
    } fields;
} MasterSpiIMR;

typedef union _MASTER_SPI_PT_ISR_U_ {
    uint32_t value; 
    struct { 
	unsigned txeis :1;
	unsigned txois :1;
	unsigned rxuis :1;
	unsigned rxois :1;
	unsigned rxfis :1;
	unsigned mstis :1;
	unsigned rsvd_isr :26;
    } fields;
} MasterSpiISR;

typedef union _MASTER_SPI_PT_RISR_U_ {
    uint32_t value; 
    struct { 
	unsigned txeir :1;
	unsigned txoir :1;
	unsigned rxuir :1;
	unsigned rxoir :1;
	unsigned rxfir :1;
	unsigned mstir :1;
	unsigned rsvd_risr :26;
    } fields;
} MASTER_SPI_PT_RISR_UT;

typedef union _MASTER_SPI_PT_TXOICR_U_ {
    uint32_t value; 
    struct { 
	unsigned txoicr :1;
	unsigned rsvd_txoicr :31;
    } fields;
} MASTER_SPI_PT_TXOICR_UT;

typedef union _MASTER_SPI_PT_RXOICR_U_ {
    uint32_t value; 
    struct { 
	unsigned rxoicr :1;
	unsigned rsvd_rxoicr :31;
    } fields;
} MASTER_SPI_PT_RXOICR_UT;

typedef union _MASTER_SPI_PT_RXUICR_U_ {
    uint32_t value; 
    struct { 
	unsigned rxuicr :1;
	unsigned rsvd_rxuicr :31;
    } fields;
} MASTER_SPI_PT_RXUICR_UT;

typedef union _MASTER_SPI_PT_MSTICR_U_ {
    uint32_t value; 
    struct { 
	unsigned msticr :1;
	unsigned rsvd_msticr :31;
    } fields;
} MASTER_SPI_PT_MSTICR_UT;

typedef union _MASTER_SPI_PT_ICR_U_ {
    uint32_t value; 
    struct { 
	unsigned icr :1;
	unsigned rsvd_icr :31;
    } fields;
} MASTER_SPI_PT_ICR_UT;

typedef union _MASTER_SPI_PT_DMACR_U_ {
    uint32_t value; 
    struct { 
	unsigned rdmae :1;
	unsigned tdmae :1;
	unsigned rsvd_dmacr :30;
    } fields;
} MASTER_SPI_PT_DMACR_UT;

typedef union _MASTER_SPI_PT_DMATDLR_U_ {
    uint32_t value; 
    struct { 
	unsigned dmatdl :3;
	unsigned rsvd_dmatdlr :29;
    } fields;
} MASTER_SPI_PT_DMATDLR_UT;

typedef union _MASTER_SPI_PT_DMARDLR_U_ {
    uint32_t value; 
    struct { 
	unsigned dmardl :3;
	unsigned rsvd_dmardlr :29;
    } fields;
} MASTER_SPI_PT_DMARDLR_UT;

typedef union _MASTER_SPI_PT_IDR_U_ {
    uint32_t value; 
    struct { 
	unsigned idcode :32;
    } fields;
} MASTER_SPI_PT_IDR_UT;

typedef union _MASTER_SPI_PT_SSI_VERSION_ID_U_ {
    uint32_t value; 
    struct { 
	unsigned ssi_comp_version :32;
    } fields;
} MASTER_SPI_PT_SSI_VERSION_ID_UT;

typedef union _MASTER_SPI_PT_DR0_U_ {
    uint32_t value; 
    struct { 
	unsigned dr :16;
	unsigned rsvd_dr :16;
    } fields;
} MasterSpiDR0;

typedef union _MASTER_SPI_PT_DR1_U_ {
    uint32_t value; 
    struct { 
	unsigned dr :16;
	unsigned rsvd_dr :16;
    } fields;
} MASTER_SPI_PT_DR1_UT;

typedef union _MASTER_SPI_PT_DR2_U_ {
    uint32_t value; 
    struct { 
	unsigned dr :16;
	unsigned rsvd_dr :16;
    } fields;
} MASTER_SPI_PT_DR2_UT;

typedef union _MASTER_SPI_PT_DR3_U_ {
    uint32_t value; 
    struct { 
	unsigned dr :16;
	unsigned rsvd_dr :16;
    } fields;
} MASTER_SPI_PT_DR3_UT;

typedef union _MASTER_SPI_PT_DR4_U_ {
    uint32_t value; 
    struct { 
	unsigned dr :16;
	unsigned rsvd_dr :16;
    } fields;
} MASTER_SPI_PT_DR4_UT;

typedef union _MASTER_SPI_PT_DR5_U_ {
    uint32_t value; 
    struct { 
	unsigned dr :16;
	unsigned rsvd_dr :16;
    } fields;
} MASTER_SPI_PT_DR5_UT;

typedef union _MASTER_SPI_PT_DR6_U_ {
    uint32_t value; 
    struct { 
	unsigned dr :16;
	unsigned rsvd_dr :16;
    } fields;
} MASTER_SPI_PT_DR6_UT;

typedef union _MASTER_SPI_PT_DR7_U_ {
    uint32_t value; 
    struct { 
	unsigned dr :16;
	unsigned rsvd_dr :16;
    } fields;
} MASTER_SPI_PT_DR7_UT;

typedef union _MASTER_SPI_PT_DR8_U_ {
    uint32_t value; 
    struct { 
	unsigned dr :16;
	unsigned rsvd_dr :16;
    } fields;
} MASTER_SPI_PT_DR8_UT;

typedef union _MASTER_SPI_PT_DR9_U_ {
    uint32_t value; 
    struct { 
	unsigned dr :16;
	unsigned rsvd_dr :16;
    } fields;
} MASTER_SPI_PT_DR9_UT;

typedef union _MASTER_SPI_PT_DR10_U_ {
    uint32_t value; 
    struct { 
	unsigned dr :16;
	unsigned rsvd_dr :16;
    } fields;
} MASTER_SPI_PT_DR10_UT;

typedef union _MASTER_SPI_PT_DR11_U_ {
    uint32_t value; 
    struct { 
	unsigned dr :16;
	unsigned rsvd_dr :16;
    } fields;
} MASTER_SPI_PT_DR11_UT;

typedef union _MASTER_SPI_PT_DR12_U_ {
    uint32_t value; 
    struct { 
	unsigned dr :16;
	unsigned rsvd_dr :16;
    } fields;
} MASTER_SPI_PT_DR12_UT;

typedef union _MASTER_SPI_PT_DR13_U_ {
    uint32_t value; 
    struct { 
	unsigned dr :16;
	unsigned rsvd_dr :16;
    } fields;
} MASTER_SPI_PT_DR13_UT;

typedef union _MASTER_SPI_PT_DR14_U_ {
    uint32_t value; 
    struct { 
	unsigned dr :16;
	unsigned rsvd_dr :16;
    } fields;
} MASTER_SPI_PT_DR14_UT;

typedef union _MASTER_SPI_PT_DR15_U_ {
    uint32_t value; 
    struct { 
	unsigned dr :16;
	unsigned rsvd_dr :16;
    } fields;
} MASTER_SPI_PT_DR15_UT;

typedef union _MASTER_SPI_PT_DR16_U_ {
    uint32_t value; 
    struct { 
	unsigned dr :16;
	unsigned rsvd_dr :16;
    } fields;
} MASTER_SPI_PT_DR16_UT;

typedef union _MASTER_SPI_PT_DR17_U_ {
    uint32_t value; 
    struct { 
	unsigned dr :16;
	unsigned rsvd_dr :16;
    } fields;
} MASTER_SPI_PT_DR17_UT;

typedef union _MASTER_SPI_PT_DR18_U_ {
    uint32_t value; 
    struct { 
	unsigned dr :16;
	unsigned rsvd_dr :16;
    } fields;
} MASTER_SPI_PT_DR18_UT;

typedef union _MASTER_SPI_PT_DR19_U_ {
    uint32_t value; 
    struct { 
	unsigned dr :16;
	unsigned rsvd_dr :16;
    } fields;
} MASTER_SPI_PT_DR19_UT;

typedef union _MASTER_SPI_PT_DR20_U_ {
    uint32_t value; 
    struct { 
	unsigned dr :16;
	unsigned rsvd_dr :16;
    } fields;
} MASTER_SPI_PT_DR20_UT;

typedef union _MASTER_SPI_PT_DR21_U_ {
    uint32_t value; 
    struct { 
	unsigned dr :16;
	unsigned rsvd_dr :16;
    } fields;
} MASTER_SPI_PT_DR21_UT;

typedef union _MASTER_SPI_PT_DR22_U_ {
    uint32_t value; 
    struct { 
	unsigned dr :16;
	unsigned rsvd_dr :16;
    } fields;
} MASTER_SPI_PT_DR22_UT;

typedef union _MASTER_SPI_PT_DR23_U_ {
    uint32_t value; 
    struct { 
	unsigned dr :16;
	unsigned rsvd_dr :16;
    } fields;
} MASTER_SPI_PT_DR23_UT;

typedef union _MASTER_SPI_PT_DR24_U_ {
    uint32_t value; 
    struct { 
	unsigned dr :16;
	unsigned rsvd_dr :16;
    } fields;
} MASTER_SPI_PT_DR24_UT;

typedef union _MASTER_SPI_PT_DR25_U_ {
    uint32_t value; 
    struct { 
	unsigned dr :16;
	unsigned rsvd_dr :16;
    } fields;
} MASTER_SPI_PT_DR25_UT;

typedef union _MASTER_SPI_PT_DR26_U_ {
    uint32_t value; 
    struct { 
	unsigned dr :16;
	unsigned rsvd_dr :16;
    } fields;
} MASTER_SPI_PT_DR26_UT;

typedef union _MASTER_SPI_PT_DR27_U_ {
    uint32_t value; 
    struct { 
	unsigned dr :16;
	unsigned rsvd_dr :16;
    } fields;
} MASTER_SPI_PT_DR27_UT;

typedef union _MASTER_SPI_PT_DR28_U_ {
    uint32_t value; 
    struct { 
	unsigned dr :16;
	unsigned rsvd_dr :16;
    } fields;
} MASTER_SPI_PT_DR28_UT;

typedef union _MASTER_SPI_PT_DR29_U_ {
    uint32_t value; 
    struct { 
	unsigned dr :16;
	unsigned rsvd_dr :16;
    } fields;
} MASTER_SPI_PT_DR29_UT;

typedef union _MASTER_SPI_PT_DR30_U_ {
    uint32_t value; 
    struct { 
	unsigned dr :16;
	unsigned rsvd_dr :16;
    } fields;
} MASTER_SPI_PT_DR30_UT;

typedef union _MASTER_SPI_PT_DR31_U_ {
    uint32_t value; 
    struct { 
	unsigned dr :16;
	unsigned rsvd_dr :16;
    } fields;
} MASTER_SPI_PT_DR31_UT;

typedef union _MASTER_SPI_PT_DR32_U_ {
    uint32_t value; 
    struct { 
	unsigned dr :16;
	unsigned rsvd_dr :16;
    } fields;
} MASTER_SPI_PT_DR32_UT;

typedef union _MASTER_SPI_PT_DR33_U_ {
    uint32_t value; 
    struct { 
	unsigned dr :16;
	unsigned rsvd_dr :16;
    } fields;
} MASTER_SPI_PT_DR33_UT;

typedef union _MASTER_SPI_PT_DR34_U_ {
    uint32_t value; 
    struct { 
	unsigned dr :16;
	unsigned rsvd_dr :16;
    } fields;
} MASTER_SPI_PT_DR34_UT;

typedef union _MASTER_SPI_PT_DR35_U_ {
    uint32_t value; 
    struct { 
	unsigned dr :16;
	unsigned rsvd_dr :16;
    } fields;
} MASTER_SPI_PT_DR35_UT;

typedef union _MASTER_SPI_PT_RSVD_U_ {
    uint32_t value; 
    struct { 
	unsigned rsvd :32;
    } fields;
} MASTER_SPI_PT_RSVD_UT;

//module typedef
typedef volatile struct _MASTER_SPI_PT_MODULE_S
{
	MasterSpiCtrl0 CTRLR0;
	MasterSpiCtrl1 CTRLR1;
	MasterSpiSsiEnR SSIENR;
	MasterSpiMWCR MWCR;
	MasterSpiSER SER;
	MasterSpiBaudR BAUDR;
	MasterSpiTxFTLR TXFTLR;
	MasterSpiRxFTLR RXFTLR;
	MasterSpiTxFLR TXFLR;
	MasterSpiRxFLR RXFLR;
	MasterSpiSR SR;
	MasterSpiIMR IMR;
	MasterSpiISR ISR;
	MASTER_SPI_PT_RISR_UT RISR;
	MASTER_SPI_PT_TXOICR_UT TXOICR;
	MASTER_SPI_PT_RXOICR_UT RXOICR;
	MASTER_SPI_PT_RXUICR_UT RXUICR;
	MASTER_SPI_PT_MSTICR_UT MSTICR;
	MASTER_SPI_PT_ICR_UT ICR;
	MASTER_SPI_PT_DMACR_UT DMACR;
	MASTER_SPI_PT_DMATDLR_UT DMATDLR;
	MASTER_SPI_PT_DMARDLR_UT DMARDLR;
	MASTER_SPI_PT_IDR_UT IDR;
	MASTER_SPI_PT_SSI_VERSION_ID_UT SSI_VERSION_ID;
	MasterSpiDR0 DR0;
	MASTER_SPI_PT_DR1_UT DR1;
	MASTER_SPI_PT_DR2_UT DR2;
	MASTER_SPI_PT_DR3_UT DR3;
	MASTER_SPI_PT_DR4_UT DR4;
	MASTER_SPI_PT_DR5_UT DR5;
	MASTER_SPI_PT_DR6_UT DR6;
	MASTER_SPI_PT_DR7_UT DR7;
	MASTER_SPI_PT_DR8_UT DR8;
	MASTER_SPI_PT_DR9_UT DR9;
	MASTER_SPI_PT_DR10_UT DR10;
	MASTER_SPI_PT_DR11_UT DR11;
	MASTER_SPI_PT_DR12_UT DR12;
	MASTER_SPI_PT_DR13_UT DR13;
	MASTER_SPI_PT_DR14_UT DR14;
	MASTER_SPI_PT_DR15_UT DR15;
	MASTER_SPI_PT_DR16_UT DR16;
	MASTER_SPI_PT_DR17_UT DR17;
	MASTER_SPI_PT_DR18_UT DR18;
	MASTER_SPI_PT_DR19_UT DR19;
	MASTER_SPI_PT_DR20_UT DR20;
	MASTER_SPI_PT_DR21_UT DR21;
	MASTER_SPI_PT_DR22_UT DR22;
	MASTER_SPI_PT_DR23_UT DR23;
	MASTER_SPI_PT_DR24_UT DR24;
	MASTER_SPI_PT_DR25_UT DR25;
	MASTER_SPI_PT_DR26_UT DR26;
	MASTER_SPI_PT_DR27_UT DR27;
	MASTER_SPI_PT_DR28_UT DR28;
	MASTER_SPI_PT_DR29_UT DR29;
	MASTER_SPI_PT_DR30_UT DR30;
	MASTER_SPI_PT_DR31_UT DR31;
	MASTER_SPI_PT_DR32_UT DR32;
	MASTER_SPI_PT_DR33_UT DR33;
	MASTER_SPI_PT_DR34_UT DR34;
	MASTER_SPI_PT_DR35_UT DR35;
	unsigned rsv_EC_FC [3];
	MASTER_SPI_PT_RSVD_UT RSVD;
} MASTER_SPI_PT_MODULE_ST;



//define parameterized depths


//define enumerated types


//define offsets
#define SLAVE_SPI_PT_CTRLR0_OFFSET  (0x0)
#define SLAVE_SPI_PT_SSIENR_OFFSET  (0x8)
#define SLAVE_SPI_PT_MWCR_OFFSET  (0xC)
#define SLAVE_SPI_PT_TXFTLR_OFFSET  (0x18)
#define SLAVE_SPI_PT_RXFTLR_OFFSET  (0x1C)
#define SLAVE_SPI_PT_TXFLR_OFFSET  (0x20)
#define SLAVE_SPI_PT_RXFLR_OFFSET  (0x24)
#define SLAVE_SPI_PT_SR_OFFSET  (0x28)
#define SLAVE_SPI_PT_IMR_OFFSET  (0x2C)
#define SLAVE_SPI_PT_ISR_OFFSET  (0x30)
#define SLAVE_SPI_PT_RISR_OFFSET  (0x34)
#define SLAVE_SPI_PT_TXOICR_OFFSET  (0x38)
#define SLAVE_SPI_PT_RXOICR_OFFSET  (0x3C)
#define SLAVE_SPI_PT_RXUICR_OFFSET  (0x40)
#define SLAVE_SPI_PT_MSTICR_OFFSET  (0x44)
#define SLAVE_SPI_PT_ICR_OFFSET  (0x48)
#define SLAVE_SPI_PT_DMACR_OFFSET  (0x4C)
#define SLAVE_SPI_PT_DMATDLR_OFFSET  (0x50)
#define SLAVE_SPI_PT_DMARDLR_OFFSET  (0x54)
#define SLAVE_SPI_PT_IDR_OFFSET  (0x58)
#define SLAVE_SPI_PT_SSI_VERSION_ID_OFFSET  (0x5C)
#define SLAVE_SPI_PT_DR0_OFFSET  (0x60)
#define SLAVE_SPI_PT_DR1_OFFSET  (0x64)
#define SLAVE_SPI_PT_DR2_OFFSET  (0x68)
#define SLAVE_SPI_PT_DR3_OFFSET  (0x6C)
#define SLAVE_SPI_PT_DR4_OFFSET  (0x70)
#define SLAVE_SPI_PT_DR5_OFFSET  (0x74)
#define SLAVE_SPI_PT_DR6_OFFSET  (0x78)
#define SLAVE_SPI_PT_DR7_OFFSET  (0x7C)
#define SLAVE_SPI_PT_DR8_OFFSET  (0x80)
#define SLAVE_SPI_PT_DR9_OFFSET  (0x84)
#define SLAVE_SPI_PT_DR10_OFFSET  (0x88)
#define SLAVE_SPI_PT_DR11_OFFSET  (0x8C)
#define SLAVE_SPI_PT_DR12_OFFSET  (0x90)
#define SLAVE_SPI_PT_DR13_OFFSET  (0x94)
#define SLAVE_SPI_PT_DR14_OFFSET  (0x98)
#define SLAVE_SPI_PT_DR15_OFFSET  (0x9C)
#define SLAVE_SPI_PT_DR16_OFFSET  (0xA0)
#define SLAVE_SPI_PT_DR17_OFFSET  (0xA4)
#define SLAVE_SPI_PT_DR18_OFFSET  (0xA8)
#define SLAVE_SPI_PT_DR19_OFFSET  (0xAC)
#define SLAVE_SPI_PT_DR20_OFFSET  (0xB0)
#define SLAVE_SPI_PT_DR21_OFFSET  (0xB4)
#define SLAVE_SPI_PT_DR22_OFFSET  (0xB8)
#define SLAVE_SPI_PT_DR23_OFFSET  (0xBC)
#define SLAVE_SPI_PT_DR24_OFFSET  (0xC0)
#define SLAVE_SPI_PT_DR25_OFFSET  (0xC4)
#define SLAVE_SPI_PT_DR26_OFFSET  (0xC8)
#define SLAVE_SPI_PT_DR27_OFFSET  (0xCC)
#define SLAVE_SPI_PT_DR28_OFFSET  (0xD0)
#define SLAVE_SPI_PT_DR29_OFFSET  (0xD4)
#define SLAVE_SPI_PT_DR30_OFFSET  (0xD8)
#define SLAVE_SPI_PT_DR31_OFFSET  (0xDC)
#define SLAVE_SPI_PT_DR32_OFFSET  (0xE0)
#define SLAVE_SPI_PT_DR33_OFFSET  (0xE4)
#define SLAVE_SPI_PT_DR34_OFFSET  (0xE8)
#define SLAVE_SPI_PT_DR35_OFFSET  (0xEC)
#define SLAVE_SPI_PT_RSVD_OFFSET  (0xFC)


typedef union _SLAVE_SPI_PT_CTRLR0_U_ {
    unsigned int w32; 
    struct { 
	unsigned dfs :4;
	unsigned frf :2;
	unsigned scph :1;
	unsigned scpol :1;
	unsigned tmod :2;
	unsigned slv_oe :1;
	unsigned srl :1;
	unsigned cfs :4;
	unsigned dfs_32 :5;
	unsigned spi_frf :2;
	unsigned rsvd_ctrlr0_23 :1;
	unsigned sste :1;
	unsigned rsvd_ctrlr0 :7;
    } b;
} SLAVE_SPI_PT_CTRLR0_UT;

typedef union _SLAVE_SPI_PT_SSIENR_U_ {
    unsigned int w32; 
    struct { 
	unsigned ssi_en :1;
	unsigned rsvd_ssienr :31;
    } b;
} SLAVE_SPI_PT_SSIENR_UT;

typedef union _SLAVE_SPI_PT_MWCR_U_ {
    unsigned int w32; 
    struct { 
	unsigned mwmod :1;
	unsigned mdd :1;
	unsigned rsvd_mhs :1;
	unsigned rsvd_mwcr :29;
    } b;
} SLAVE_SPI_PT_MWCR_UT;

typedef union _SLAVE_SPI_PT_TXFTLR_U_ {
    unsigned int w32; 
    struct { 
	unsigned tft :3;
	unsigned rsvd_txftlr :29;
    } b;
} SLAVE_SPI_PT_TXFTLR_UT;

typedef union _SLAVE_SPI_PT_RXFTLR_U_ {
    unsigned int w32; 
    struct { 
	unsigned rft :3;
	unsigned rsvd_rxftlr :29;
    } b;
} SLAVE_SPI_PT_RXFTLR_UT;

typedef union _SLAVE_SPI_PT_TXFLR_U_ {
    unsigned int w32; 
    struct { 
	unsigned txtfl :4;
	unsigned rsvd_txflr :28;
    } b;
} SLAVE_SPI_PT_TXFLR_UT;

typedef union _SLAVE_SPI_PT_RXFLR_U_ {
    unsigned int w32; 
    struct { 
	unsigned rxtfl :4;
	unsigned rsvd_rxflr :28;
    } b;
} SLAVE_SPI_PT_RXFLR_UT;

typedef union _SLAVE_SPI_PT_SR_U_ {
    unsigned int w32; 
    struct { 
	unsigned busy :1;
	unsigned tfnf :1;
	unsigned tfe :1;
	unsigned rfne :1;
	unsigned rff :1;
	unsigned txe :1;
	unsigned rsvd_dcol :1;
	unsigned rsvd_sr :25;
    } b;
} SLAVE_SPI_PT_SR_UT;

typedef union _SLAVE_SPI_PT_IMR_U_ {
    unsigned int w32; 
    struct { 
	unsigned txeim :1;
	unsigned txoim :1;
	unsigned rxuim :1;
	unsigned rxoim :1;
	unsigned rxfim :1;
	unsigned rsv0 :1;
	unsigned rsvd_imr :26;
    } b;
} SLAVE_SPI_PT_IMR_UT;

typedef union _SLAVE_SPI_PT_ISR_U_ {
    unsigned int w32; 
    struct { 
	unsigned txeis :1;
	unsigned txois :1;
	unsigned rxuis :1;
	unsigned rxois :1;
	unsigned rxfis :1;
	unsigned rsvd_mstis :1;
	unsigned rsvd_isr :26;
    } b;
} SLAVE_SPI_PT_ISR_UT;

typedef union _SLAVE_SPI_PT_RISR_U_ {
    unsigned int w32; 
    struct { 
	unsigned txeir :1;
	unsigned txoir :1;
	unsigned rxuir :1;
	unsigned rxoir :1;
	unsigned rxfir :1;
	unsigned rsvd_mstir :1;
	unsigned rsvd_risr :26;
    } b;
} SLAVE_SPI_PT_RISR_UT;

typedef union _SLAVE_SPI_PT_TXOICR_U_ {
    unsigned int w32; 
    struct { 
	unsigned txoicr :1;
	unsigned rsvd_txoicr :31;
    } b;
} SLAVE_SPI_PT_TXOICR_UT;

typedef union _SLAVE_SPI_PT_RXOICR_U_ {
    unsigned int w32; 
    struct { 
	unsigned rxoicr :1;
	unsigned rsvd_rxoicr :31;
    } b;
} SLAVE_SPI_PT_RXOICR_UT;

typedef union _SLAVE_SPI_PT_RXUICR_U_ {
    unsigned int w32; 
    struct { 
	unsigned rxuicr :1;
	unsigned rsvd_rxuicr :31;
    } b;
} SLAVE_SPI_PT_RXUICR_UT;

typedef union _SLAVE_SPI_PT_MSTICR_U_ {
    unsigned int w32; 
    struct { 
	unsigned msticr :1;
	unsigned rsvd_msticr :31;
    } b;
} SLAVE_SPI_PT_MSTICR_UT;

typedef union _SLAVE_SPI_PT_ICR_U_ {
    unsigned int w32; 
    struct { 
	unsigned icr :1;
	unsigned rsvd_icr :31;
    } b;
} SLAVE_SPI_PT_ICR_UT;

typedef union _SLAVE_SPI_PT_DMACR_U_ {
    unsigned int w32; 
    struct { 
	unsigned rdmae :1;
	unsigned tdmae :1;
	unsigned rsvd_dmacr :30;
    } b;
} SLAVE_SPI_PT_DMACR_UT;

typedef union _SLAVE_SPI_PT_DMATDLR_U_ {
    unsigned int w32; 
    struct { 
	unsigned dmatdl :3;
	unsigned rsvd_dmatdlr :29;
    } b;
} SLAVE_SPI_PT_DMATDLR_UT;

typedef union _SLAVE_SPI_PT_DMARDLR_U_ {
    unsigned int w32; 
    struct { 
	unsigned dmardl :3;
	unsigned rsvd_dmardlr :29;
    } b;
} SLAVE_SPI_PT_DMARDLR_UT;

typedef union _SLAVE_SPI_PT_IDR_U_ {
    unsigned int w32; 
    struct { 
	unsigned idcode :32;
    } b;
} SLAVE_SPI_PT_IDR_UT;

typedef union _SLAVE_SPI_PT_SSI_VERSION_ID_U_ {
    unsigned int w32; 
    struct { 
	unsigned ssi_comp_version :32;
    } b;
} SLAVE_SPI_PT_SSI_VERSION_ID_UT;

typedef union _SLAVE_SPI_PT_DR0_U_ {
    unsigned int w32; 
    struct { 
	unsigned dr :16;
	unsigned rsvd_dr :16;
    } b;
} SLAVE_SPI_PT_DR0_UT;

typedef union _SLAVE_SPI_PT_DR1_U_ {
    unsigned int w32; 
    struct { 
	unsigned dr :16;
	unsigned rsvd_dr :16;
    } b;
} SLAVE_SPI_PT_DR1_UT;

typedef union _SLAVE_SPI_PT_DR2_U_ {
    unsigned int w32; 
    struct { 
	unsigned dr :16;
	unsigned rsvd_dr :16;
    } b;
} SLAVE_SPI_PT_DR2_UT;

typedef union _SLAVE_SPI_PT_DR3_U_ {
    unsigned int w32; 
    struct { 
	unsigned dr :16;
	unsigned rsvd_dr :16;
    } b;
} SLAVE_SPI_PT_DR3_UT;

typedef union _SLAVE_SPI_PT_DR4_U_ {
    unsigned int w32; 
    struct { 
	unsigned dr :16;
	unsigned rsvd_dr :16;
    } b;
} SLAVE_SPI_PT_DR4_UT;

typedef union _SLAVE_SPI_PT_DR5_U_ {
    unsigned int w32; 
    struct { 
	unsigned dr :16;
	unsigned rsvd_dr :16;
    } b;
} SLAVE_SPI_PT_DR5_UT;

typedef union _SLAVE_SPI_PT_DR6_U_ {
    unsigned int w32; 
    struct { 
	unsigned dr :16;
	unsigned rsvd_dr :16;
    } b;
} SLAVE_SPI_PT_DR6_UT;

typedef union _SLAVE_SPI_PT_DR7_U_ {
    unsigned int w32; 
    struct { 
	unsigned dr :16;
	unsigned rsvd_dr :16;
    } b;
} SLAVE_SPI_PT_DR7_UT;

typedef union _SLAVE_SPI_PT_DR8_U_ {
    unsigned int w32; 
    struct { 
	unsigned dr :16;
	unsigned rsvd_dr :16;
    } b;
} SLAVE_SPI_PT_DR8_UT;

typedef union _SLAVE_SPI_PT_DR9_U_ {
    unsigned int w32; 
    struct { 
	unsigned dr :16;
	unsigned rsvd_dr :16;
    } b;
} SLAVE_SPI_PT_DR9_UT;

typedef union _SLAVE_SPI_PT_DR10_U_ {
    unsigned int w32; 
    struct { 
	unsigned dr :16;
	unsigned rsvd_dr :16;
    } b;
} SLAVE_SPI_PT_DR10_UT;

typedef union _SLAVE_SPI_PT_DR11_U_ {
    unsigned int w32; 
    struct { 
	unsigned dr :16;
	unsigned rsvd_dr :16;
    } b;
} SLAVE_SPI_PT_DR11_UT;

typedef union _SLAVE_SPI_PT_DR12_U_ {
    unsigned int w32; 
    struct { 
	unsigned dr :16;
	unsigned rsvd_dr :16;
    } b;
} SLAVE_SPI_PT_DR12_UT;

typedef union _SLAVE_SPI_PT_DR13_U_ {
    unsigned int w32; 
    struct { 
	unsigned dr :16;
	unsigned rsvd_dr :16;
    } b;
} SLAVE_SPI_PT_DR13_UT;

typedef union _SLAVE_SPI_PT_DR14_U_ {
    unsigned int w32; 
    struct { 
	unsigned dr :16;
	unsigned rsvd_dr :16;
    } b;
} SLAVE_SPI_PT_DR14_UT;

typedef union _SLAVE_SPI_PT_DR15_U_ {
    unsigned int w32; 
    struct { 
	unsigned dr :16;
	unsigned rsvd_dr :16;
    } b;
} SLAVE_SPI_PT_DR15_UT;

typedef union _SLAVE_SPI_PT_DR16_U_ {
    unsigned int w32; 
    struct { 
	unsigned dr :16;
	unsigned rsvd_dr :16;
    } b;
} SLAVE_SPI_PT_DR16_UT;

typedef union _SLAVE_SPI_PT_DR17_U_ {
    unsigned int w32; 
    struct { 
	unsigned dr :16;
	unsigned rsvd_dr :16;
    } b;
} SLAVE_SPI_PT_DR17_UT;

typedef union _SLAVE_SPI_PT_DR18_U_ {
    unsigned int w32; 
    struct { 
	unsigned dr :16;
	unsigned rsvd_dr :16;
    } b;
} SLAVE_SPI_PT_DR18_UT;

typedef union _SLAVE_SPI_PT_DR19_U_ {
    unsigned int w32; 
    struct { 
	unsigned dr :16;
	unsigned rsvd_dr :16;
    } b;
} SLAVE_SPI_PT_DR19_UT;

typedef union _SLAVE_SPI_PT_DR20_U_ {
    unsigned int w32; 
    struct { 
	unsigned dr :16;
	unsigned rsvd_dr :16;
    } b;
} SLAVE_SPI_PT_DR20_UT;

typedef union _SLAVE_SPI_PT_DR21_U_ {
    unsigned int w32; 
    struct { 
	unsigned dr :16;
	unsigned rsvd_dr :16;
    } b;
} SLAVE_SPI_PT_DR21_UT;

typedef union _SLAVE_SPI_PT_DR22_U_ {
    unsigned int w32; 
    struct { 
	unsigned dr :16;
	unsigned rsvd_dr :16;
    } b;
} SLAVE_SPI_PT_DR22_UT;

typedef union _SLAVE_SPI_PT_DR23_U_ {
    unsigned int w32; 
    struct { 
	unsigned dr :16;
	unsigned rsvd_dr :16;
    } b;
} SLAVE_SPI_PT_DR23_UT;

typedef union _SLAVE_SPI_PT_DR24_U_ {
    unsigned int w32; 
    struct { 
	unsigned dr :16;
	unsigned rsvd_dr :16;
    } b;
} SLAVE_SPI_PT_DR24_UT;

typedef union _SLAVE_SPI_PT_DR25_U_ {
    unsigned int w32; 
    struct { 
	unsigned dr :16;
	unsigned rsvd_dr :16;
    } b;
} SLAVE_SPI_PT_DR25_UT;

typedef union _SLAVE_SPI_PT_DR26_U_ {
    unsigned int w32; 
    struct { 
	unsigned dr :16;
	unsigned rsvd_dr :16;
    } b;
} SLAVE_SPI_PT_DR26_UT;

typedef union _SLAVE_SPI_PT_DR27_U_ {
    unsigned int w32; 
    struct { 
	unsigned dr :16;
	unsigned rsvd_dr :16;
    } b;
} SLAVE_SPI_PT_DR27_UT;

typedef union _SLAVE_SPI_PT_DR28_U_ {
    unsigned int w32; 
    struct { 
	unsigned dr :16;
	unsigned rsvd_dr :16;
    } b;
} SLAVE_SPI_PT_DR28_UT;

typedef union _SLAVE_SPI_PT_DR29_U_ {
    unsigned int w32; 
    struct { 
	unsigned dr :16;
	unsigned rsvd_dr :16;
    } b;
} SLAVE_SPI_PT_DR29_UT;

typedef union _SLAVE_SPI_PT_DR30_U_ {
    unsigned int w32; 
    struct { 
	unsigned dr :16;
	unsigned rsvd_dr :16;
    } b;
} SLAVE_SPI_PT_DR30_UT;

typedef union _SLAVE_SPI_PT_DR31_U_ {
    unsigned int w32; 
    struct { 
	unsigned dr :16;
	unsigned rsvd_dr :16;
    } b;
} SLAVE_SPI_PT_DR31_UT;

typedef union _SLAVE_SPI_PT_DR32_U_ {
    unsigned int w32; 
    struct { 
	unsigned dr :16;
	unsigned rsvd_dr :16;
    } b;
} SLAVE_SPI_PT_DR32_UT;

typedef union _SLAVE_SPI_PT_DR33_U_ {
    unsigned int w32; 
    struct { 
	unsigned dr :16;
	unsigned rsvd_dr :16;
    } b;
} SLAVE_SPI_PT_DR33_UT;

typedef union _SLAVE_SPI_PT_DR34_U_ {
    unsigned int w32; 
    struct { 
	unsigned dr :16;
	unsigned rsvd_dr :16;
    } b;
} SLAVE_SPI_PT_DR34_UT;

typedef union _SLAVE_SPI_PT_DR35_U_ {
    unsigned int w32; 
    struct { 
	unsigned dr :16;
	unsigned rsvd_dr :16;
    } b;
} SLAVE_SPI_PT_DR35_UT;

typedef union _SLAVE_SPI_PT_RSVD_U_ {
    unsigned int w32; 
    struct { 
	unsigned rsvd :32;
    } b;
} SLAVE_SPI_PT_RSVD_UT;

//module typedef
typedef volatile struct _SLAVE_SPI_PT_MODULE_S
{
	SLAVE_SPI_PT_CTRLR0_UT CTRLR0;
	unsigned rsv_0_8 [1];
	SLAVE_SPI_PT_SSIENR_UT SSIENR;
	SLAVE_SPI_PT_MWCR_UT MWCR;
	unsigned rsv_C_18 [2];
	SLAVE_SPI_PT_TXFTLR_UT TXFTLR;
	SLAVE_SPI_PT_RXFTLR_UT RXFTLR;
	SLAVE_SPI_PT_TXFLR_UT TXFLR;
	SLAVE_SPI_PT_RXFLR_UT RXFLR;
	SLAVE_SPI_PT_SR_UT SR;
	SLAVE_SPI_PT_IMR_UT IMR;
	SLAVE_SPI_PT_ISR_UT ISR;
	SLAVE_SPI_PT_RISR_UT RISR;
	SLAVE_SPI_PT_TXOICR_UT TXOICR;
	SLAVE_SPI_PT_RXOICR_UT RXOICR;
	SLAVE_SPI_PT_RXUICR_UT RXUICR;
	SLAVE_SPI_PT_MSTICR_UT MSTICR;
	SLAVE_SPI_PT_ICR_UT ICR;
	SLAVE_SPI_PT_DMACR_UT DMACR;
	SLAVE_SPI_PT_DMATDLR_UT DMATDLR;
	SLAVE_SPI_PT_DMARDLR_UT DMARDLR;
	SLAVE_SPI_PT_IDR_UT IDR;
	SLAVE_SPI_PT_SSI_VERSION_ID_UT SSI_VERSION_ID;
	SLAVE_SPI_PT_DR0_UT DR0;
	SLAVE_SPI_PT_DR1_UT DR1;
	SLAVE_SPI_PT_DR2_UT DR2;
	SLAVE_SPI_PT_DR3_UT DR3;
	SLAVE_SPI_PT_DR4_UT DR4;
	SLAVE_SPI_PT_DR5_UT DR5;
	SLAVE_SPI_PT_DR6_UT DR6;
	SLAVE_SPI_PT_DR7_UT DR7;
	SLAVE_SPI_PT_DR8_UT DR8;
	SLAVE_SPI_PT_DR9_UT DR9;
	SLAVE_SPI_PT_DR10_UT DR10;
	SLAVE_SPI_PT_DR11_UT DR11;
	SLAVE_SPI_PT_DR12_UT DR12;
	SLAVE_SPI_PT_DR13_UT DR13;
	SLAVE_SPI_PT_DR14_UT DR14;
	SLAVE_SPI_PT_DR15_UT DR15;
	SLAVE_SPI_PT_DR16_UT DR16;
	SLAVE_SPI_PT_DR17_UT DR17;
	SLAVE_SPI_PT_DR18_UT DR18;
	SLAVE_SPI_PT_DR19_UT DR19;
	SLAVE_SPI_PT_DR20_UT DR20;
	SLAVE_SPI_PT_DR21_UT DR21;
	SLAVE_SPI_PT_DR22_UT DR22;
	SLAVE_SPI_PT_DR23_UT DR23;
	SLAVE_SPI_PT_DR24_UT DR24;
	SLAVE_SPI_PT_DR25_UT DR25;
	SLAVE_SPI_PT_DR26_UT DR26;
	SLAVE_SPI_PT_DR27_UT DR27;
	SLAVE_SPI_PT_DR28_UT DR28;
	SLAVE_SPI_PT_DR29_UT DR29;
	SLAVE_SPI_PT_DR30_UT DR30;
	SLAVE_SPI_PT_DR31_UT DR31;
	SLAVE_SPI_PT_DR32_UT DR32;
	SLAVE_SPI_PT_DR33_UT DR33;
	SLAVE_SPI_PT_DR34_UT DR34;
	SLAVE_SPI_PT_DR35_UT DR35;
	unsigned rsv_EC_FC [3];
	SLAVE_SPI_PT_RSVD_UT RSVD;
} SLAVE_SPI_PT_MODULE_ST;



//define parameterized depths


//define enumerated types


//define offsets


//module typedef
typedef volatile struct _SRAM_PT_MODULE_S
{
} SRAM_PT_MODULE_ST;

#define MEMORY_Code_SRAM_ADDR 0x0
#define MEMORY_Code_SRAM_SIZE 131072


//define parameterized depths


//define enumerated types


//define offsets
#define SYS_CFG_PT_CLK_CTRL_OFFSET  (0x0)
#define SYS_CFG_PT_RESET_CTRL_OFFSET  (0x4)
#define SYS_CFG_PT_ADDR_SPACE_CTRL_OFFSET  (0x8)
#define SYS_CFG_PT_TEST_DIG_MUX_SEL_OFFSET  (0xc)
#define SYS_CFG_PT_CLK_1K_COUNT_CTRL_OFFSET  (0x10)
#define SYS_CFG_PT_CLK_1K_COUNT_RES_OFFSET  (0x14)
#define SYS_CFG_PT_UART_MUXING_OFFSET  (0x18)
#define SYS_CFG_PT_RS485_RX_DET_OFFSET  (0x1c)


typedef union _SYS_CFG_PT_CLK_CTRL_U_ {
    unsigned int w32; 
    struct { 
	unsigned uart0_clk_en :1;
	unsigned uart1_clk_en :1;
	unsigned uart2_clk_en :1;
	unsigned uart3_clk_en :1;
	unsigned spi_m_clk_en :1;
	unsigned spi_s_clk_en :1;
	unsigned i2c_clk_en :1;
	unsigned io_ctrl_clk_en :1;
	unsigned analog_regs_clk_en :1;
	unsigned fuse_clk_en :1;
	unsigned timers_clk_en :1;
	unsigned rsv0 :5;
	unsigned dma_clk_en :1;
	unsigned matrix_clk_en :1;
	unsigned adc_clock_sel :2;
	unsigned clk_1m_div :1;
	unsigned rsv1 :2;
	unsigned de_det_en :1;
	unsigned uart_in1_det_en :1;
	unsigned uart_in2_det_en :1;
	unsigned uart_in3_det_en :1;
	unsigned uart_in4_det_en :1;
	unsigned rsv2 :4;
    } b;
} SYS_CFG_PT_CLK_CTRL_UT;

typedef union _SYS_CFG_PT_RESET_CTRL_U_ {
    unsigned int w32; 
    struct { 
	unsigned sysrst :1;
	unsigned rsv0 :3;
	unsigned wd_refresh :1;
	unsigned rsv1 :27;
    } b;
} SYS_CFG_PT_RESET_CTRL_UT;

typedef union _SYS_CFG_PT_ADDR_SPACE_CTRL_U_ {
    unsigned int w32; 
    struct { 
	unsigned write2rom :1;
	unsigned rsv0 :31;
    } b;
} SYS_CFG_PT_ADDR_SPACE_CTRL_UT;

typedef union _SYS_CFG_PT_TEST_DIG_MUX_SEL_U_ {
    unsigned int w32; 
    struct { 
	unsigned test_src :3;
	unsigned rsv0 :29;
    } b;
} SYS_CFG_PT_TEST_DIG_MUX_SEL_UT;

typedef union _SYS_CFG_PT_CLK_1K_COUNT_CTRL_U_ {
    unsigned int w32; 
    struct { 
	unsigned clk_1k_count_req :1;
	unsigned rsv0 :3;
	unsigned clk_1k_count_intr_clr :1;
	unsigned rsv1 :27;
    } b;
} SYS_CFG_PT_CLK_1K_COUNT_CTRL_UT;

typedef union _SYS_CFG_PT_CLK_1K_COUNT_RES_U_ {
    unsigned int w32; 
    struct { 
	unsigned clk_1k_count_res :16;
	unsigned clk_1k_count_done :1;
	unsigned rsv0 :15;
    } b;
} SYS_CFG_PT_CLK_1K_COUNT_RES_UT;

typedef union _SYS_CFG_PT_UART_MUXING_U_ {
    unsigned int w32; 
    struct { 
	unsigned uart0_tx_sel :1;
	unsigned uart1_tx_sel :1;
	unsigned uart2_tx_sel :1;
	unsigned uart3_tx_sel :1;
	unsigned uart0_rs485 :1;
	unsigned uart1_rs485 :1;
	unsigned rsv0 :2;
	unsigned uart0_de_val :1;
	unsigned uart0_de_sel :1;
	unsigned rsv1 :2;
	unsigned uart0_de_inv :1;
	unsigned uart1_duplex_inv :1;
	unsigned rsv2 :18;
    } b;
} SYS_CFG_PT_UART_MUXING_UT;

typedef union _SYS_CFG_PT_RS485_RX_DET_U_ {
    unsigned int w32; 
    struct { 
	unsigned rsv0 :9;
	unsigned din :1;
	unsigned int_mode :3;
	unsigned rsv1 :19;
    } b;
} SYS_CFG_PT_RS485_RX_DET_UT;

//module typedef
typedef volatile struct _SYS_CFG_PT_MODULE_S
{
	SYS_CFG_PT_CLK_CTRL_UT CLK_CTRL;
	SYS_CFG_PT_RESET_CTRL_UT RESET_CTRL;
	SYS_CFG_PT_ADDR_SPACE_CTRL_UT ADDR_SPACE_CTRL;
	SYS_CFG_PT_TEST_DIG_MUX_SEL_UT TEST_DIG_MUX_SEL;
	SYS_CFG_PT_CLK_1K_COUNT_CTRL_UT CLK_1K_COUNT_CTRL;
	SYS_CFG_PT_CLK_1K_COUNT_RES_UT CLK_1K_COUNT_RES;
	SYS_CFG_PT_UART_MUXING_UT UART_MUXING;
	SYS_CFG_PT_RS485_RX_DET_UT RS485_RX_DET;
} SYS_CFG_PT_MODULE_ST;



//define parameterized depths


//define enumerated types


//define offsets
#define TIMER_PT_TIMER1LOADCOUNT_OFFSET  (0x0)
#define TIMER_PT_TIMER1CURRENTVAL_OFFSET  (0x4)
#define TIMER_PT_TIMER1CONTROLREG_OFFSET  (0x8)
#define TIMER_PT_TIMER1EOI_OFFSET  (0xC)
#define TIMER_PT_TIMER1INTSTAT_OFFSET  (0x10)
#define TIMER_PT_TIMER2LOADCOUNT_OFFSET  (0x14)
#define TIMER_PT_TIMER2CURRENTVAL_OFFSET  (0x18)
#define TIMER_PT_TIMER2CONTROLREG_OFFSET  (0x1C)
#define TIMER_PT_TIMER2EOI_OFFSET  (0x20)
#define TIMER_PT_TIMER2INTSTAT_OFFSET  (0x24)
#define TIMER_PT_TIMER3LOADCOUNT_OFFSET  (0x28)
#define TIMER_PT_TIMER3CURRENTVAL_OFFSET  (0x2C)
#define TIMER_PT_TIMER3CONTROLREG_OFFSET  (0x30)
#define TIMER_PT_TIMER3EOI_OFFSET  (0x34)
#define TIMER_PT_TIMER3INTSTAT_OFFSET  (0x38)
#define TIMER_PT_TimersIntStatus_OFFSET  (0xA0)
#define TIMER_PT_TimersEOI_OFFSET  (0xA4)
#define TIMER_PT_TimersRawIntStatus_OFFSET  (0xA8)
#define TIMER_PT_TIMERS_COMP_VERSION_OFFSET  (0xAC)
#define TIMER_PT_TIMER1LOADCOUNT2_OFFSET  (0xB0)
#define TIMER_PT_TIMER2LOADCOUNT2_OFFSET  (0xB4)
#define TIMER_PT_TIMER3LOADCOUNT2_OFFSET  (0xB8)


typedef union _TIMER_PT_TIMER1LOADCOUNT_U_ {
    unsigned int w32; 
    struct { 
	unsigned timernloadcount :32;
    } b;
} TIMER_PT_TIMER1LOADCOUNT_UT;

typedef union _TIMER_PT_TIMER1CURRENTVAL_U_ {
    unsigned int w32; 
    struct { 
	unsigned timerncurrentvalue :32;
    } b;
} TIMER_PT_TIMER1CURRENTVAL_UT;

typedef union _TIMER_PT_TIMER1CONTROLREG_U_ {
    unsigned int w32; 
    struct { 
	unsigned timer_enable :1;
	unsigned timer_mode :1;
	unsigned timer_interrupt_mask :1;
	unsigned timer_pwm :1;
	unsigned rsvd_timer_0n100pwm_en :1;
	unsigned rsvd_timerncontrolreg :27;
    } b;
} TIMER_PT_TIMER1CONTROLREG_UT;

typedef union _TIMER_PT_TIMER1EOI_U_ {
    unsigned int w32; 
    struct { 
	unsigned timerneoi :1;
	unsigned rsvd_timerneoi :31;
    } b;
} TIMER_PT_TIMER1EOI_UT;

typedef union _TIMER_PT_TIMER1INTSTAT_U_ {
    unsigned int w32; 
    struct { 
	unsigned timernintstatus :1;
	unsigned rsvd_timernintstatus :31;
    } b;
} TIMER_PT_TIMER1INTSTAT_UT;

typedef union _TIMER_PT_TIMER2LOADCOUNT_U_ {
    unsigned int w32; 
    struct { 
	unsigned timer2loadcount :32;
    } b;
} TIMER_PT_TIMER2LOADCOUNT_UT;

typedef union _TIMER_PT_TIMER2CURRENTVAL_U_ {
    unsigned int w32; 
    struct { 
	unsigned timer2currentval :32;
    } b;
} TIMER_PT_TIMER2CURRENTVAL_UT;

typedef union _TIMER_PT_TIMER2CONTROLREG_U_ {
    unsigned int w32; 
    struct { 
	unsigned timer_enable :1;
	unsigned timer_mode :1;
	unsigned timer_interrupt_mask :1;
	unsigned timer_pwm :1;
	unsigned rsvd_timer_0n100pwm_en :1;
	unsigned rsvd_timer2controlreg :27;
    } b;
} TIMER_PT_TIMER2CONTROLREG_UT;

typedef union _TIMER_PT_TIMER2EOI_U_ {
    unsigned int w32; 
    struct { 
	unsigned timer2eoi :1;
	unsigned rsvd_timer2eoi :31;
    } b;
} TIMER_PT_TIMER2EOI_UT;

typedef union _TIMER_PT_TIMER2INTSTAT_U_ {
    unsigned int w32; 
    struct { 
	unsigned timer2intstat :1;
	unsigned rsvd_timer2intstat :31;
    } b;
} TIMER_PT_TIMER2INTSTAT_UT;

typedef union _TIMER_PT_TIMER3LOADCOUNT_U_ {
    unsigned int w32; 
    struct { 
	unsigned timer3loadcount :32;
    } b;
} TIMER_PT_TIMER3LOADCOUNT_UT;

typedef union _TIMER_PT_TIMER3CURRENTVAL_U_ {
    unsigned int w32; 
    struct { 
	unsigned timer3currentval :32;
    } b;
} TIMER_PT_TIMER3CURRENTVAL_UT;

typedef union _TIMER_PT_TIMER3CONTROLREG_U_ {
    unsigned int w32; 
    struct { 
	unsigned timer_enable :1;
	unsigned timer_mode :1;
	unsigned timer_interrupt_mask :1;
	unsigned timer_pwm :1;
	unsigned rsvd_timer_0n100pwm_en :1;
	unsigned rsvd_timer3controlreg :27;
    } b;
} TIMER_PT_TIMER3CONTROLREG_UT;

typedef union _TIMER_PT_TIMER3EOI_U_ {
    unsigned int w32; 
    struct { 
	unsigned timer3eoi :1;
	unsigned rsvd_timer3eoi :31;
    } b;
} TIMER_PT_TIMER3EOI_UT;

typedef union _TIMER_PT_TIMER3INTSTAT_U_ {
    unsigned int w32; 
    struct { 
	unsigned timer3intstat :1;
	unsigned rsvd_timer3intstat :31;
    } b;
} TIMER_PT_TIMER3INTSTAT_UT;

typedef union _TIMER_PT_TimersIntStatus_U_ {
    unsigned int w32; 
    struct { 
	unsigned timersintstatus :3;
	unsigned rsvd_timersintstatus :29;
    } b;
} TIMER_PT_TimersIntStatus_UT;

typedef union _TIMER_PT_TimersEOI_U_ {
    unsigned int w32; 
    struct { 
	unsigned timerseoi :3;
	unsigned rsvd_timerseoi :29;
    } b;
} TIMER_PT_TimersEOI_UT;

typedef union _TIMER_PT_TimersRawIntStatus_U_ {
    unsigned int w32; 
    struct { 
	unsigned timersrawintstat :3;
	unsigned rsvd_timersrawintstat :29;
    } b;
} TIMER_PT_TimersRawIntStatus_UT;

typedef union _TIMER_PT_TIMERS_COMP_VERSION_U_ {
    unsigned int w32; 
    struct { 
	unsigned timerscompversion :32;
    } b;
} TIMER_PT_TIMERS_COMP_VERSION_UT;

typedef union _TIMER_PT_TIMER1LOADCOUNT2_U_ {
    unsigned int w32; 
    struct { 
	unsigned timernloadcount2 :32;
    } b;
} TIMER_PT_TIMER1LOADCOUNT2_UT;

typedef union _TIMER_PT_TIMER2LOADCOUNT2_U_ {
    unsigned int w32; 
    struct { 
	unsigned timer2loadcount2 :32;
    } b;
} TIMER_PT_TIMER2LOADCOUNT2_UT;

typedef union _TIMER_PT_TIMER3LOADCOUNT2_U_ {
    unsigned int w32; 
    struct { 
	unsigned timer3loadcount2 :32;
    } b;
} TIMER_PT_TIMER3LOADCOUNT2_UT;

//module typedef
typedef volatile struct _TIMER_PT_MODULE_S
{
	TIMER_PT_TIMER1LOADCOUNT_UT TIMER1LOADCOUNT;
	TIMER_PT_TIMER1CURRENTVAL_UT TIMER1CURRENTVAL;
	TIMER_PT_TIMER1CONTROLREG_UT TIMER1CONTROLREG;
	TIMER_PT_TIMER1EOI_UT TIMER1EOI;
	TIMER_PT_TIMER1INTSTAT_UT TIMER1INTSTAT;
	TIMER_PT_TIMER2LOADCOUNT_UT TIMER2LOADCOUNT;
	TIMER_PT_TIMER2CURRENTVAL_UT TIMER2CURRENTVAL;
	TIMER_PT_TIMER2CONTROLREG_UT TIMER2CONTROLREG;
	TIMER_PT_TIMER2EOI_UT TIMER2EOI;
	TIMER_PT_TIMER2INTSTAT_UT TIMER2INTSTAT;
	TIMER_PT_TIMER3LOADCOUNT_UT TIMER3LOADCOUNT;
	TIMER_PT_TIMER3CURRENTVAL_UT TIMER3CURRENTVAL;
	TIMER_PT_TIMER3CONTROLREG_UT TIMER3CONTROLREG;
	TIMER_PT_TIMER3EOI_UT TIMER3EOI;
	TIMER_PT_TIMER3INTSTAT_UT TIMER3INTSTAT;
	unsigned rsv_38_A0 [25];
	TIMER_PT_TimersIntStatus_UT TimersIntStatus;
	TIMER_PT_TimersEOI_UT TimersEOI;
	TIMER_PT_TimersRawIntStatus_UT TimersRawIntStatus;
	TIMER_PT_TIMERS_COMP_VERSION_UT TIMERS_COMP_VERSION;
	TIMER_PT_TIMER1LOADCOUNT2_UT TIMER1LOADCOUNT2;
	TIMER_PT_TIMER2LOADCOUNT2_UT TIMER2LOADCOUNT2;
	TIMER_PT_TIMER3LOADCOUNT2_UT TIMER3LOADCOUNT2;
} TIMER_PT_MODULE_ST;



//define parameterized depths


//define enumerated types


//define offsets
#define UART_PT_RBR_DLL_THR_OFFSET  (0x0)
#define UART_PT_IER_DLH_OFFSET  (0x4)
#define UART_PT_IIR_FCR_OFFSET  (0x8)
#define UART_PT_LCR_OFFSET  (0xC)
#define UART_PT_MCR_OFFSET  (0x10)
#define UART_PT_LSR_OFFSET  (0x14)
#define UART_PT_MSR_OFFSET  (0x18)
#define UART_PT_SCR_OFFSET  (0x1C)
#define UART_PT_FAR_OFFSET  (0x70)
#define UART_PT_TFR_OFFSET  (0x74)
#define UART_PT_RFW_OFFSET  (0x78)
#define UART_PT_USR_OFFSET  (0x7C)
#define UART_PT_HTX_OFFSET  (0xA4)
#define UART_PT_DMASA_OFFSET  (0xA8)
#define UART_PT_REG_TIMEOUT_RST_OFFSET  (0xD4)


typedef union _UART_PT_RBR_DLL_THR_U_ {
    unsigned int w32; 
    struct { 
	unsigned rbr :8;
	unsigned rsvd_rbr :24;
    } b;
} UART_PT_RBR_DLL_THR_UT;

typedef union _UART_PT_IER_DLH_U_ {
    unsigned int w32; 
    struct { 
	unsigned erbfi :1;
	unsigned etbei :1;
	unsigned elsi :1;
	unsigned edssi :1;
	unsigned elcolr :1;
	unsigned rsvd_ier_6to5 :2;
	unsigned ptime :1;
	unsigned rsvd_ier_31to8 :24;
    } b;
} UART_PT_IER_DLH_UT;

typedef union _UART_PT_IIR_FCR_U_ {
    unsigned int w32; 
    struct { 
	unsigned iid :4;
	unsigned rsvd_iir_5to4 :2;
	unsigned fifose :2;
	unsigned rsvd_iir_31to8 :24;
    } b;
} UART_PT_IIR_FCR_UT;

typedef union _UART_PT_LCR_U_ {
    unsigned int w32; 
    struct { 
	unsigned dls :2;
	unsigned stop :1;
	unsigned pen :1;
	unsigned eps :1;
	unsigned sp :1;
	unsigned bc :1;
	unsigned dlab :1;
	unsigned rsvd_lcr_31to8 :24;
    } b;
} UART_PT_LCR_UT;

typedef union _UART_PT_MCR_U_ {
    unsigned int w32; 
    struct { 
	unsigned dtr :1;
	unsigned rts :1;
	unsigned out1 :1;
	unsigned out2 :1;
	unsigned loopback :1;
	unsigned afce :1;
	unsigned sire :1;
	unsigned rsvd_mcr_31to7 :25;
    } b;
} UART_PT_MCR_UT;

typedef union _UART_PT_LSR_U_ {
    unsigned int w32; 
    struct { 
	unsigned dr :1;
	unsigned oe :1;
	unsigned pe :1;
	unsigned fe :1;
	unsigned bi :1;
	unsigned thre :1;
	unsigned temt :1;
	unsigned rfe :1;
	unsigned rsvd_addr_rcvd :1;
	unsigned rsvd_lsr_31to9 :23;
    } b;
} UART_PT_LSR_UT;

typedef union _UART_PT_MSR_U_ {
    unsigned int w32; 
    struct { 
	unsigned dcts :1;
	unsigned ddsr :1;
	unsigned teri :1;
	unsigned ddcd :1;
	unsigned cts :1;
	unsigned dsr :1;
	unsigned ri :1;
	unsigned dcd :1;
	unsigned rsvd_msr_31to8 :24;
    } b;
} UART_PT_MSR_UT;

typedef union _UART_PT_SCR_U_ {
    unsigned int w32; 
    struct { 
	unsigned scr :8;
	unsigned rsvd_scr_31to8 :24;
    } b;
} UART_PT_SCR_UT;

typedef union _UART_PT_FAR_U_ {
    unsigned int w32; 
    struct { 
	unsigned far :1;
	unsigned rsvd_far_31to1 :31;
    } b;
} UART_PT_FAR_UT;

typedef union _UART_PT_TFR_U_ {
    unsigned int w32; 
    struct { 
	unsigned tfr :8;
	unsigned rsvd_tfr_31to8 :24;
    } b;
} UART_PT_TFR_UT;

typedef union _UART_PT_RFW_U_ {
    unsigned int w32; 
    struct { 
	unsigned rfwd :8;
	unsigned rfpe :1;
	unsigned rffe :1;
	unsigned rsvd_rfw_31to10 :22;
    } b;
} UART_PT_RFW_UT;

typedef union _UART_PT_USR_U_ {
    unsigned int w32; 
    struct { 
	unsigned busy :1;
	unsigned rsvd_tfnf :1;
	unsigned rsvd_tfe :1;
	unsigned rsvd_rfne :1;
	unsigned rsvd_rff :1;
	unsigned rsvd_usr_31to5 :27;
    } b;
} UART_PT_USR_UT;

typedef union _UART_PT_HTX_U_ {
    unsigned int w32; 
    struct { 
	unsigned htx :1;
	unsigned rsvd_htx_31to1 :31;
    } b;
} UART_PT_HTX_UT;

typedef union _UART_PT_DMASA_U_ {
    unsigned int w32; 
    struct { 
	unsigned dmasa :1;
	unsigned rsvd_dmasa_31to1 :31;
    } b;
} UART_PT_DMASA_UT;

typedef union _UART_PT_REG_TIMEOUT_RST_U_ {
    unsigned int w32; 
    struct { 
	unsigned reg_timeout_rst :4;
	unsigned rsvd_reg_timeout_rst :28;
    } b;
} UART_PT_REG_TIMEOUT_RST_UT;

//module typedef
typedef volatile struct _UART_PT_MODULE_S
{
	UART_PT_RBR_DLL_THR_UT RBR_DLL_THR;
	UART_PT_IER_DLH_UT IER_DLH;
	UART_PT_IIR_FCR_UT IIR_FCR;
	UART_PT_LCR_UT LCR;
	UART_PT_MCR_UT MCR;
	UART_PT_LSR_UT LSR;
	UART_PT_MSR_UT MSR;
	UART_PT_SCR_UT SCR;
	unsigned rsv_1C_70 [20];
	UART_PT_FAR_UT FAR;
	UART_PT_TFR_UT TFR;
	UART_PT_RFW_UT RFW;
	UART_PT_USR_UT USR;
	unsigned rsv_7C_A4 [9];
	UART_PT_HTX_UT HTX;
	UART_PT_DMASA_UT DMASA;
	unsigned rsv_A8_D4 [10];
	UART_PT_REG_TIMEOUT_RST_UT REG_TIMEOUT_RST;
} UART_PT_MODULE_ST;

#define ANALOG_MODULE_ADDR  (0x400A0000)
#define DMA_CH0_MODULE_ADDR  (0x50000000)
#define DMA_CH1_MODULE_ADDR  (0x50000058)
#define DMA_CH2_MODULE_ADDR  (0x500000b0)
#define DMA_CH3_MODULE_ADDR  (0x50000108)
#define DMA_CH4_MODULE_ADDR  (0x50000160)
#define DMA_CH5_MODULE_ADDR  (0x500001b8)
#define DMA_CH6_MODULE_ADDR  (0x50000210)
#define DMA_CH7_MODULE_ADDR  (0x50000268)
#define DMA_INTERRUPT_MODULE_ADDR  (0x500002c0)
#define DMA_MISC_MODULE_ADDR  (0x50000398)
#define DMA_SW_HS_MODULE_ADDR  (0x50000368)
#define EFUSE_MODULE_ADDR  (0x400D0000)
#define I2C_MODULE_ADDR  (0x40040000)
#define IO_CTRL_MODULE_ADDR  (0x400C0000)
#define ROM_MODULE_ADDR  (0x00000000)
#define SPI_MASTER_MODULE_ADDR  (0x40050000)
#define SPI_SLAVE_MODULE_ADDR  (0x40060000)
#define SRAM_MODULE_ADDR  (0x20000000)
#define SYS_CFG_MODULE_ADDR  (0x400B0000)
#define TIMERs_MODULE_ADDR  (0x40070000)
#define UART0_MODULE_ADDR  (0x40000000)
#define UART1_MODULE_ADDR  (0x40010000)
#define UART2_MODULE_ADDR  (0x40020000)
#define UART3_MODULE_ADDR  (0x40030000)



#define UART0       ((UART_PT_MODULE_ST  *) UART0_MODULE_ADDR)        /**< UART0 base pointer */
#define UART1       ((UART_PT_MODULE_ST  *) UART1_MODULE_ADDR)        /**< UART1 base pointer */
#define UART2       ((UART_PT_MODULE_ST  *) UART2_MODULE_ADDR)        /**< UART2 base pointer */
#define UART3       ((UART_PT_MODULE_ST  *) UART3_MODULE_ADDR)        /**< UART2 base pointer */

#define MII_SPI_NUM_INSTANCES 2
#define MII_SPI_0_DEVICE_ID   0


/** @} */ /* End of group Device_Peripheral_Registers */

/* --------  End of section using anonymous unions and disabling warnings  -------- */
#if   defined (__CC_ARM)
  #pragma pop
#elif defined (__ICCARM__)
  /* leave anonymous unions enabled */
#elif (defined(__ARMCC_VERSION) && (__ARMCC_VERSION >= 6010050))
  #pragma clang diagnostic pop
#elif defined (__GNUC__)
  /* anonymous unions are enabled by default */
#elif defined (__TMS470__)
  /* anonymous unions are enabled by default */
#elif defined (__TASKING__)
  #pragma warning restore
#elif defined (__CSMC__)
  /* anonymous unions are enabled by default */
#else
  #warning Not supported compiler type
#endif


#ifdef __cplusplus
}
#endif

#endif  /* ARMCM4_H */
