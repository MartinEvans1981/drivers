#ifndef PERIPHERAL_PINS_H
#define PERIPHERAL_PINS_H

#include "PinNames.h"

#ifdef __cplusplus
extern "C" {
#endif

#define NUM_OF_AF 8

typedef enum 
{
    GPIO_FUNC = 0,
    
    I2C_SDA_AF,
    I2C_SCL_AF,
    
    RS485_DE_AF,
    RS485_H_AF,
    
    SWD_CLK_AF,
    SWD_IO_AF,
    
    SPI_CLK_AF,
    
    SPIM_CS_AF,
    SPIM_MISO_AF,
    SPIM_MOSI_AF,
    
    SPIS_CS_AF,
    SPIS_MISO_AF,
    SPIS_MOSI_AF,
    
    UART1_RX_AF,
    UART1_TX_AF,

    UART2_RX_AF,
    UART2_TX_AF,
    
    UART3_RX_AF,
    UART3_TX_AF,
    
    UART4_RX_AF,
    UART4_TX_AF,
    
    CLK24_AF,
    
    NA
}PinFunc;

extern const PinFunc pin_func_map[NUM_OF_GPIOS][NUM_OF_AF];


#ifdef __cplusplus
}
#endif

#endif
