#ifndef PIN_NAMES_H
#define PIN_NAMES_H

#ifdef __cplusplus
extern "C" {
#endif

typedef enum 
{
    DE_PIN = 0,
    H_FN_PIN,
    SPI_CS_0_PIN,
    SPI_DIN_PIN,
    SPI_DOUT_PIN,
    SPI_SCK_PIN,
    UART_IN1_PIN,
    UART_IN2_PIN,
    UART_IN3_PIN,
    UART_IN4_PIN,
    UART_OUT1_PIN,
    UART_OUT2_PIN,
    UART_OUT3_PIN,
    UART_OUT4_PIN,

    NOT_CONNECTED,
    
    NUM_OF_GPIOS = NOT_CONNECTED
}PinName;




// TEMP LOCATION:
typedef void (*interrupt_handler) (void);



#ifdef __cplusplus
}
#endif

#endif
