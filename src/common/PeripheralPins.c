#include "PeripheralPins.h"

const PinFunc pin_func_map[NUM_OF_GPIOS][NUM_OF_AF] = 
{
    {GPIO_FUNC,   RS485_DE_AF,   SWD_IO_AF,    NA,         NA,         NA, NA, NA}, // DE_PIN
    {GPIO_FUNC,   RS485_H_AF,    SWD_CLK_AF,   NA,         NA,         NA, NA, NA}, // H_FN_PIN
    {GPIO_FUNC,   SPIM_CS_AF,    SPIS_CS_AF,   NA,         NA,         NA, NA, NA}, // SPI_CS_0_PIN
    {GPIO_FUNC,   SPIM_MISO_AF,  SPIS_MISO_AF, NA,         NA,         NA, NA, NA}, // SPI_DIN_PIN
    {GPIO_FUNC,   SPIM_MOSI_AF,  SPIS_MOSI_AF, I2C_SDA_AF, NA,         NA, NA, NA}, // SPI_DOUT_PIN
    {GPIO_FUNC,   SPI_CLK_AF,    I2C_SCL_AF,   NA,         NA,         NA, NA, NA}, // SPI_SCK_PIN
    {GPIO_FUNC,   UART1_RX_AF,   NA,           NA,         NA,         NA, NA, NA}, // UART_IN1_PIN
    {GPIO_FUNC,   UART2_RX_AF,   NA,           NA,         NA,         NA, NA, NA}, // UART_IN2_PIN
    {GPIO_FUNC,   UART3_RX_AF,   NA,           NA,         NA,         NA, NA, NA}, // UART_IN3_PIN
    {GPIO_FUNC,   UART4_RX_AF,   NA,           NA,         NA,         NA, NA, NA}, // UART_IN4_PIN
    {GPIO_FUNC,   UART1_TX_AF,   CLK24_AF,     NA,         NA,         NA, NA, NA}, // UART_OUT1_PIN
    {GPIO_FUNC,   UART2_TX_AF,   NA,           NA,         NA,         NA, NA, NA}, // UART_OUT2_PIN
    {GPIO_FUNC,   UART3_TX_AF,   NA,           NA,         NA,         NA, NA, NA}, // UART_OUT3_PIN
    {GPIO_FUNC,   UART4_TX_AF,   NA,           NA,         NA,         NA, NA, NA}, // UART_OUT4_PIN
};
