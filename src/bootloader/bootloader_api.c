#include "bootloader_api.h"

void start_application(uintptr_t address)
{
    void *sp;
    void *pc;

    // Interrupts are re-enabled in start_new_application
    __disable_irq();

    SysTick->CTRL = 0x00000000;
    powerdown_nvic();
    powerdown_scb(address);
    
    sp = *((void **)address + 0);
    pc = *((void **)address + 1);
    start_new_application(sp, pc);
}

/*****************************************************************/

static void powerdown_nvic()
{
    int i;
    int j;
    int isr_groups_32;

    isr_groups_32 = ((SCnSCB->ICTR & SCnSCB_ICTR_INTLINESNUM_Msk) >> SCnSCB_ICTR_INTLINESNUM_Pos) + 1;

    for (i = 0; i < isr_groups_32; i++) 
    {
        NVIC->ICER[i] = 0xFFFFFFFF;
        NVIC->ICPR[i] = 0xFFFFFFFF;
        for (j = 0; j < 8; j++) 
        {
            NVIC->IP[i * 8 + j] = 0x00000000;
        }
    }
}

/*****************************************************************/

static void powerdown_scb(uint32_t vtor)
{
    int i;

    // SCB->CPUID   - Read only CPU ID register
    SCB->ICSR = SCB_ICSR_PENDSVCLR_Msk | SCB_ICSR_PENDSTCLR_Msk;
    SCB->VTOR = vtor;
    SCB->AIRCR = 0x05FA | 0x0000;
    SCB->SCR = 0x00000000;

    int num_pri_reg; // Number of priority registers
    num_pri_reg = 12;
    for (i = 0; i < num_pri_reg; i++) 
    {
        SCB->SHP[i] = 0x00;
    }
    SCB->SHCSR = 0x00000000;

    SCB->CFSR = 0xFFFFFFFF;
    SCB->HFSR = SCB_HFSR_DEBUGEVT_Msk | SCB_HFSR_FORCED_Msk | SCB_HFSR_VECTTBL_Msk;
    SCB->DFSR = SCB_DFSR_EXTERNAL_Msk | SCB_DFSR_VCATCH_Msk |
                SCB_DFSR_DWTTRAP_Msk | SCB_DFSR_BKPT_Msk | SCB_DFSR_HALTED_Msk;
}

/*****************************************************************/

__asm static void start_new_application(void *sp, void *pc)
{
    MOVS R2, #0
    MSR CONTROL, R2         // Switch to main stack
    MOV SP, R0
    MSR PRIMASK, R2         // Enable interrupts
    BX R1
}
