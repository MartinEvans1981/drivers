#ifndef BOOTLOADER_HAL_H
#define BOOTLOADER_HAL_H

#include <ARMCM4_MII.h>

#ifdef __cplusplus
extern "C" {    // allow C++ to use these headers
#endif

void start_application(uintptr_t address);
static void powerdown_nvic(void);
static void powerdown_scb(uint32_t vtor);
static void start_new_application(void *sp, void *pc);

    
#ifdef __cplusplus
}
#endif


#endif
